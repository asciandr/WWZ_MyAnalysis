EventLoop AnalysisBase-based code producing mini ntuples and/or histograms out of STDM5(3) DxAODs.

* 30.09.17: works with ``AnalysisBase,21.2.5``.
* 31.10.17: works with ``AnalysisBase,21.2.6``.
* 24.11.17: works with ``AnalysisBase,21.2.10``.
* 05.03.18: works with ``AnalysisBase,21.2.20``.
* 28.04.18: works with ``AnalysisBase,21.2.26``.
* 01.05.18: works with ``AnalysisBase,21.2.27``.
* 12.07.18: works with ``AnalysisBase,21.2.34``.

It is inspired by some parts of the software tutorial at https://atlassoftwaredocs.web.cern.ch/ABtutorial/ and has been migrated to use CMake and run locally (through the executable testRun) or on the GRID (submit_GRID.sh). 

Check git project out and setup environment
---------
I.e. ::

  lsetup git
  git clone https://:@gitlab.cern.ch:8443/asciandr/WWZ_MyAnalysis.git
  cd WWZ_MyAnalysis
  mkdir build
  cd source 
  setupATLAS
  asetup AnalysisBase,21.2.34,here 

Compile
---------
Compile your code with cmake ::

  cd ../build
  cmake ../source && make -j4
  source x86_64-slc6-gcc62-opt/setup.sh

Update cross-section file
---------
Go to source/data/ and download the latest version of SUSY xsec list ::

  cd $TestArea/WWZanalysis/data/
  svn export svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/trunk/data/susy_crosssections_13TeV.txt susy_crosssections_13TeV.txt

Customise your job configuration
---------
Change options in the config file (e.g. run/config) to choose job options, CP tools configuration, event selection, ...

Test locally
---------
Run a local test of your code ::

  cd ../run
  testRun outDir $TestArea/../run/config $TestArea/WWZanalysis/data/susy_crosssections_13TeV.txt

where in config you should set path and name of input file, ``susy_crosssections_13TeV.txt`` is the cross-section file and outDir is the output folder with histos and ntuples produced.

Check memory leaks
---------
Run valgrind for your executable ::

  valgrind --leak-check=full --trace-children=yes --num-callers=18 --show-reachable=yes --track-origins=yes --error-limit=no --smc-check=all testRun outDir $TestArea/../run/config $TestArea/WWZanalysis/data/susy_crosssections_13TeV.txt 2>&1 | tee valgrind.log &

and carefully check the `LEAK SUMMARY` at the end of the log file. In case of large leak check whether it is an initialisation or per-event leak.

Test on the GRID
---------
Source setup for GRID jobs ::

  . GRID_setup.sh

and run a GRID test of your code, e.g. ::

  testRun_on_the_GRID outDir mc16_13TeV.364243.Sherpa_222_NNPDF30NNLO_WWZ_4l2v_EW6.deriv.DAOD_STDM5.e5887_e5984_s3126_r9781_r9778_p3309/ WWZ_4l2v_EW6.364243.STDM5.flat_ntuple test.txt $TestArea/../run/config &>log &

Launch nominal production on the GRID
---------
Source setup for GRID jobs ::

  . GRID_setup.sh

put list of derivations into a .txt file, e.g. STDM5 derivations :: 

  rucio list-dids mc16_13TeV.*STDM5*r9315_p3371 | grep -o "mc16_13TeV.*_p3371" | cut -f2- -d: >& p3371_MCLists/MC16a.txt
  rucio list-dids mc16_13TeV.*STDM5*r9778_p3371 | grep -o "mc16_13TeV.*_p3371" | cut -f2- -d: >& p3371_MCLists/MC16c.txt

set ntuple name extension in ``submit_GRID.sh`` and launch a GRID job per input ::

  ./submit_GRID.sh  old_lists/list_STDM5.txt

Launch full-systematic production on the GRID
---------
When producing ~80 systematic TTrees per sample, it would take forever to launch all of the grid jobs; you can do this exploiting batch jobs. Setup your grid proxy (and nothing else!) in a suitable folder and with relevant permissions, e.g.::

  voms-proxy-init -voms atlas --out $WORK_DIR/grid_proxy --valid 72:0
  chmod 755 $WORK_DIR/grid_proxy

Set ntuple name extension, config file name and blacklist of grid sites in ``batch_submit_GRID.sh``, check that all of the paths are correctly defined in ``BATCH_job.sh`` and launch a batch job per input (itself launching one GRID job per ``syst_ttree`` defined in the config) ::

  ./batch_submit_GRID.sh list.txt

Check GRID jobs
---------
Set the production name, e.g. ``productionName = 'ntuple_v0.10'``, in ``check_GRID_jobs.py`` and run this python script ::

  . CheckGRIDjobs_setup.sh
  python check_GRID_jobs.py

Under ``# list of options`` there's plenty of bool options to kill(), retry(), killAndRetry(), ... jobs according to their status (e.g. ``retry_finished``).

Download and hadd flat ntuples
---------
Download flat ntuples organised in a list (e.g. rucio_ntuples.txt) through rucio ::

  cd utils/
  rucio list-dids user.asciandr.mc16_13TeV.*.ntuple_v0.4_myOutput.root | grep -o "user.*.root" | cut -f2- -d: >& rucio_ntuples.txt 
  . getFlatNtuples.sh rucio_ntuples.txt

N.B. ``source`` (don't run!) bash script. Hadd flat ntuples per sample (``IMPORTANT`` not to get wrong sample total weight) giving same input .txt to a dedicated script ::

  . haddFlatNtuples.sh rucio_ntuples.txt

Total events and weight
---------
Store the total number of processed events and their total per-lumi weight, e.g. ::

  cd utils/totweight/
  . setup.sh
  python getTotWeight.py flat_list.txt > output/weights.txt
