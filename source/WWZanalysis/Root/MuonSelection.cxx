#include "WWZanalysis/MuonSelection.h"

#include <unordered_map>

namespace WWZanalysis {
  namespace MuonSelection {



    std::vector<xAOD::Muon const *> filter_by_type(xAOD::Muon::MuonType type, std::vector<xAOD::Muon const *> in) {
      std::vector<xAOD::Muon const *> out;
      out.reserve(in.size());

      for (auto const * ob : in) {
        if (ob->muonType() == type) {
          out.push_back(ob);
        }
      }

      return out;
    }



    std::unordered_map<std::string, xAOD::Muon::Quality> const quality_map = {
        {"Tight", xAOD::Muon::Tight},
        {"Medium", xAOD::Muon::Medium},
        {"Loose", xAOD::Muon::Loose},
        {"VeryLoose", xAOD::Muon::VeryLoose}
    };



    xAOD::Muon::Quality to_enumerated_quality(std::string const & quality_str) {
      try {
        return quality_map.at(quality_str);
      }
      catch (std::exception const & except) {
        std::cerr << except.what() << std::endl;
        throw std::logic_error(std::string("Cannot convert muon selection working point string \"") + quality_str + std::string("\" to enumerated type xAOD::Muon::Quality. Please check the map in WWZanalysis/MuonSelection.cxx"));
      }
    }



    std::vector<xAOD::Muon const *> make_std_vector(xAOD::MuonContainer const * in) {
      std::vector<xAOD::Muon const *> out;
      out.reserve(in->size());

      for (auto const & ob : * in) {
        out.push_back(ob);
      }

      return out;
    }



    void apply_basic_selection(xAOD::MuonContainer * out, xAOD::MuonContainer const * in, CP::MuonSelectionTool const * selection_tool) {
      for (auto const * muon : * in) {
        if (selection_tool->accept(muon)) {
          xAOD::Muon * selected_muon = new xAOD::Muon(); // don't delete, we give up ownership of it to TStore
          selected_muon->makePrivateStore(* muon);
          out->push_back(selected_muon);
        }
      }
    }



    std::vector<xAOD::Muon const *> select_accepted_by_standalone_isolation_and_adjust_scale_factor(
        std::vector<xAOD::Muon const *> const & in,
        CP::IsolationSelectionTool * iso_tool,
        CP::MuonEfficiencyScaleFactors * old_sf_tool,
        CP::MuonEfficiencyScaleFactors * new_sf_tool) {

      /// Do nothing if the tool does not exist
      if (! iso_tool) {
        return in;
      }

      std::vector<xAOD::Muon const *> out;

      for (auto const * ob : in) {
        if (ob->muonType() == xAOD::Muon::MuonType::MuonStandAlone) { /// A stand-alone muon!
          if (iso_tool->accept(* ob)) { /// Passes the new isolation

            ///
            /// Update the scale factor to match the new isolation requirement
            ///
            /// Get old total scale factor weight (may contain isolation scale factors and others)
            double old_scale_factor_weight = ScaleFactors::get_weight(ob);
            //std::cout << "Old scale factor weight: " << old_scale_factor_weight << std::endl;

            /// Get old isolation scale factor
            float old_iso_sf = 1.0;
            if (old_sf_tool) {
              old_sf_tool->getEfficiencyScaleFactor(* ob, old_iso_sf); // Didn't manage trivially to wrap this is in a CHECK macro
            }
            //std::cout << "Old iso SF: " << old_iso_sf << std::endl;

            /// Get new isolation scale factor
            float new_iso_sf = 1.0;
            if (new_sf_tool) {
              new_sf_tool->getEfficiencyScaleFactor(* ob, new_iso_sf); // Didn't manage trivially to wrap this is in a CHECK macro
            }
            //std::cout << "New iso SF: " << new_iso_sf << std::endl;

            /// Calculate new scale factor weight
            double new_scale_factor_weight = old_scale_factor_weight * static_cast<double>(new_iso_sf) / static_cast<double>(old_iso_sf);
            //std::cout << "New scale factor weight: " << new_scale_factor_weight << std::endl << std::endl;

            /// Decorate muon object with new scale factor weight and add it to output vector
            ob->auxdecor<double>("scale_factor_weight") = new_scale_factor_weight;
            out.push_back(ob);
          }
        }
        else { /// Not a stand-alone muon, leave unchanged!
          out.push_back(ob);
        }
      }

      return out;
    }



  }
}
