#include "WWZanalysis/DileptonPair.h"
#include "WWZanalysis/ZPoleMass.h"

#include <cmath>



namespace WWZanalysis {



  bool first_argument_is_closer_to_Z_mass(double m1, double m2) {
    double const distance_first = std::abs(Z_mass - m1);
    double const distance_second = std::abs(Z_mass - m2);
    return (distance_first < distance_second);
  }



  DileptonPair::DileptonPair(std::pair<Dilepton, Dilepton> dileptons_) :
    dileptons(dileptons_)
  {
  }



  DileptonPairType DileptonPair::channel() const {
    auto const flavour_1 =  dileptons.first.flavour;
    auto const flavour_2 =  dileptons.second.flavour;

    if (flavour_1 == LeptonFlavour::Electron && flavour_2 == LeptonFlavour::Electron) {
      return DileptonPairType::Electrons;
    }

    else if (flavour_1 == LeptonFlavour::Muon && flavour_2 == LeptonFlavour::Muon) {
      return DileptonPairType::Muons;
    }

    else {
      return DileptonPairType::Mixed;
    }
  }



  Dilepton DileptonPair::get_dilepton_with_mass_closer_to_Z() const {
    auto const mass_first = dileptons.first.mass();
    auto const mass_second = dileptons.second.mass();
    bool const first_closer = first_argument_is_closer_to_Z_mass(mass_first, mass_second);
    return (first_closer) ? dileptons.first : dileptons.second;
  }



  Dilepton DileptonPair::get_dilepton_with_mass_further_from_Z() const {
    auto const mass_first = dileptons.first.mass();
    auto const mass_second = dileptons.second.mass();
    bool const first_closer = first_argument_is_closer_to_Z_mass(mass_first, mass_second);
    return (first_closer) ? dileptons.second : dileptons.first;
  }



  Dilepton DileptonPair::get_leading_dilepton() const {
    bool const first_leading = (dileptons.first.momentum().Pt() > dileptons.second.momentum().Pt());
    return (first_leading) ? dileptons.first : dileptons.second;
  }



  Dilepton DileptonPair::get_subleading_dilepton() const {
    bool const first_subleading = (dileptons.first.momentum().Pt() <= dileptons.second.momentum().Pt());
    return (first_subleading) ? dileptons.first : dileptons.second;
  }



  TLorentzVector DileptonPair::momentum() const {
    return (dileptons.first.momenta.first + dileptons.first.momenta.second
        + dileptons.second.momenta.first + dileptons.second.momenta.second);
  }



  double DileptonPair::mass() const {
    return this->momentum().M() * 0.001; // Converted to GeV!
  }


}
