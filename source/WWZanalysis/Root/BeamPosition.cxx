#include "WWZanalysis/BeamPosition.h"



namespace WWZanalysis {



  BeamPosition::BeamPosition(xAOD::EventInfo const * ei) :
      sigma_x(ei->beamPosSigmaX()),
      sigma_y(ei->beamPosSigmaY()),
      sigma_xy(ei->beamPosSigmaXY())
  {
  }



}
