#include "WWZanalysis/AbsEtaRange.h"

namespace WWZanalysis {

  AbsEtaRange::AbsEtaRange(double abs_eta_1, double abs_eta_2) :
      abs_eta_min(-1.0), // temporary initialisation
      abs_eta_max(-1.0)  // temporary initialisation
  {
    abs_eta_min = (abs_eta_1 < abs_eta_2) ? abs_eta_1 : abs_eta_2;
    abs_eta_max = (abs_eta_1 > abs_eta_2) ? abs_eta_1 : abs_eta_2;
  }



  AbsEtaRange::AbsEtaRange(double abs_eta_max) :
        abs_eta_min(0.0),
        abs_eta_max(abs_eta_max)
  {
  }



  /// Check if a value lies in the absolute pseudorapidity range
  bool AbsEtaRange::contains_value(double value) const {
    return (value >= abs_eta_min && value < abs_eta_max);
  }




}
