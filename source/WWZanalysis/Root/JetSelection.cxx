#include "WWZanalysis/JetSelection.h"
#include "xAODJet/JetTypes.h"

#include <stdexcept>
#include <cstdlib>

namespace WWZanalysis {
  namespace JetSelection {

    std::vector<xAOD::Jet const *> make_std_vector(xAOD::JetContainer const * in) {
      std::vector<xAOD::Jet const *> out;
      out.reserve(in->size());

      for (auto const & ob : * in) {
        out.push_back(ob);
      }

      return out;
    }

    std::vector<xAOD::Jet const *> sub_std_vectors(std::vector<xAOD::Jet const *> a, std::vector<xAOD::Jet const *> b) {
      std::vector<xAOD::Jet const *> out;
      const int newsize = a.size() - b.size();
      if (newsize < 0)
        throw std::logic_error("In JetSelection::sub_std_vectors(): The result of vector subtraction is negative.");
      out.reserve(newsize);

      std::sort(a.begin(), a.end());
      std::sort(b.begin(), b.end());
      std::set_difference(a.begin(), a.end(), b.begin(), b.end(), std::back_inserter(out));
      return out;
    }

    void decorate_with_updated_JVT(const xAOD::JetContainer * in, const JetVertexTaggerTool * tool) {
      //std::cout << "decorate_with_updated_JVT" << std::endl;
      for (auto const & ob : * in) {
        if (tool) {
          // new updateJvt method requires three inputs, see https://gitlab.cern.ch/atlas/athena/merge_requests/3982
          //const float JVT = tool->updateJvt(* ob, "Jvt", "JetEtaJESScaleMomentum"); // < despite its misleading name, this function does not update the object, it only returns an updated value...
          // in 21.21.20 new updateJvt method needs only one input, see https://gitlab.cern.ch/atlas/athena/merge_requests/7483/diffs#8762f319ccab10a12c0da264d0630708172449bf_31_29
          const float JVT = tool->updateJvt(* ob); //, "Jvt", "JetEtaJESScaleMomentum"); // < despite its misleading name, this function does not update the object, it only returns an updated value...
          //const float JVT = -999.; 
          ob->auxdecor<float>("JVTName") = JVT; // This makes a new decoration. The tool has to have a different "JVTName" than default "Jvt" in initialisation.
          //std::cout << "OLD JVT:	" << ob->auxdata<float>("Jvt") << std::endl;
          //std::cout << "NEW JVT:	" << JVT << std::endl;
        }
      }
      return;
    }

    std::vector<xAOD::Jet const *> select_vertex_tagged(std::vector<xAOD::Jet const *> const & in, double JVT_min, const JetVertexTaggerTool * tool) {
      // Consistency check
      if (in.size() != 0 && ! in.front()->isAvailable<float>(*(tool->getProperty< std::string >("JVTName")))) {
        throw std::logic_error("In JetSelection::select_vertex_tagged(): could not access decoration variable 'updated_JVT_value'!"
        "Please make sure you create it first using JetSelection::get_decorated_with_updated_JVT().");
      }

      std::vector<xAOD::Jet const *> out;
      out.reserve(in.size());

      for (auto const * ob : in) {
        float const JVT = ob->auxdataConst<float>(*(tool->getProperty< std::string >("JVTName")));
        if (JVT > JVT_min) {
          out.push_back(ob);
        }
      }

      return out;
    }

    void decorate_with_fwd_JVT(xAOD::JetContainer * in, const JetForwardJvtTool *tool) {
      if (tool) {
        tool->modify(*in); // assigning "passFJVT" decorator
      }
      return;
    }

    std::vector<xAOD::Jet const *> select_fwd_vertex_tagged(std::vector<xAOD::Jet const *> const & in,  JetForwardJvtTool const * tool) {
      // Consistency check
      if (in.size() != 0 && ! in.front()->isAvailable<char>(*(tool->getProperty< std::string >("OutputDec")))) {
        throw std::logic_error("In JetSelection::select_fwd_vertex_tagged(): could not access decoration variable 'passFJVT'!"
        "Please make sure you create it first using JetSelection::get_decorated_with_fwd_JVT().");
      }

      std::vector<xAOD::Jet const *> out;
      out.reserve(in.size());

      for (auto const * ob : in) {
        if (ob->auxdata<char>(*(tool->getProperty< std::string >("OutputDec")))) {
          out.push_back(ob);
        }
      }
      return out;
    }

    void TEST_decorate_with_truth_fwd_JVT(xAOD::JetContainer * in, xAOD::JetContainer const * raw , JetForwardJvtTool const * tool) {
      if (tool) {
        tool->tagTruth(in,raw); // additional decorators "isJvtHS" and "isJvtPU"
      }
      return;
    }

    std::vector<xAOD::Jet const *> TEST_select_truth_fwd_vertex_tagged(std::vector<xAOD::Jet const *> const & in, Type type) {
      std::vector<xAOD::Jet const *> out;
      out.reserve(in.size());

      for (auto const * ob : in) {
        switch ( type ) {
        default :
        case JetSelection::Type::Jvt :
          if (ob->auxdata<char>("passFJVT") == 1) {
            //std::cout << "openc: jet has tag: passFJVT\n";
            out.push_back(ob);
          }
          break;
        case JetSelection::Type::HardScatter :
          if (ob->auxdata<char>("isJvtHS")) {
            //std::cout << "openc: jet has tag: isJvtHS\n";
            out.push_back(ob);
          }
          break;
        case JetSelection::Type::Pileup :
          if (ob->auxdata<char>("isJvtPU")) {
            //std::cout << "openc: jet has tag: isJvtPU\n";
            out.push_back(ob);
          }
          break;
        }
      }
      return out;
    }

    std::vector<xAOD::Jet const *> select_detector_eta(std::vector<xAOD::Jet const *> const & in, const AbsEtaRange & aer) {
      std::vector<xAOD::Jet const *> out;
      out.reserve(in.size());

      for (auto const & jet : in) {
        //xAOD::JetFourMom_t jetScaleP4 = jet->getAttribute< xAOD::JetFourMom_t >(jetScaleType.c_str());
        auto const detector_eta = jet->jetP4(xAOD::JetConstitScaleMomentum).eta();
        if (aer.contains_value(std::abs(detector_eta))) {
          out.push_back(jet);
        }
      }
      return out;
    }

    std::vector<xAOD::Jet const *> get_JVTs_failed_and_gap_jets(std::vector<xAOD::Jet const *> const & in, std::vector<xAOD::Jet const *> const & central, std::vector<xAOD::Jet const *> const & forward) {

      std::vector<xAOD::Jet const *> tmp;
      tmp.reserve(in.size());
      std::vector<xAOD::Jet const *> out;
      out.reserve(tmp.size());

      // one can do this, if central and forward sets are exclusive
      // fJVT checks eta
      // JVT does not (is done now)
      std::set_difference(in.begin(), in.end(), central.begin(), central.end(), std::back_inserter(tmp));
      std::set_difference(tmp.begin(), tmp.end(), forward.begin(), forward.end(), std::back_inserter(out));
      return out;
    }

  }
}
