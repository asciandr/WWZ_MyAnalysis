#include "WWZanalysis/BTaggingScaleFactors.h"
#include "xAODJet/JetTypes.h"

#include <stdexcept>
#include <cstdlib>

namespace WWZanalysis {
  namespace BTaggingScaleFactors {


    void decorate_with_bTagSFs(const xAOD::JetContainer * in, const BTaggingEfficiencyTool * tool, const BTaggingSelectionTool * sel_tool) {
      //std::cout << "decorate_with_bTagSFs" << std::endl;
      if (tool && sel_tool) {
        for (auto const & ob : * in) {
          float bTagSF(-1.);
          // for tagged jets getScaleFactor
          if(sel_tool->accept(* ob)) {
            tool->getScaleFactor(* ob, bTagSF); 
          }
          // for untagged jets getInefficiencyScaleFactor
          else {
            tool->getInefficiencyScaleFactor(* ob, bTagSF);
          }
          ob->auxdecor<float>("bTagSF") = bTagSF; 
          //std::cout << "nominal bTagSF:		" << bTagSF << std::endl;
        }
      }
      else throw std::logic_error("Error in decorate_with_bTagSFs: BTaggingEfficiencyTool and/or BTaggingSelectionTool are not defined!");
 
      return;
    }

  
    std::vector<float> retrieve_Eigen(const xAOD::Jet *ob, BTaggingEfficiencyTool * tool, const BTaggingSelectionTool * sel_tool, std::vector<CP::SystematicSet> ftag_systematics_list, std::string bstring, int up_down) {
      std::vector<float> eigen_variation;
      //std::string key_string = "FT_EFF_Eigen_"+ bstring;
      std::string up_or_down = (up_down==1) ? "__1up" : "__1down";
    
      //std::cout << "New jet" << std::endl;
      //int counter(-1.);
      for (auto& bSYS : ftag_systematics_list) {  
        if ( !(bSYS.name().find(bstring)!=std::string::npos && bSYS.name().find(up_or_down)!=std::string::npos) ) continue;
        //std::cout << "I passed, my name is:\t" << bSYS.name() << std::endl;
        tool->applySystematicVariation(bSYS);
        float bTagSF_var(-1.);
        // for tagged jets getScaleFactor
        if(sel_tool->accept(* ob)) {
          tool->getScaleFactor(* ob, bTagSF_var);
        }
        // for untagged jets getInefficiencyScaleFactor
        else {
          tool->getInefficiencyScaleFactor(* ob, bTagSF_var);
        }
        eigen_variation.push_back( bTagSF_var );
      }
      // Reset tool
      tool->applySystematicVariation(CP::SystematicSet());

      return eigen_variation;

    }


  }
}
