#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "WWZanalysis/MyxAODAnalysis.h"
#include "WWZanalysis/VertexSelection.h"
#include "WWZanalysis/JetSelection.h"
#include "WWZanalysis/BTaggingScaleFactors.h"
#include "WWZanalysis/AbsEtaRange.h"
#include "WWZanalysis/CorrectionsCalibrationsSmearing.h"
#include "WWZanalysis/MuonSelection.h"
#include "WWZanalysis/ElectronSelection.h"
#include "WWZanalysis/SkimmingInfo.h"
#include "WWZanalysis/EventData.h"
#include "WWZanalysis/Utilities.h"
#include "WWZanalysis/TriggerScaleFactors.h"
#include "WWZanalysis/SystematicVariations.h"
#include "WWZanalysis/METMaker.h"
#include "WWZanalysis/TruthSelector.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// ASG status code check
#include "AsgTools/MessageCheck.h"
#include "AsgTools/AnaToolHandle.h"

// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "xAODJet/JetContainer.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODCore/ShallowCopy.h"

#include "xAODBase/IParticleHelpers.h"

#include <TSystem.h> // used to define JERTool calibration path (you may already have this from the GRL part) 
#include <TFile.h>
// for retrieving of topocluster container
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

// for retrieving of PFO container
#include "xAODPFlow/PFO.h"
#include "xAODPFlow/PFOContainer.h"

#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODEgamma/EgammaTruthxAODHelpers.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "xAODMissingET/MissingETAuxContainer.h"
#include "METUtilities/METMaker.h"
#include "METUtilities/METSystematicsTool.h"

#include "EventLoop/OutputStream.h"
#include "PathResolver/PathResolver.h"
#include <fstream>
#include <string>
#include <sstream>
#include <valarray>

#include "AthContainers/ConstDataVector.h"

// this is needed to distribute the algorithm to the workers
ClassImp(WWZanalysis::MyxAODAnalysis)

namespace WWZanalysis {

  MyxAODAnalysis :: MyxAODAnalysis ()
  {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job& job)
  {
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.
  
    // let's initialize the algorithm to use the xAODRootAccess package
    job.useXAOD ();
  
    ANA_CHECK_SET_TYPE (EL::StatusCode);
    ANA_CHECK(xAOD::Init());
  
    // tell EventLoop about our output xAOD:
    // MOVED TO SUBMISSION SCRIPTS!
    //EL::OutputStream out ("outputLabel", "xAOD");
    //job.outputAdd (out);
  
    return EL::StatusCode::SUCCESS;
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: histInitialize ()
  {
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.


    // check CP TOOLS OPTIONS
    if (!m_opt_prw_confFiles.size())				{ std::cout << "ERROR: Empty PRW config file string... returning success status code" << std::endl; return EL::StatusCode::SUCCESS; }
    // check CP TOOLS OPTIONS

    // get the output file, create a new TTree and connect it to that output
    // define which branches will go in that tree
    TFile *outputFile = wk()->getOutputFile (m_outputName);
    Info("histInitialize()", "TTree name:\t%s", m_outputtree.c_str() );
    m_tree = new TTree (m_outputtree.c_str(), m_outputtree.c_str());
    //m_tree = new TTree ("nominal", "nominal");
    m_tree->SetDirectory (outputFile);
    // Is it the nominal tree?
    m_isnominalTTree = (m_outputtree.compare("nominal")==0) ? true : false;
    // Adding of systematic branches makes sense only
    // for the nominal TTree!
    m_doSystematics = m_isnominalTTree ? m_doSystematics : false;
    // event
    m_tree->Branch ("RunNumber"								, & m_runNumber 						);
    m_tree->Branch ("RunYear"								, & m_RunYear 							);
    m_tree->Branch ("MCchannelNumber"							, & m_mcChannelNumber 						);
    m_tree->Branch ("mcWeightOrg"							, & m_mcEventWeight 						);
    if (m_isnominalTTree) m_tree->Branch ("AllmcWeightOrg"				, & m_allMC_weights 						);
    m_tree->Branch ("xsec"								, & m_xsec	 						);
    m_tree->Branch ("totweight"								, & m_sum_of_weights_before_skimming				);
    m_tree->Branch ("tot_events"							, & m_number_of_events_before_skimming				);
    m_tree->Branch ("average_mu"							, & m_mu							);
    m_tree->Branch ("pileupEventWeight"							, & m_pileup_weight						);
    if (m_doSystematics) m_tree->Branch ("pileupEventWeight_UP"				, & m_pileup_weight_UP						);
    if (m_doSystematics) m_tree->Branch ("pileupEventWeight_DOWN"			, & m_pileup_weight_DOWN					);
    m_tree->Branch ("JVT_EventWeight"							, & m_JVT_EventWeight						);
    if (m_doSystematics) m_tree->Branch ("JVT_EventWeight_UP"				, & m_JVT_EventWeight_UP					);
    if (m_doSystematics) m_tree->Branch ("JVT_EventWeight_DOWN"				, & m_JVT_EventWeight_DOWN					);
    m_tree->Branch ("MV2c10_FixedCutBEff_70_EventWeight"				, & m_MV2c10_FixedCutBEff_70_EventWeight			);
    if (m_doSystematics) m_tree->Branch ("bTagSF_weight_MV2c10_FixedCutBEff_70_B_up"		, & m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_up		);
    if (m_doSystematics) m_tree->Branch ("bTagSF_weight_MV2c10_FixedCutBEff_70_B_down"		, & m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_down	);
    if (m_doSystematics) m_tree->Branch ("bTagSF_weight_MV2c10_FixedCutBEff_70_C_up"		, & m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_up		);
    if (m_doSystematics) m_tree->Branch ("bTagSF_weight_MV2c10_FixedCutBEff_70_C_down"		, & m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_down	);
    if (m_doSystematics) m_tree->Branch ("bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up"	, & m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up	);
    if (m_doSystematics) m_tree->Branch ("bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down"	, & m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down	);
    m_tree->Branch ("EventNumber"							, & m_EventNumber						);
    m_tree->Branch ("passTrigger"							, & m_passTrigger						);
    m_tree->Branch ("passEventCleaning"							, & m_passEventCleaning						);
    // trigger menus (lowest unprescaled triggers per data period)
    // 2015, 2016 and 2017
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_mu50"				, & m_HLT_mu50					);
    // 2015 only
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e24_lhmedium_L1EM20VH"		, & m_HLT_e24_lhmedium_L1EM20VH			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e60_lhmedium"			, & m_HLT_e60_lhmedium				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e120_lhloose"			, & m_HLT_e120_lhloose				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_mu20_iloose_L1MU15"			, & m_HLT_mu20_iloose_L1MU15			);
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_2e12_lhloose_L12EM10VH"		, & m_HLT_2e12_lhloose_L12EM10VH		);
    // if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_mu18_mu8noL1"			, & m_HLT_mu18_mu8noL1				);
    // if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e17_lhloose_mu14"			, & m_HLT_e17_lhloose_mu14			);
    // 2016 and 2017 
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_mu26_ivarmedium"			, & m_HLT_mu26_ivarmedium			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e26_lhtight_nod0_ivarloose"		, & m_HLT_e26_lhtight_nod0_ivarloose		);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e60_lhmedium_nod0"			, & m_HLT_e60_lhmedium_nod0			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e140_lhloose_nod0"			, & m_HLT_e140_lhloose_nod0			);
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_mu22_mu8noL1"			, & m_HLT_mu22_mu8noL1				);
    // if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e17_lhloose_nod0_mu14"		, & m_HLT_e17_lhloose_nod0_mu14			);
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // 2016 only
    // if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_2e17_lhvloose_nod0"			, & m_HLT_2e17_lhvloose_nod0			);
    // 2017 only 
    // if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_2e24_lhvloose_nod0"			, & m_HLT_2e24_lhvloose_nod0			);
    //
    // unused triggers
    //
    //if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e26_lhmedium_nod0_mu8noL1"		, & m_HLT_e26_lhmedium_nod0_mu8noL1				);
    //if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e7_lhmedium_nod0_mu24"		, & m_HLT_e7_lhmedium_nod0_mu24					);
    //if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_mu20_2mu4noL1"			, & m_HLT_mu20_2mu4noL1						);
    //if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH"		, & m_HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH );
    //if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("HLT_2mu14"				, & m_HLT_2mu14							);
    m_tree->Branch ("isTrigMatched"									, & m_isTrigMatched				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("has_PV"					, & m_has_PV					);
    m_tree->Branch ("total_charge"									, & m_tot_charge				);
    // truth event info
    m_tree->Branch ("higgsDecayMode"									, & m_higgsMode					);
    m_tree->Branch ("ZbosonDecayMode"									, & m_ZdecayMode				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("n_truthPromptLeps"			, & m_nTruthPromptLep				);
//    m_tree->Branch ("n_Wlep"								, & m_W_lep							);
//    m_tree->Branch ("n_Whad"								, & m_W_had							);
//    m_tree->Branch ("n_Zlep"								, & m_Z_lep							);
//    m_tree->Branch ("n_Zinv"								, & m_Z_inv							);
//    m_tree->Branch ("n_Zhad"								, & m_Z_had							);
    // inv masses
    m_tree->Branch ("Mllll0123"								, & m_Mllll0123							);
    m_tree->Branch ("Mlll012"								, & m_Mlll012							);
    m_tree->Branch ("Mlll013"								, & m_Mlll013							);
    m_tree->Branch ("Mlll023"								, & m_Mlll023							);
    m_tree->Branch ("Mlll123"								, & m_Mlll123							);
    m_tree->Branch ("Mll01"								, & m_Mll01							);
    m_tree->Branch ("Mll02"								, & m_Mll02							);
    m_tree->Branch ("Mll03"								, & m_Mll03							);
    m_tree->Branch ("Mll12"								, & m_Mll12							);
    m_tree->Branch ("Mll13"								, & m_Mll13							);
    m_tree->Branch ("Mll23"								, & m_Mll23							);
    m_tree->Branch ("best_Z_Mll"							, & m_best_Z_Mll						);
    m_tree->Branch ("best_Z_other_Mll"							, & m_best_Z_other_Mll						);
    m_tree->Branch ("Mjj01"								, & m_Mjj01							);
    m_tree->Branch ("Mjj02"								, & m_Mjj02							);
    m_tree->Branch ("Mjj03"								, & m_Mjj03							);
    m_tree->Branch ("Mjj04"								, & m_Mjj04							);
    m_tree->Branch ("Mjj05"								, & m_Mjj05							);
    m_tree->Branch ("Mjj12"								, & m_Mjj12 							);
    m_tree->Branch ("Mjj13"								, & m_Mjj13							);
    m_tree->Branch ("Mjj14"								, & m_Mjj14							);
    m_tree->Branch ("Mjj15"								, & m_Mjj15							);
    m_tree->Branch ("Mjj23"								, & m_Mjj23							);
    m_tree->Branch ("Mjj24"								, & m_Mjj24							);
    m_tree->Branch ("Mjj25"								, & m_Mjj25							);
    m_tree->Branch ("Mjj34"								, & m_Mjj34							);
    m_tree->Branch ("Mjj35"								, & m_Mjj35							);
    m_tree->Branch ("Mjj45"								, & m_Mjj45							);
    // event kinematics
    m_tree->Branch ("HT_lep"								, & m_HT_lep							);
    m_tree->Branch ("HT_had"								, & m_HT_had							);
    // MET
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("MET"			, & m_MET							);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("MET_phi"			, & m_MET_phi							);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("MET_x"			, & m_MET_x							);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("MET_y"			, & m_MET_y							);
    m_tree->Branch ("MET_RefFinal_et"							, & m_MET_RefFinal_et						);
    m_tree->Branch ("MET_RefFinal_phi"							, & m_MET_RefFinal_phi						);
    m_tree->Branch ("MET_RefFinal_x"							, & m_MET_RefFinal_x						);
    m_tree->Branch ("MET_RefFinal_y"							, & m_MET_RefFinal_y						);
    // jets
    m_tree->Branch ("jet_E"								, & m_vjet_E							);
    m_tree->Branch ("jet_pt"								, & m_vjet_pt 							);
    m_tree->Branch ("jet_eta"								, & m_vjet_eta							);
    m_tree->Branch ("jet_phi"								, & m_vjet_phi							);
    m_tree->Branch ("jet_jvt"								, & m_vjet_jvt							);
    m_tree->Branch ("jet_coneLabel"							, & m_vjet_coneLabel						);
    m_tree->Branch ("jet_mv2c10"							, & m_vjet_mv2c10						);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("jet_mv2c10mu"		, & m_vjet_mv2c10mu						);
    m_tree->Branch ("nJets_mv2c10_60"							, & m_nJets_mv2c10_60						); 
    m_tree->Branch ("nJets_mv2c10_70"							, & m_nJets_mv2c10_70						); 
    m_tree->Branch ("nJets_mv2c10_77"							, & m_nJets_mv2c10_77						); 
    m_tree->Branch ("nJets_mv2c10_85"							, & m_nJets_mv2c10_85						); 
    // leptons
    m_tree->Branch ("lepton_ID"								, & m_vlepton_ID						);
    m_tree->Branch ("lepton_E"								, & m_vlepton_E							);
    m_tree->Branch ("lepton_pt"								, & m_vlepton_pt						);
    m_tree->Branch ("lepton_eta"							, & m_vlepton_eta						);
    m_tree->Branch ("lepton_phi"							, & m_vlepton_phi						);
    m_tree->Branch ("lepton_q"								, & m_vlepton_q							);
    m_tree->Branch ("lepton_d0"								, & m_vlepton_d0						);
    m_tree->Branch ("lepton_z0"								, & m_vlepton_z0						);
    m_tree->Branch ("lepton_z0sinT"							, & m_vlepton_z0sinT						);
    m_tree->Branch ("lepton_truthType"							, & m_vlepton_truthType						);
    m_tree->Branch ("lepton_truthOrigin"						, & m_vlepton_truthOrigin					);
    m_tree->Branch ("lepton_sigd0"                    					, & m_vlepton_sigd0                           			);
    m_tree->Branch ("lepton_PromptLeptonVeto"                    			, & m_lepton_PromptLeptonVeto                 			);
    m_tree->Branch ("lepton_PromptLeptonIso_TagWeight"                 			, & m_lepton_PromptLeptonIso_TagWeight            		);
    m_tree->Branch ("lepton_isTightLH"                    				, & m_vlepton_isTightLH                 			);
    m_tree->Branch ("lepton_isTightID"                    				, & m_vlepton_isTightID                 			);
    m_tree->Branch ("lepton_chargeIDBDTTight"                  				, & m_vlepton_chargeIDBDTTight                 			);
    m_tree->Branch ("lepton_ambiguityType"                  				, & m_vlepton_ambiguityType                 			);
    m_tree->Branch ("lepton_isolationFixedCutLoose"                  			, & m_vlepton_isolationFixedCutLoose                 		);
    // lepton SFs
//    m_tree->Branch ("lepSFTrigLoose"                    				, & m_lepSFTrigLoose                        			);
    m_tree->Branch ("lepton_SFIDLoose"                    				, & m_vlepton_SFIDLoose                        			);
    m_tree->Branch ("lepton_SFIDTightLH"                  				, & m_vlepton_SFIDTightLH                        		);
    m_tree->Branch ("lepton_SFIsoLoose"                    				, & m_vlepton_SFIsoLoose                       			);
    m_tree->Branch ("lepton_SFIsoPLV"                    				, & m_vlepton_SFIsoPLV                       			);
    m_tree->Branch ("lepton_SFReco"                    					, & m_vlepton_SFReco                       			);
    m_tree->Branch ("lepton_SFTTVA"                    					, & m_vlepton_SFTTVA                       			);
    m_tree->Branch ("lepton_SFObjLoose"                    				, & m_vlepton_SFObjLoose                       			);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_EL_SF_ID_UP"     		, & m_vlepton_SFObjLoose_EL_SF_ID_UP                       	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_EL_SF_ID_DOWN"   		, & m_vlepton_SFObjLoose_EL_SF_ID_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_EL_SF_Reco_UP"     	, & m_vlepton_SFObjLoose_EL_SF_Reco_UP                       	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_EL_SF_Reco_DOWN"   	, & m_vlepton_SFObjLoose_EL_SF_Reco_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_EL_SF_Iso_UP"     		, & m_vlepton_SFObjLoose_EL_SF_Iso_UP                       	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_EL_SF_Iso_DOWN"   		, & m_vlepton_SFObjLoose_EL_SF_Iso_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_RECO_STAT_UP"   	, & m_vlepton_SFObjLoose_MU_SF_RECO_STAT_UP           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN"  	, & m_vlepton_SFObjLoose_MU_SF_RECO_STAT_DOWN           	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_RECO_SYST_UP"   	, & m_vlepton_SFObjLoose_MU_SF_RECO_SYST_UP     		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN"  	, & m_vlepton_SFObjLoose_MU_SF_RECO_SYST_DOWN          		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_Iso_STAT_UP"   	, & m_vlepton_SFObjLoose_MU_SF_Iso_STAT_UP           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_Iso_STAT_DOWN"  	, & m_vlepton_SFObjLoose_MU_SF_Iso_STAT_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_Iso_SYST_UP"   	, & m_vlepton_SFObjLoose_MU_SF_Iso_SYST_UP     			);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_Iso_SYST_DOWN"  	, & m_vlepton_SFObjLoose_MU_SF_Iso_SYST_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_TTVA_STAT_UP"   	, & m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_UP           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN"  	, & m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN           	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_TTVA_SYST_UP"   	, & m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_UP     		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN"  	, & m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN          		);
    m_tree->Branch ("lepton_SFObjTightLH"                  				, & m_vlepton_SFObjTightLH                    			);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTightLH_EL_SF_ID_UP"     	, & m_vlepton_SFObjTightLH_EL_SF_ID_UP                       	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTightLH_EL_SF_ID_DOWN"   	, & m_vlepton_SFObjTightLH_EL_SF_ID_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTightLH_EL_SF_Reco_UP"     	, & m_vlepton_SFObjTightLH_EL_SF_Reco_UP                       	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTightLH_EL_SF_Reco_DOWN"   	, & m_vlepton_SFObjTightLH_EL_SF_Reco_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTightLH_EL_SF_Iso_UP"     	, & m_vlepton_SFObjTightLH_EL_SF_Iso_UP                       	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTightLH_EL_SF_Iso_DOWN"   	, & m_vlepton_SFObjTightLH_EL_SF_Iso_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_RECO_STAT_UP"   	, & m_vlepton_SFObjTight_MU_SF_RECO_STAT_UP           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_RECO_STAT_DOWN" 	, & m_vlepton_SFObjTight_MU_SF_RECO_STAT_DOWN           	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_RECO_SYST_UP"   	, & m_vlepton_SFObjTight_MU_SF_RECO_SYST_UP     		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_RECO_SYST_DOWN" 	, & m_vlepton_SFObjTight_MU_SF_RECO_SYST_DOWN           	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_Iso_STAT_UP"   	, & m_vlepton_SFObjTight_MU_SF_Iso_STAT_UP           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_Iso_STAT_DOWN"  	, & m_vlepton_SFObjTight_MU_SF_Iso_STAT_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_Iso_SYST_UP"   	, & m_vlepton_SFObjTight_MU_SF_Iso_SYST_UP     			);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_Iso_SYST_DOWN"  	, & m_vlepton_SFObjTight_MU_SF_Iso_SYST_DOWN           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_TTVA_STAT_UP"   	, & m_vlepton_SFObjTight_MU_SF_TTVA_STAT_UP           		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_TTVA_STAT_DOWN" 	, & m_vlepton_SFObjTight_MU_SF_TTVA_STAT_DOWN           	);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_TTVA_SYST_UP"   	, & m_vlepton_SFObjTight_MU_SF_TTVA_SYST_UP     		);
    if (m_doSystematics)  m_tree->Branch ("lepton_SFObjTight_MU_SF_TTVA_SYST_DOWN" 	, & m_vlepton_SFObjTight_MU_SF_TTVA_SYST_DOWN           	);
    // muons
    m_tree->Branch ("muon_E"								, & m_vmuon_E							);
    m_tree->Branch ("muon_pt"								, & m_vmuon_pt							);
    m_tree->Branch ("muon_eta"								, & m_vmuon_eta							);
    m_tree->Branch ("muon_phi"								, & m_vmuon_phi							);
    m_tree->Branch ("muon_q"								, & m_vmuon_q							);
    m_tree->Branch ("muon_d0"								, & m_vmuon_d0							);
    m_tree->Branch ("muon_z0"								, & m_vmuon_z0							);
    m_tree->Branch ("muon_z0sinT"							, & m_vmuon_z0sinT						);
    m_tree->Branch ("muon_truthType"							, & m_vmuon_truthType						);
    m_tree->Branch ("muon_truthOrigin"							, & m_vmuon_truthOrigin						);
    m_tree->Branch ("muon_sigd0"                    					, & m_vmuon_sigd0                           			);
    //m_tree->Branch ("muon_sigz0"                    					, & m_vmuon_sigz0                           			);
    // electrons
    m_tree->Branch ("electron_E"							, & m_velectron_E						);
    m_tree->Branch ("electron_pt"							, & m_velectron_pt						);
    m_tree->Branch ("electron_eta"							, & m_velectron_eta						);
    m_tree->Branch ("electron_phi"							, & m_velectron_phi						);
    m_tree->Branch ("electron_q"							, & m_velectron_q						);
    m_tree->Branch ("electron_d0"							, & m_velectron_d0						);
    m_tree->Branch ("electron_z0"							, & m_velectron_z0						);
    m_tree->Branch ("electron_z0sinT"							, & m_velectron_z0sinT						);
    m_tree->Branch ("electron_truthType"              					, & m_velectron_truthType                     			);
    m_tree->Branch ("electron_truthOrigin"            					, & m_velectron_truthOrigin                   			);
    m_tree->Branch ("electron_sigd0"         						, & m_velectron_sigd0						);
    //m_tree->Branch ("electron_sigz0"           					, & m_velectron_sigz0						);
    // jets
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_jet_E"				, & m_beforeOLR_vjet_E				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_jet_pt"			, & m_beforeOLR_vjet_pt 			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_jet_eta"			, & m_beforeOLR_vjet_eta			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_jet_phi"			, & m_beforeOLR_vjet_phi			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_jet_jvt"			, & m_beforeOLR_vjet_jvt			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_jet_coneLabel"			, & m_beforeOLR_vjet_coneLabel			);
    // forward jets
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("forward_jet_E"				, & m_forward_vjet_E				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("forward_jet_pt"				, & m_forward_vjet_pt 				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("forward_jet_eta"				, & m_forward_vjet_eta				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("forward_jet_phi"				, & m_forward_vjet_phi				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("forward_jet_jvt"				, & m_forward_vjet_jvt				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("forward_jet_coneLabel"			, & m_forward_vjet_coneLabel			);
    // muons
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_muon_E"			, & m_beforeOLR_vmuon_E				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_muon_pt"			, & m_beforeOLR_vmuon_pt			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_muon_eta"			, & m_beforeOLR_vmuon_eta			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_muon_phi"			, & m_beforeOLR_vmuon_phi			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_muon_q"			, & m_beforeOLR_vmuon_q				);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_muon_d0"			, & m_beforeOLR_vmuon_d0			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_muon_z0"			, & m_beforeOLR_vmuon_z0			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_muon_z0sinT"			, & m_beforeOLR_vmuon_z0sinT			);
    // electrons
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_electron_E"			, & m_beforeOLR_velectron_E			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_electron_pt"			, & m_beforeOLR_velectron_pt			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_electron_eta"			, & m_beforeOLR_velectron_eta			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_electron_phi"			, & m_beforeOLR_velectron_phi			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_electron_q"			, & m_beforeOLR_velectron_q			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_electron_d0"			, & m_beforeOLR_velectron_d0			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_electron_z0"			, & m_beforeOLR_velectron_z0			);
    if (!m_doSystematics && m_isnominalTTree) m_tree->Branch ("beforeOLR_electron_z0sinT"		, & m_beforeOLR_velectron_z0sinT		);
    
    // histograms
    // Do not duplicate total events histogram, as eventually 
    // nominal and systematic ntuples will be hadded
    if (m_isnominalTTree) {
      h_total_events = new TH1F("h_total_events", "h_total_events", 1001, -0.5, 1000.5);
      h_total_events->SetDirectory (outputFile);
    }
    /*
    h_jetPt  	= new TH1F("h_jetPt", "h_jetPt", 100, 0, 500); // jet pt [GeV]
    h_jeteta 	= new TH1F("h_jeteta", "h_jeteta", 100, -5, 5); // jet eta
    h_jetphi 	= new TH1F("h_jetphi", "h_jeteta", 100, -3.2, 3.2); // jet phi
    wk()->addOutput (h_jetPt);
    wk()->addOutput (h_jeteta);
    wk()->addOutput (h_jetphi);
    */  

    return EL::StatusCode::SUCCESS;
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: fileExecute ()
  {
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed

    m_number_of_events_before_skimming	= 0;
    m_sum_of_weights_before_skimming	= 0; 
    m_allSUM_of_weights_before_skimming = {};

    // Accumulate number of events before skimming (with and without weights) in each input file!
    // They'll be put into a counter histogram at the end of the run.
    xAOD::TEvent * event = wk()->xaodEvent();
    m_theres_cutbookkeeper = false;
    try {
      m_number_of_events_before_skimming += SkimmingInfo::number_of_events_before_skimming(event);
      m_sum_of_weights_before_skimming += SkimmingInfo::sum_of_weights_before_skimming(event);
      m_theres_cutbookkeeper = true;
      if (m_debugMode) std::cout << "number_of_events_before_skimming: 	"<< m_number_of_events_before_skimming << std::endl;
      if (m_debugMode) std::cout << "sum_of_weights_before_skimming: 	"<< m_sum_of_weights_before_skimming << std::endl;
    }
    catch (std::runtime_error const & e) {
      Warning("fileExecute()", "HEY HEY HEY!!! No cutbookkeeper found! Will get wrong number of events before skimming");
    }
 
    return EL::StatusCode::SUCCESS;
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: changeInput (bool firstFile)
  {
    // Here you do everything you need to do when we change input files,
    // e.g. resetting branch addresses on trees.  If you are using
    // D3PDReader or a similar service this method is not needed.
    return EL::StatusCode::SUCCESS;
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: initialize ()
  {
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.
  
    ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  
    xAOD::TEvent* event = wk()->xaodEvent();
    // Retrieve Event info
    const xAOD::EventInfo* eventInfo = nullptr;
    ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));
    bool isData = !(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION )); //bool describing if the events are data or from simulation
    // Switch off systematic variations when running on data
    m_doSystematics = isData ? false : m_doSystematics;
    if ( m_debugMode ) std::cout << "isData:           " << isData << std::endl;
  
    std::cout << "m_theres_cutbookkeeper = 	" << m_theres_cutbookkeeper << std::endl; 

    // as a check, let's see the number of events in our xAOD
    Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int
  
    // count number of events
    m_eventCounter 			= 0;

    // debug mode
    //m_debugMode				= false;//true;
  
    // TTree branches
    // event
    m_mcChannelNumber					= 0;
    m_xsec						= 0;
    m_mcEventWeight					= 0;
    m_allMC_weights					= {};
    m_ZdecayMode					= {};
    // jets
    m_vjet_E						= {};
    m_vjet_pt						= {};
    m_vjet_eta						= {};
    m_vjet_phi						= {};
    m_vjet_jvt						= {};
    m_vjet_coneLabel					= {};
    m_vjet_mv2c10					= {};
    m_vjet_mv2c10mu					= {};
    // leptons
    m_vlepton_ID					= {};
    m_vlepton_E						= {};
    m_vlepton_pt					= {};
    m_vlepton_eta					= {};
    m_vlepton_phi					= {};
    m_vlepton_q						= {};
    m_vlepton_d0					= {};
    m_vlepton_z0					= {};
    m_vlepton_z0sinT					= {};
    m_vlepton_truthType					= {}; 
    m_vlepton_truthOrigin				= {}; 
    m_vlepton_sigd0                   			= {};
    m_lepton_PromptLeptonVeto				= {};
    m_lepton_PromptLeptonIso_TagWeight			= {};
    m_vlepton_isTightLH 				= {};
    m_vlepton_isTightID 				= {};
    m_vlepton_chargeIDBDTTight				= {};
    m_vlepton_ambiguityType				= {};
    m_vlepton_isolationFixedCutLoose			= {};
    // lepton SFs
//    m_lepSFTrigLoose					= 0;
    m_vlepton_SFIDLoose                			= {};
    m_vlepton_SFIDTightLH                		= {};
    m_vlepton_SFIsoLoose               			= {};
    m_vlepton_SFIsoPLV					= {};
    m_vlepton_SFReco					= {};
    m_vlepton_SFTTVA					= {};
    m_vlepton_SFObjLoose				= {};
    m_vlepton_SFObjTightLH				= {};
    m_vlepton_SFObjLoose_EL_SF_ID_UP   			= {};
    m_vlepton_SFObjLoose_EL_SF_ID_DOWN 			= {};
    m_vlepton_SFObjLoose_EL_SF_Reco_UP   		= {};
    m_vlepton_SFObjLoose_EL_SF_Reco_DOWN 		= {};
    m_vlepton_SFObjLoose_EL_SF_Iso_UP   		= {};
    m_vlepton_SFObjLoose_EL_SF_Iso_DOWN 		= {};
    m_vlepton_SFObjLoose_MU_SF_RECO_STAT_UP 		= {};
    m_vlepton_SFObjLoose_MU_SF_RECO_STAT_DOWN		= {};
    m_vlepton_SFObjLoose_MU_SF_RECO_SYST_UP 		= {};
    m_vlepton_SFObjLoose_MU_SF_RECO_SYST_DOWN		= {};
    m_vlepton_SFObjLoose_MU_SF_Iso_STAT_UP 		= {};
    m_vlepton_SFObjLoose_MU_SF_Iso_STAT_DOWN		= {};
    m_vlepton_SFObjLoose_MU_SF_Iso_SYST_UP 		= {};
    m_vlepton_SFObjLoose_MU_SF_Iso_SYST_DOWN		= {};
    m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_UP 		= {};
    m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN		= {};
    m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_UP 		= {};
    m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN		= {};
    m_vlepton_SFObjTightLH				= {};
    m_vlepton_SFObjTightLH_EL_SF_ID_UP   		= {};
    m_vlepton_SFObjTightLH_EL_SF_ID_DOWN 		= {};
    m_vlepton_SFObjTightLH_EL_SF_Reco_UP   		= {};
    m_vlepton_SFObjTightLH_EL_SF_Reco_DOWN 		= {};
    m_vlepton_SFObjTightLH_EL_SF_Iso_UP   		= {};
    m_vlepton_SFObjTightLH_EL_SF_Iso_DOWN 		= {};
    m_vlepton_SFObjTight_MU_SF_RECO_STAT_UP 		= {};
    m_vlepton_SFObjTight_MU_SF_RECO_STAT_DOWN		= {};
    m_vlepton_SFObjTight_MU_SF_RECO_SYST_UP 		= {};
    m_vlepton_SFObjTight_MU_SF_RECO_SYST_DOWN		= {};
    m_vlepton_SFObjTight_MU_SF_Iso_STAT_UP 		= {};
    m_vlepton_SFObjTight_MU_SF_Iso_STAT_DOWN		= {};
    m_vlepton_SFObjTight_MU_SF_Iso_SYST_UP 		= {};
    m_vlepton_SFObjTight_MU_SF_Iso_SYST_DOWN		= {};
    m_vlepton_SFObjTight_MU_SF_TTVA_STAT_UP 		= {};
    m_vlepton_SFObjTight_MU_SF_TTVA_STAT_DOWN		= {};
    m_vlepton_SFObjTight_MU_SF_TTVA_SYST_UP 		= {};
    m_vlepton_SFObjTight_MU_SF_TTVA_SYST_DOWN		= {};
    // muons
    m_vmuon_E						= {};
    m_vmuon_pt						= {};
    m_vmuon_eta						= {};
    m_vmuon_phi						= {};
    m_vmuon_q						= {};
    m_vmuon_d0						= {};
    m_vmuon_z0						= {};
    m_vmuon_z0sinT					= {};
    m_vmuon_truthType					= {}; 
    m_vmuon_truthOrigin					= {}; 
    m_vmuon_sigd0                   			= {};
    //m_vmuon_sigz0                   			= {};
    // electrons
    m_velectron_E					= {};
    m_velectron_pt					= {};
    m_velectron_eta					= {};
    m_velectron_phi					= {};
    m_velectron_q					= {};
    m_velectron_d0					= {};
    m_velectron_z0					= {};
    m_velectron_z0sinT					= {};
    m_velectron_truthType             			= {};
    m_velectron_truthOrigin         			= {};
    m_velectron_sigd0					= {}; 	
    //m_velectron_sigz0					= {};	
    // jets
    m_beforeOLR_vjet_E					= {};
    m_beforeOLR_vjet_pt					= {};
    m_beforeOLR_vjet_eta				= {};
    m_beforeOLR_vjet_phi				= {};
    m_beforeOLR_vjet_jvt				= {};
    m_beforeOLR_vjet_coneLabel				= {};
    // forward jets
    m_forward_vjet_E					= {};
    m_forward_vjet_pt					= {};
    m_forward_vjet_eta					= {};
    m_forward_vjet_phi					= {};
    m_forward_vjet_jvt					= {};
    m_forward_vjet_coneLabel				= {};
    // muons
    m_beforeOLR_vmuon_E					= {};
    m_beforeOLR_vmuon_pt				= {};
    m_beforeOLR_vmuon_eta				= {};
    m_beforeOLR_vmuon_phi				= {};
    m_beforeOLR_vmuon_q					= {};
    m_beforeOLR_vmuon_d0				= {};
    m_beforeOLR_vmuon_z0				= {};
    m_beforeOLR_vmuon_z0sinT				= {};
    // electrons
    m_beforeOLR_velectron_E				= {};
    m_beforeOLR_velectron_pt				= {};
    m_beforeOLR_velectron_eta				= {};
    m_beforeOLR_velectron_phi				= {};
    m_beforeOLR_velectron_q				= {};
    m_beforeOLR_velectron_d0				= {};
    m_beforeOLR_velectron_z0				= {};
    m_beforeOLR_velectron_z0sinT			= {};
    // b-tagging systematics
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_up		= {};
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_down	= {}; 
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_up		= {}; 
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_down	= {}; 
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up	= {}; 
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down	= {}; 
 
    

    //----------------------------
    // EVENT
    //---------------------------

    if (!m_isAODfile) {
      m_PileupReweightingTool = new CP::PileupReweightingTool("PileupReweightingTool"+m_outputName);
      ANA_CHECK( m_PileupReweightingTool->setProperty( "ConfigFiles"		,m_opt_prw_confFiles) );
      ANA_CHECK( m_PileupReweightingTool->setProperty( "LumiCalcFiles"		,m_opt_prw_lcalcFiles) );
      m_PileupReweightingTool->msg().setLevel(MSG::DEBUG);
      ANA_CHECK( m_PileupReweightingTool->initialize() );
    }

    m_GoodRunsListSelectionTool = new GoodRunsListSelectionTool("GoodRunsListSelectionTool"+m_outputName);
    std::vector<std::string> vecStringGRL;
    ANA_CHECK( m_GoodRunsListSelectionTool->setProperty( "GoodRunsListVec"	,m_opt_grl_GoodRunsList) );
    ANA_CHECK( m_GoodRunsListSelectionTool->setProperty( "PassThrough"		,false) ); 
    ANA_CHECK( m_GoodRunsListSelectionTool->initialize() );

    m_xAODConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"+m_outputName);
    ANA_CHECK( m_xAODConfigTool->initialize() );
    ToolHandle< TrigConf::ITrigConfigTool > handle(m_xAODConfigTool);
    m_TrigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool"+m_outputName);
    ANA_CHECK( m_TrigDecisionTool->setProperty( "ConfigTool"			,handle ) );
    ANA_CHECK( m_TrigDecisionTool->setProperty( "TrigDecisionKey"		,"xTrigDecision" ) );
    ANA_CHECK( m_TrigDecisionTool->setProperty( "AcceptMultipleInstance"     	,true ) );
    ANA_CHECK( m_TrigDecisionTool->initialize() );

    m_MatchingTool.setTypeAndName("Trig::MatchingTool");
    ANA_CHECK( m_MatchingTool.setProperty( "TrigDecisionTool"             	,"TrigDecisionTool"+m_outputName) );
    ANA_CHECK( m_MatchingTool.retrieve() );


    // initialize and configure the event cleaning tool
//    m_EventCleaningTool = new ECUtils::EventCleaningTool("EventCleaningTool");
//    ANA_CHECK( m_EventCleaningTool->setProperty("PtCut" 			,25000.0) );
//    ANA_CHECK( m_EventCleaningTool->setProperty("EtaCut" 			,2.4) );
//    ANA_CHECK( m_EventCleaningTool->setProperty("JvtDecorator" 			,"JVTName") ); //corrected JVT is JVTName
//    ANA_CHECK( m_EventCleaningTool->setProperty("OrDecorator" 			,"overlaps") ); //see ORUtils::ToolBox below...
//    ANA_CHECK( m_EventCleaningTool->setProperty("CleaningLevel" 		,"LooseBad") );
//    ANA_CHECK( m_EventCleaningTool->initialize() );


    m_OLR = new ORUtils::ToolBox("ToolBox"+m_outputName);
    ORUtils::ORFlags olrFlags("ToolBox", "include_in_OLR", "overlaps");
    olrFlags.doElectrons 	= true;
    olrFlags.doMuons 		= true;
    olrFlags.doJets 		= true;
    olrFlags.doTaus 		= false;
    olrFlags.doPhotons 		= false;
    ANA_CHECK( ORUtils::recommendedTools(olrFlags, *m_OLR) ); 
    ANA_CHECK( m_OLR->initialize() );

    // MET
    ASG_SET_ANA_TOOL_TYPE(metMaker, met::METMaker);
    metMaker.setTypeAndName("met::METMaker");
    ANA_CHECK( metMaker.setProperty("JetSelection"			,"Tight") );
    ANA_CHECK( metMaker.setProperty("DoMuonEloss"			,true) );
    ANA_CHECK( metMaker.setProperty("DoRemoveMuonJets"			,true) );
    ANA_CHECK( metMaker.setProperty("DoSetMuonJetEMScale"		,true) );
    ANA_CHECK( metMaker.retrieve() );
    // MET calibration
    ASG_SET_ANA_TOOL_TYPE(metSystTool, met::METSystematicsTool);
    metSystTool.setTypeAndName("met::METSystematicsTool");
    ANA_CHECK( metSystTool.retrieve() );

    //----------------------------
    // EVENT
    //---------------------------



    //----------------------------
    // JETS
    //---------------------------
    // initialize and configure the jet cleaning tool
    m_jetCleaning = new JetCleaningTool("JetCleaning"+m_outputName);
    ANA_CHECK( m_jetCleaning->setProperty( "CutLevel"			,m_opt_jclean_CutLevel) );
    ANA_CHECK( m_jetCleaning->setProperty( "DoUgly"			,m_opt_jclean_DoUgly) );
    ANA_CHECK( m_jetCleaning->initialize() );
    // jet calibration
    // FIXME: JetArea_Residual_Origin_EtaJES_GSC_Insitu (which is for data) not found
    TString calibSeq 	= isData ? m_opt_jcal_CalibSequence[1] : m_opt_jcal_CalibSequence[0]; //String describing the calibration sequence to apply
    m_JetCalibrationTool  = new JetCalibrationTool("JetCalibrationTool"+m_outputName);
    ANA_CHECK( m_JetCalibrationTool->setProperty( "JetCollection"	,m_opt_jcal_JetCollection) );
    ANA_CHECK( m_JetCalibrationTool->setProperty( "ConfigFile"		,m_opt_jcal_ConfigFile) );
    ANA_CHECK( m_JetCalibrationTool->setProperty( "CalibSequence"	,calibSeq.Data()) );
    // Please note that from 21.2.10 onwards, a new property must be set (CalibArea). The current recommendation is to use "00-04-81", see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ApplyJetCalibrationR21#Calibration_of_Large_R_jet_colle
    ANA_CHECK( m_JetCalibrationTool->setProperty( "CalibArea"		,"00-04-81") );
    ANA_CHECK( m_JetCalibrationTool->setProperty( "IsData"		,isData) );
    ANA_CHECK( m_JetCalibrationTool->initialize() );
    // JES
    m_JetUncertaintiesTool = new JetUncertaintiesTool("JetUncertaintiesTool"+m_outputName);
    ANA_CHECK( m_JetUncertaintiesTool->setProperty( "JetDefinition"	,m_opt_jcal_JetCollection) );
    ANA_CHECK( m_JetUncertaintiesTool->setProperty( "MCType"		,m_opt_junc_MCType) );//"MC15" or "MC16") );
    ANA_CHECK( m_JetUncertaintiesTool->setProperty( "ConfigFile"	,m_opt_junc_ConfigFile) );
    ANA_CHECK( m_JetUncertaintiesTool->setProperty( "CalibArea"		,"CalibArea-04") );
    ANA_CHECK( m_JetUncertaintiesTool->initialize() );

    // instantiate and initialize the JER
    // JER
    m_JERTool = new JERTool("JERTool"+m_outputName);
    ANA_CHECK( m_JERTool->setProperty( "CollectionName"			,m_opt_jcal_JetCollection) );
    ANA_CHECK( m_JERTool->setProperty( "PlotFileName"			,m_opt_jer_PlotFileName) );
    ANA_CHECK( m_JERTool->initialize() );
    m_JERSmearingTool = new JERSmearingTool("JERSmearingTool"+m_outputName);
    ToolHandle<IJERTool> jerHandle(m_JERTool->name());
    ANA_CHECK( m_JERSmearingTool->setProperty( "JERTool"		,jerHandle) );
    ANA_CHECK( m_JERSmearingTool->setProperty( "isMC"			,!isData) );
    ANA_CHECK( m_JERSmearingTool->setProperty( "ApplyNominalSmearing"	,m_opt_jersmear_ApplyNominalSmearing) );
    ANA_CHECK( m_JERSmearingTool->setProperty( "SystematicMode"		,m_opt_jersmear_SystematicMode) );
    ANA_CHECK( m_JERSmearingTool->initialize() );
    // JVT
    m_JetVertexTaggerTool = new JetVertexTaggerTool("JetVertexTaggerTool"+m_outputName);
    ANA_CHECK( m_JetVertexTaggerTool->initialize() );
 
    m_JetCaloEnergies = new JetCaloEnergies("JetCaloEnergies"+m_outputName);
    ANA_CHECK(m_JetCaloEnergies->initialize());

    m_JetVertexTaggerToolSF = new CP::JetJvtEfficiency("JetVertexTaggerToolSF"+m_outputName);
    ANA_CHECK( m_JetVertexTaggerToolSF->setProperty("WorkingPoint"	,m_opt_jvtSF_WP) ); //0.59
    ANA_CHECK( m_JetVertexTaggerToolSF->setProperty("SFFile"		,m_opt_jvtSF_SFFile) );
    ANA_CHECK( m_JetVertexTaggerToolSF->initialize() );

    //----------------------------
    // JETS
    //---------------------------



    //----------------------------
    // MUONS
    //---------------------------
    

    // isolation is the same tool for both mu and el
    // iso reqs on loose leptons
    m_IsolationSelectionTool = new CP::IsolationSelectionTool("IsolationSelectionTool"+m_outputName);
    m_IsolationSelectionTool->msg().setLevel(MSG::DEBUG);
    ANA_CHECK( m_IsolationSelectionTool->setProperty("MuonWP"					,m_opt_isosel_MuonWP) );//"Loose", "Gradient"...) );
    // Electrons
    ANA_CHECK( m_IsolationSelectionTool->setProperty("ElectronWP"				,m_opt_isosel_ElectronWP) );//"FixedCutTight") );//"Gradient") );
    ANA_CHECK( m_IsolationSelectionTool->initialize() );

    // iso reqs stored into dedicated branches
    std::string FixedCutLoose_WP("FixedCutLoose");
    m_IsolationSelectionTool_FixedCutLoose = new CP::IsolationSelectionTool("IsolationSelectionTool_FixedCutLoose"+m_outputName);
    m_IsolationSelectionTool_FixedCutLoose->msg().setLevel(MSG::DEBUG);
    ANA_CHECK( m_IsolationSelectionTool_FixedCutLoose->setProperty("MuonWP"			,FixedCutLoose_WP) );
    // Electrons
    ANA_CHECK( m_IsolationSelectionTool_FixedCutLoose->setProperty("ElectronWP"			,FixedCutLoose_WP) );
    ANA_CHECK( m_IsolationSelectionTool_FixedCutLoose->initialize() );

    m_MuonSelectionTool_looseID = new CP::MuonSelectionTool("MuonSelectionTool_looseID"+m_outputName);
    ANA_CHECK( m_MuonSelectionTool_looseID->setProperty( "MaxEta"					,m_opt_musel_MaxEta ) );
    ANA_CHECK( m_MuonSelectionTool_looseID->setProperty( "MuQuality"					,m_opt_musel_MuQuality) ); //enum {Tight, Medium, Loose, VeryLoose}, corresponding to 0, 1, 2 and 3
    ANA_CHECK( m_MuonSelectionTool_looseID->initialize() );    
    // initialize the muon calibration and smearing tool
    m_MuonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool("MuonCalibrationAndSmearingTool"+m_outputName);
    ANA_CHECK( m_MuonCalibrationAndSmearingTool->initialize() );    

    // Muon Tight ID
    m_MuonSelectionTool = new CP::MuonSelectionTool("MuonSelectionTool"+m_outputName);
    ANA_CHECK( m_MuonSelectionTool->setProperty( "MaxEta"					,m_opt_musel_MaxEta ) );
    ANA_CHECK( m_MuonSelectionTool->setProperty( "MuQuality"					,0) ); //enum {Tight, Medium, Loose, VeryLoose}, corresponding to 0, 1, 2 and 3
    ANA_CHECK( m_MuonSelectionTool->initialize() );    

    // scale factors
    m_MuonEfficiencyScaleFactors_ID = new CP::MuonEfficiencyScaleFactors("MuonEfficiencyScaleFactors_ID"+m_outputName);
    ANA_CHECK( m_MuonEfficiencyScaleFactors_ID->setProperty("WorkingPoint"			,m_opt_muidSF_WP) );
    if (m_debugMode) m_MuonEfficiencyScaleFactors_ID->msg().setLevel(MSG::DEBUG);
    ANA_CHECK( m_MuonEfficiencyScaleFactors_ID->initialize() );

    m_MuonEfficiencyScaleFactors_isolation = new CP::MuonEfficiencyScaleFactors("MuonEfficiencyScaleFactors_isolation"+m_outputName);
    ANA_CHECK( m_MuonEfficiencyScaleFactors_isolation->setProperty("WorkingPoint"          	,m_opt_muisoSF_WP) );
    if (m_debugMode) m_MuonEfficiencyScaleFactors_isolation->msg().setLevel(MSG::DEBUG);
    ANA_CHECK( m_MuonEfficiencyScaleFactors_isolation->initialize() );

    m_MuonEfficiencyScaleFactors_TTVA = new CP::MuonEfficiencyScaleFactors("MuonEfficiencyScaleFactors_TTVA"+m_outputName);
    ANA_CHECK( m_MuonEfficiencyScaleFactors_TTVA->setProperty("WorkingPoint"          		,m_opt_muttvaSF_WP) );
    if (m_debugMode) m_MuonEfficiencyScaleFactors_TTVA->msg().setLevel(MSG::DEBUG);
    ANA_CHECK( m_MuonEfficiencyScaleFactors_TTVA->initialize() );

    // Custom MuonEfficiencyScaleFactors devoted to calibration of PLV
    m_MuonEfficiencyScaleFactors_PLV = new CP::MuonEfficiencyScaleFactors("CP::MuonEfficiencyScaleFactors_PLV"+m_outputName);
    //if (m_debugMode) m_MuonEfficiencyScaleFactors_PLV->msg().setLevel(MSG::DEBUG);
    ANA_CHECK( m_MuonEfficiencyScaleFactors_PLV->setProperty("WorkingPoint"          		,"PromptLeptonIso") );
    ANA_CHECK( m_MuonEfficiencyScaleFactors_PLV->setProperty("CustomInputFolder"		,m_PLV_calfolder) );
    ANA_CHECK( m_MuonEfficiencyScaleFactors_PLV->setProperty("CustomFileCombined"		,"Iso_mptLeptonVetoIso_Z.root") );
    ANA_CHECK( m_MuonEfficiencyScaleFactors_PLV->initialize() );

    m_MuonTriggerScaleFactors = new CP::MuonTriggerScaleFactors("MuonTriggerScaleFactors"+m_outputName);
    ANA_CHECK( m_MuonTriggerScaleFactors->setProperty("MuonQuality"				,m_opt_mutrigSF_MuonQuality) );
    // FIXME: allowing 0 SFs since no SF is available for 2017 data yet
    ANA_CHECK( m_MuonTriggerScaleFactors->setProperty("AllowZeroSF"				,m_opt_mutrigSF_AllowZeroSF) );
    ANA_CHECK( m_MuonTriggerScaleFactors->initialize() );    

    //----------------------------
    // MUONS
    //---------------------------



    //----------------------------
    // ELECTRONS
    //---------------------------
 
    m_EgammaCalibrationAndSmearingTool = new CP::EgammaCalibrationAndSmearingTool("EgammaCalibrationAndSmearingTool"+m_outputName); 
    //m_EgammaCalibrationAndSmearingTool->msg().setLevel(MSG::DEBUG);
    if(m_isAODfile)  ANA_CHECK( m_EgammaCalibrationAndSmearingTool->setProperty("randomRunNumber" 			,123456) );
    ANA_CHECK( m_EgammaCalibrationAndSmearingTool->setProperty("ESModel"				,m_opt_elcal_ESModel) ); 
    ANA_CHECK( m_EgammaCalibrationAndSmearingTool->setProperty("decorrelationModel"			,m_opt_elcal_decorrelationModel) ); 
    ANA_CHECK( m_EgammaCalibrationAndSmearingTool->initialize() );

    // Electron LH is not supposed to be computed by analyses FWs anymore, but retrieved from DAODs, see https://groups.cern.ch/group/hn-atlas-EGammaWG/Lists/Archive/Flat.aspx?RootFolder=%2fgroup%2fhn%2datlas%2dEGammaWG%2fLists%2fArchive%2fMissing%20likelihood%20variables%20in%20default%20derivation%20lists&FolderCTID=0x01200200D17DAC8476E41D4F8451088E75A5CE34
    m_AsgElectronLikelihoodTool_looseID = new AsgElectronLikelihoodTool("AsgElectronLikelihoodTool_looseID"+m_outputName);
    //m_AsgElectronLikelihoodTool_looseID->msg().setLevel(MSG::DEBUG);
    ANA_CHECK( m_AsgElectronLikelihoodTool_looseID->setProperty("primaryVertexContainer"		,"PrimaryVertices"));
    ANA_CHECK( m_AsgElectronLikelihoodTool_looseID->setProperty("WorkingPoint"				,m_opt_elLH_WorkingPoint));
    ANA_CHECK( m_AsgElectronLikelihoodTool_looseID->setProperty("ConfigFile"				,m_opt_elLH_ConfigFile));
    ANA_CHECK( m_AsgElectronLikelihoodTool_looseID->initialize() );

    // Electron Tight LH WP
    m_AsgElectronLikelihoodTool = new AsgElectronLikelihoodTool("AsgElectronLikelihoodTool"+m_outputName);
    //m_AsgElectronLikelihoodTool->msg().setLevel(MSG::DEBUG);
    ANA_CHECK( m_AsgElectronLikelihoodTool->setProperty("primaryVertexContainer"		,"PrimaryVertices"));
    ANA_CHECK( m_AsgElectronLikelihoodTool->setProperty("WorkingPoint"				,"TightLHElectron"));
    ANA_CHECK( m_AsgElectronLikelihoodTool->setProperty("ConfigFile"				,"ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodTightOfflineConfig2017_Smooth.conf"));
    ANA_CHECK( m_AsgElectronLikelihoodTool->initialize() );
 
    m_AsgElectronChargeIDSelectorTool = new AsgElectronChargeIDSelectorTool("AsgElectronChargeIDSelectorTool_tight"+m_outputName);
    ANA_CHECK( m_AsgElectronChargeIDSelectorTool->setProperty("TrainingFile"			,"ElectronPhotonSelectorTools/ChargeID/ECIDS_20161125for2017Moriond.root"));
    ANA_CHECK( m_AsgElectronChargeIDSelectorTool->initialize() );

    // Application of iso corrections on electrons is currently (r21.2.25) not needed, see https://its.cern.ch/jira/browse/ATLEGAMDPD-75
//    m_IsolationCorrectionTool = new CP::IsolationCorrectionTool("IsolationCorrectionTool");
//    //m_IsolationCorrectionTool->msg().setLevel(MSG::DEBUG);
//    ANA_CHECK( m_IsolationCorrectionTool->setProperty( "IsMC"						,!isData) );
//    ANA_CHECK( m_IsolationCorrectionTool->initialize() );

    // scale factors
    // Loose LH ID
    m_AsgElectronEfficiencyCorrectionTool_ID = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_ID"+m_outputName);
    //m_AsgElectronEfficiencyCorrectionTool_ID->msg().setLevel(MSG::DEBUG);
    // Recommendations here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2#LH_electron_identification
    std::vector<std::string> inputFileID{m_opt_elidSF_input} ;
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_ID->setProperty("CorrectionFileNameList"		,inputFileID) );
    // FIXME: looks like ForceDataType=0 is not available anymore!
    int data_type = isData ? 1 : 1; //set datatype, 0-Data(or dont use the tool - faster), 1-FULLSIM, 3-AF2 (https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XAODElectronEfficiencyCorrectionTool)
    //int data_type = isData ? 0 : 1; //set datatype, 0-Data(or dont use the tool - faster), 1-FULLSIM, 3-AF2 (https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XAODElectronEfficiencyCorrectionTool)
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_ID->setProperty("ForceDataType" 			,data_type) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_ID->setProperty("CorrelationModel"			,"TOTAL") );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_ID->initialize() );

    // Tight LH ID
    m_AsgElectronEfficiencyCorrectionTool_ID_TightLH = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_ID_TightLH"+m_outputName);
    // Recommendations here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2#LH_electron_identification
    std::vector<std::string> inputFileID_TightLH{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/offline/efficiencySF.offline.TightLLH_d0z0_v13.root"} ;
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_ID_TightLH->setProperty("CorrectionFileNameList"		,inputFileID_TightLH) );
    // FIXME: looks like ForceDataType=0 is not available anymore!
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_ID_TightLH->setProperty("ForceDataType" 			,data_type) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_ID_TightLH->setProperty("CorrelationModel"			,"TOTAL") );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_ID_TightLH->initialize() );

    m_AsgElectronEfficiencyCorrectionTool_isolation = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_isolation"+m_outputName);
    std::vector<std::string> inputFileISO{m_opt_elisoSF_input};
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_isolation->setProperty("CorrectionFileNameList"	,inputFileISO) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_isolation->setProperty("ForceDataType"		,data_type) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_isolation->setProperty("CorrelationModel"		,"TOTAL") );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_isolation->initialize() ); 

    m_AsgElectronEfficiencyCorrectionTool_reco = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_reco"+m_outputName);
    std::vector<std::string> inputFileRECO{m_opt_elrecoSF_input};
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_reco->setProperty("CorrectionFileNameList"		,inputFileRECO) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_reco->setProperty("ForceDataType"			,data_type) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_reco->setProperty("CorrelationModel"		,"TOTAL") );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_reco->initialize() ); 

    m_AsgElectronEfficiencyCorrectionTool_trigSF = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_trigger"+m_outputName);
    // FIXME: completely random input
    std::vector<std::string> inputFileTSF{m_opt_eltrigSF_input};
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_trigSF->setProperty("CorrectionFileNameList"	,inputFileTSF) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_trigSF->setProperty("ForceDataType"		,data_type) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_trigSF->setProperty("CorrelationModel"		,"TOTAL") );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_trigSF->initialize() ); 

    // Custom AsgElectronEfficiencyCorrectionTool devoted to calibration of PLV
    m_AsgElectronEfficiencyCorrectionTool_PLV = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_PLV_Iso"+m_outputName);
    //m_AsgElectronEfficiencyCorrectionTool_PLV->msg().setLevel(MSG::DEBUG);
    std::string PLV_calfolder(m_PLV_calfolder.Data());
    std::string elPLVSF_input = PLV_calfolder + "/efficiencySF.Isolation.TightLLH_d0z0_v13_PLVeto_CFTtight_ambiguity0_isolFixedCutLoose.root";
    std::vector<std::string> inputFileElISO_PLV{elPLVSF_input};
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_PLV->setProperty("CorrectionFileNameList"		,inputFileElISO_PLV) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_PLV->setProperty("ForceDataType"			,data_type) );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_PLV->setProperty("CorrelationModel"		,"TOTAL") );
    ANA_CHECK( m_AsgElectronEfficiencyCorrectionTool_PLV->initialize() ); 

    //----------------------------
    // ELECTRONS
    //---------------------------



    //----------------------------
    // B-TAGGING
    //---------------------------

    m_BTaggingSelectionTool = new BTaggingSelectionTool("BTaggingSelectionTool"+m_outputName);
    ANA_CHECK( m_BTaggingSelectionTool->setProperty("TaggerName"                                       	,"MV2c10") );
    ANA_CHECK( m_BTaggingSelectionTool->setProperty("OperatingPoint"                                   	,"FixedCutBEff_70") );
    ANA_CHECK( m_BTaggingSelectionTool->setProperty("JetAuthor"                                        	,"AntiKt4EMTopoJets") );
    ANA_CHECK( m_BTaggingSelectionTool->setProperty("MinPt"						,static_cast<double>(m_jet_pt_cut)) );
    ANA_CHECK( m_BTaggingSelectionTool->setProperty("MaxEta"						,static_cast<double>(m_jet_eta_cut)) );
    ANA_CHECK( m_BTaggingSelectionTool->setProperty("FlvTagCutDefinitionsFileName"			,"xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-04-24_v1.root") );
    ANA_CHECK( m_BTaggingSelectionTool->initialize() );

    m_BTaggingEfficiencyTool = new BTaggingEfficiencyTool("BTaggingEfficiencyTool_a"+m_outputName);
    ANA_CHECK( m_BTaggingEfficiencyTool->setProperty("TaggerName"					,"MV2c10") );
    ANA_CHECK( m_BTaggingEfficiencyTool->setProperty("OperatingPoint"					,"FixedCutBEff_70") );
    ANA_CHECK( m_BTaggingEfficiencyTool->setProperty("JetAuthor"           				,"AntiKt4EMTopoJets") );
    std::string calib_file_path = PathResolverFindCalibFile("xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-06-29_v1.root");
    ANA_CHECK( m_BTaggingEfficiencyTool->setProperty("EfficiencyFileName" 				,calib_file_path) ); 
    ANA_CHECK( m_BTaggingEfficiencyTool->setProperty("ScaleFactorFileName"				,calib_file_path) ); 
    ANA_CHECK( m_BTaggingEfficiencyTool->initialize() );

    //----------------------------
    // B-TAGGING
    //---------------------------



    //----------------------------
    // SYSTEMATICS
    //---------------------------

    // Get the recommended systematic variations for the tools we previously initialised
    if (m_doSystematics) {
      std::cout << "Setting systematic variations to loop over..." << std::endl;
      CP::SystematicRegistry 	const & systematic_registry 	= CP::SystematicRegistry::getInstance();
      CP::SystematicSet 	const & recommended_systematics = systematic_registry.recommendedSystematics();
      // Add nominal and experimental systematics to the list
      systematics_list = std::vector<CP::SystematicSet>(1, CP::SystematicSet()); // < nominal only
      for (auto const & recommended : recommended_systematics) {
        if (m_debugMode) std::cout << "Systematic name:		" << recommended.name() << std::endl;
        // Jet energy scale uncertainties are defined as continuous,
        // so we need to implement the +/- 1 sigma shifts ourselves:
        if (recommended.name().substr(0, 4) == "JET_" && recommended.name() != "JET_JER_SINGLE_NP__1up") {
          systematics_list.push_back(CP::SystematicSet());
          systematics_list.back().insert(CP::SystematicVariation(boost::algorithm::replace_all_copy(recommended.name(), "__continuous", ""), -1.0));
          systematics_list.push_back(CP::SystematicSet());
          systematics_list.back().insert(CP::SystematicVariation(boost::algorithm::replace_all_copy(recommended.name(), "__continuous", ""), 1.0));
        }
        else {
          systematics_list.push_back(CP::SystematicSet());
          systematics_list.back().insert(recommended);
        }
      }
      // Save b-tagging-related systematics
      // see example here: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTaggingCalibrationDataInterface#Example
      CP::SystematicSet btag_systs = m_BTaggingEfficiencyTool->affectingSystematics();
      for (auto syst : btag_systs) {
        CP::SystematicSet myset;
        myset.insert(syst);
        ftag_systematics_list.push_back(myset);
      }
      std::cout << "ftag_systematics_list.size:\t" << ftag_systematics_list.size() << std::endl;
    }

    // Print info about which systematic variations will be performed
    for (auto const & systematic : systematics_list) {
      auto const name = systematic.name();
      auto const title = (name.empty()) ? "Nominal (no systematic variation)" : name;
      Info("initialize()", "Added systematic variation: %s", title.c_str());
    }
    Info("initialize()", "Set up %i systematic variations in addition to nominal setting.", static_cast<int>(systematics_list.size() - 1));

    //----------------------------
    // CALIBRATION TOOLS -> SYSTEMATIC TREES (UP, DOWN)
    //----------------------------

    // For systematic TTrees: apply variations to all calibration tools
    // as tools that are not affected will not be subject to changes
    if (!m_isnominalTTree) {
      if (m_debugMode) {
        std::cout << "Current systematic variation:\t:" << m_outputtree << std::endl;
        CP::SystematicSet EgammaCal_systs = m_EgammaCalibrationAndSmearingTool->affectingSystematics();
        std::cout << "Systematics affecting EgammaCalibrationAndSmearingTool:\t:" << std::endl;
        for (auto syst : EgammaCal_systs) std::cout << syst.name() << "\n" << std::endl; 
        CP::SystematicSet MuonCal_systs = m_MuonCalibrationAndSmearingTool->affectingSystematics();
        std::cout << "Systematics affecting MuonCalibrationAndSmearingTool:\t:" << std::endl;
        for (auto syst : MuonCal_systs) std::cout << syst.name() << "\n" << std::endl; 
        CP::SystematicSet JER_systs = m_JERSmearingTool->affectingSystematics();
        std::cout << "Systematics affecting JERSmearingTool:\t:" << std::endl;
        for (auto syst : JER_systs) std::cout << syst.name() << "\n" << std::endl; 
        CP::SystematicSet JetUncertainties_systs = m_JetUncertaintiesTool->affectingSystematics();
        std::cout << "Systematics affecting JetUncertaintiesTool:\t:" << std::endl;
        for (auto syst : JetUncertainties_systs) std::cout << syst.name() << "\n" << std::endl; 
      }
      std::size_t found = m_outputtree.find("__1up");
      int UP_DOWN(-99.); UP_DOWN = (found!=std::string::npos) ? 1 : -1;
      if (UP_DOWN==-1) found = m_outputtree.find("__1down");
      std::string name_of_syst = m_outputtree.substr(0,found);
      std::cout << "Systematic variation name itself:\t:" << name_of_syst << std::endl;
      CP::SystematicSet systtree_variation; systtree_variation.insert(CP::SystematicVariation(name_of_syst,UP_DOWN));
      // EG_RESOLUTION_ALL__1down, EG_RESOLUTION_ALL__1up, EG_SCALE_AF2__1down, EG_SCALE_AF2__1up, EG_SCALE_ALL__1down, EG_SCALE_ALL__1up
      ANA_CHECK( m_EgammaCalibrationAndSmearingTool->applySystematicVariation(systtree_variation) );
      // MUON_ID__1down, MUON_ID__1up, MUON_MS__1down, MUON_MS__1up, MUON_SAGITTA_RESBIAS__1down, MUON_SAGITTA_RESBIAS__1up, MUON_SAGITTA_RHO__1down, MUON_SAGITTA_RHO__1up, MUON_SCALE__1down, MUON_SCALE__1up
      ANA_CHECK( m_MuonCalibrationAndSmearingTool->applySystematicVariation(systtree_variation) );
      // MET_SoftTrk_ResoPara, MET_SoftTrk_ResoPerp, MET_SoftTrk_ScaleDown, MET_SoftTrk_ScaleUp
      CP::SystematicSet METSysSet( m_outputtree );
      if ( m_outputName.find("MET_")!=std::string::npos )	{ ANA_CHECK( metSystTool->applySystematicVariation(METSysSet) );	}
      // Simple systematic mode: JET_JER_SINGLE_NP__1up
      ANA_CHECK( m_JERSmearingTool->applySystematicVariation(systtree_variation) );
      // JET_BJES_Response__1down, JET_BJES_Response__1up,JET_EffectiveNP_Detector1__1down,JET_EffectiveNP_Detector1__1up,JET_EffectiveNP_Mixed1__1down,JET_EffectiveNP_Mixed1__1up,JET_EffectiveNP_Mixed2__1down,JET_EffectiveNP_Mixed2__1up,JET_EffectiveNP_Mixed3__1down,JET_EffectiveNP_Mixed3__1up,JET_EffectiveNP_Modelling1__1down,JET_EffectiveNP_Modelling1__1up,JET_EffectiveNP_Modelling2__1down,JET_EffectiveNP_Modelling2__1up,JET_EffectiveNP_Modelling3__1down,JET_EffectiveNP_Modelling3__1up,JET_EffectiveNP_Modelling4__1down,JET_EffectiveNP_Modelling4__1up,JET_EffectiveNP_Statistical1__1down,JET_EffectiveNP_Statistical1__1up,JET_EffectiveNP_Statistical2__1down,JET_EffectiveNP_Statistical2__1up,JET_EffectiveNP_Statistical3__1down,JET_EffectiveNP_Statistical3__1up,JET_EffectiveNP_Statistical4__1down,JET_EffectiveNP_Statistical4__1up,JET_EffectiveNP_Statistical5__1down,JET_EffectiveNP_Statistical5__1up,JET_EffectiveNP_Statistical6__1down,JET_EffectiveNP_Statistical6__1up,JET_EtaIntercalibration_Modelling__1down,JET_EtaIntercalibration_Modelling__1up,JET_EtaIntercalibration_NonClosure_highE__1down,JET_EtaIntercalibration_NonClosure_highE__1up,JET_EtaIntercalibration_NonClosure_negEta__1down,JET_EtaIntercalibration_NonClosure_negEta__1up,JET_EtaIntercalibration_NonClosure_posEta__1down,JET_EtaIntercalibration_NonClosure_posEta__1up,JET_EtaIntercalibration_TotalStat__1down,JET_EtaIntercalibration_TotalStat__1up,JET_Flavor_Composition__1down,JET_Flavor_Composition__1up,JET_Flavor_Response__1down,JET_Flavor_Response__1up,JET_JER_SINGLE_NP__1up,JET_JvtEfficiency__1down__1down,JET_JvtEfficiency__1down__1up,JET_JvtEfficiency__1up__1down,JET_JvtEfficiency__1up__1up,JET_Pileup_OffsetMu__1down,JET_Pileup_OffsetMu__1up,JET_Pileup_OffsetNPV__1down,JET_Pileup_OffsetNPV__1up,JET_Pileup_PtTerm__1down,JET_Pileup_PtTerm__1up,JET_Pileup_RhoTopology__1down,JET_Pileup_RhoTopology__1up,JET_PunchThrough_MC16__1down,JET_PunchThrough_MC16__1up,JET_SingleParticle_HighPt__1down,JET_SingleParticle_HighPt__1up
      // Jet energy scale uncertainties are defined as continuous,
      // so we need to implement the +/- 1 sigma shifts ourselves:
      if ( m_outputName.find("JET_")!=std::string::npos && m_outputName.find("_JER_")==std::string::npos ) { 
        if (m_debugMode) std::cout << "I'm a JES variation!!! Name of syst:\t" << name_of_syst << std::endl;
        CP::SystematicSet systtree_JES_variation; 
        systtree_JES_variation.insert(CP::SystematicVariation(name_of_syst, UP_DOWN));
        ANA_CHECK( m_JetUncertaintiesTool->applySystematicVariation(systtree_JES_variation) );
      }
    }

    //----------------------------
    // CALIBRATION TOOLS -> SYSTEMATIC TREES (UP, DOWN)
    //----------------------------

    //-----------------------------------------
    // MC weight for scale and PDF variations 
    //-----------------------------------------

    // scale variations: retrieve alternative MC event weights
    // see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/StandardModelASG#Accessing_names_of_entries_in_MC
    if (m_debugMode) std::cout << "Scale and PDF variations -> retrieve names" << std::endl;
    const xAOD::CutBookkeeperContainer* completeCBC = nullptr;
    ANA_CHECK( event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess() );
    m_MCWeightNames = {};
    for (const auto& cbk: *completeCBC) {
      std::string cbkName = cbk->name();
      if (cbkName.substr(0,11) == "LHE3Weight_") {
        if (m_debugMode) std::cout << "MC WEIGHT NAME\t" << cbkName << std::endl;
        m_MCWeightNames.push_back(cbkName.substr(11));
      }
    }
    // sumOfWeights for scale variations
    for (unsigned int i=0; i<m_MCWeightNames.size(); i++) { 
      if (m_debugMode) std::cout << "NEW MC WEIGHT NAME\t" << m_MCWeightNames[i] << std::endl;
      if (m_debugMode) std::cout << "corresponding SumOfWeights:\t:" <<  SkimmingInfo::all_sum_of_weights_before_skimming(event, "LHE3Weight_"+m_MCWeightNames[i]) << std::endl;
      m_allSUM_of_weights_before_skimming.push_back(SkimmingInfo::all_sum_of_weights_before_skimming(event, "LHE3Weight_"+m_MCWeightNames[i])); 
      if (m_debugMode) std::cout << "corresponding SumOfWeights:\t:" << m_allSUM_of_weights_before_skimming[i] << std::endl;
    }
    if (m_debugMode) std::cout << "m_allSUM_of_weights_before_skimming.size() = " << m_allSUM_of_weights_before_skimming.size() << std::endl;
    if (m_allSUM_of_weights_before_skimming.size() != m_MCWeightNames.size()) Warning("initialize()","MC weight vector and corresponding sumOfWeights have different size!");

    //-----------------------------------------
    // MC weight for scale and PDF variations 
    //-----------------------------------------


    //----------------------------
    // SYSTEMATICS
    //---------------------------


    // output xAOD
    TFile *file = wk()->getOutputFile (m_outputName.c_str());
    //TFile *file = wk()->getOutputFile ("outputLabel");
    //if (m_isnominalTTree) ANA_CHECK(event->writeTo(file));
    ANA_CHECK(event->writeTo(file));
    
    if (m_debugMode) std::cout << "End of initialize()" << std::endl;

    return EL::StatusCode::SUCCESS;
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: execute ()
  {
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    // clear vectors
    m_allMC_weights					.clear();
    m_ZdecayMode					.clear();
    m_vjet_E						.clear();
    m_vjet_pt						.clear();
    m_vjet_eta						.clear();
    m_vjet_phi						.clear();
    m_vjet_jvt						.clear();
    m_vjet_coneLabel					.clear();
    m_vjet_mv2c10					.clear();
    m_vjet_mv2c10mu					.clear();
    m_vlepton_ID					.clear();
    m_vlepton_E						.clear();
    m_vlepton_pt					.clear();
    m_vlepton_eta					.clear();
    m_vlepton_phi					.clear();
    m_vlepton_q						.clear();
    m_vlepton_d0					.clear();
    m_vlepton_z0					.clear();
    m_vlepton_z0sinT					.clear();
    m_vlepton_truthType              			.clear();
    m_vlepton_truthOrigin            			.clear();
    m_vlepton_sigd0                   			.clear();
    m_lepton_PromptLeptonVeto				.clear();
    m_lepton_PromptLeptonIso_TagWeight			.clear();
    m_vlepton_isTightLH					.clear();
    m_vlepton_isTightID					.clear();
    m_vlepton_chargeIDBDTTight				.clear();
    m_vlepton_ambiguityType				.clear();
    m_vlepton_isolationFixedCutLoose			.clear();
    m_vlepton_SFIDLoose                			.clear();
    m_vlepton_SFIDTightLH             			.clear();
    m_vlepton_SFIsoLoose               			.clear();
    m_vlepton_SFIsoPLV               			.clear();
    m_vlepton_SFReco					.clear();
    m_vlepton_SFTTVA					.clear();
    m_vlepton_SFObjLoose				.clear();
    m_vlepton_SFObjLoose_EL_SF_ID_UP   			.clear(); 
    m_vlepton_SFObjLoose_EL_SF_ID_DOWN 			.clear(); 
    m_vlepton_SFObjLoose_EL_SF_Reco_UP   		.clear(); 
    m_vlepton_SFObjLoose_EL_SF_Reco_DOWN		.clear(); 
    m_vlepton_SFObjLoose_EL_SF_Iso_UP   		.clear(); 
    m_vlepton_SFObjLoose_EL_SF_Iso_DOWN			.clear(); 
    m_vlepton_SFObjLoose_MU_SF_RECO_STAT_UP 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_RECO_STAT_DOWN 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_RECO_SYST_UP 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_RECO_SYST_DOWN 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_Iso_STAT_UP 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_Iso_STAT_DOWN 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_Iso_SYST_UP 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_Iso_SYST_DOWN 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_UP 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_UP 		.clear(); 
    m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN 		.clear(); 
    m_vlepton_SFObjTightLH				.clear();
    m_vlepton_SFObjTightLH_EL_SF_ID_UP   		.clear(); 
    m_vlepton_SFObjTightLH_EL_SF_ID_DOWN 		.clear(); 
    m_vlepton_SFObjTightLH_EL_SF_Reco_UP   		.clear(); 
    m_vlepton_SFObjTightLH_EL_SF_Reco_DOWN		.clear(); 
    m_vlepton_SFObjTightLH_EL_SF_Iso_UP   		.clear(); 
    m_vlepton_SFObjTightLH_EL_SF_Iso_DOWN		.clear(); 
    m_vlepton_SFObjTight_MU_SF_RECO_STAT_UP 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_RECO_STAT_DOWN 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_RECO_SYST_UP 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_RECO_SYST_DOWN 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_Iso_STAT_UP 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_Iso_STAT_DOWN 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_Iso_SYST_UP 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_Iso_SYST_DOWN 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_TTVA_STAT_UP 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_TTVA_STAT_DOWN 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_TTVA_SYST_UP 		.clear(); 
    m_vlepton_SFObjTight_MU_SF_TTVA_SYST_DOWN 		.clear(); 
    m_vmuon_E						.clear();
    m_vmuon_pt						.clear();
    m_vmuon_eta						.clear();
    m_vmuon_phi						.clear();
    m_vmuon_q						.clear();
    m_vmuon_d0						.clear();
    m_vmuon_z0						.clear();
    m_vmuon_z0sinT					.clear();
    m_vmuon_truthType                   		.clear();
    m_vmuon_truthOrigin                 		.clear();
    m_vmuon_sigd0                   			.clear();
    //m_vmuon_sigz0                   			.clear();
    m_velectron_E					.clear();
    m_velectron_pt					.clear();
    m_velectron_eta					.clear();
    m_velectron_phi					.clear();
    m_velectron_q					.clear();
    m_velectron_d0					.clear();
    m_velectron_z0					.clear();
    m_velectron_z0sinT					.clear();
    m_velectron_truthType           			.clear();
    m_velectron_truthOrigin         			.clear();
    m_velectron_sigd0                			.clear(); 
    //m_velectron_sigz0                 	 	.clear(); 
    m_beforeOLR_vjet_E					.clear();
    m_beforeOLR_vjet_pt					.clear();
    m_beforeOLR_vjet_eta				.clear();
    m_beforeOLR_vjet_phi				.clear();
    m_beforeOLR_vjet_jvt				.clear();
    m_beforeOLR_vjet_coneLabel				.clear();
    m_forward_vjet_E					.clear();
    m_forward_vjet_pt					.clear();
    m_forward_vjet_eta					.clear();
    m_forward_vjet_phi					.clear();
    m_forward_vjet_jvt					.clear();
    m_forward_vjet_coneLabel				.clear();
    m_beforeOLR_vmuon_E					.clear();
    m_beforeOLR_vmuon_pt				.clear();
    m_beforeOLR_vmuon_eta				.clear();
    m_beforeOLR_vmuon_phi				.clear();
    m_beforeOLR_vmuon_q					.clear();
    m_beforeOLR_vmuon_d0				.clear();
    m_beforeOLR_vmuon_z0				.clear();
    m_beforeOLR_vmuon_z0sinT				.clear();
    m_beforeOLR_velectron_E				.clear();
    m_beforeOLR_velectron_pt				.clear();
    m_beforeOLR_velectron_eta				.clear();
    m_beforeOLR_velectron_phi				.clear();
    m_beforeOLR_velectron_q				.clear();
    m_beforeOLR_velectron_d0				.clear();
    m_beforeOLR_velectron_z0				.clear();
    m_beforeOLR_velectron_z0sinT			.clear();
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_up        	.clear();
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_down      	.clear();
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_up        	.clear();
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_down      	.clear();
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up    	.clear();
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down  	.clear();
    // initialise variables
    m_runNumber							= 0;
    m_RunYear							= 0;
    m_tot_charge						= 0;
    m_mu							= 0;			
    m_pileup_weight						= 0;
    m_pileup_weight_UP						= 0; 
    m_pileup_weight_DOWN					= 0; 
    m_JVT_EventWeight						= 0;
    m_JVT_EventWeight_UP					= 0;
    m_JVT_EventWeight_DOWN					= 0;
    m_MV2c10_FixedCutBEff_70_EventWeight			= 0;
    m_EventNumber 						= 0;
    m_passTrigger						= 0;
    m_passEventCleaning						= 0;
    m_HLT_e60_lhmedium						= 0;
    m_HLT_e120_lhloose						= 0;
    m_HLT_mu20_iloose_L1MU15					= 0;
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // m_HLT_2e12_lhloose_L12EM10VH				= 0;
    // m_HLT_mu18_mu8noL1						= 0;
    // m_HLT_e17_lhloose_mu14					= 0;
    m_HLT_mu26_ivarmedium					= 0;
    m_HLT_mu50							= 0;
    m_HLT_e24_lhmedium_L1EM20VH					= 0;
    m_HLT_e26_lhtight_nod0_ivarloose				= 0;	
    m_HLT_e60_lhmedium_nod0					= 0;	
    m_HLT_e140_lhloose_nod0					= 0;	
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // m_HLT_mu22_mu8noL1						= 0;
    // m_HLT_2e17_lhvloose_nod0					= 0;
    // m_HLT_2e24_lhvloose_nod0					= 0;
    // m_HLT_e17_lhloose_nod0_mu14					= 0;
    // unused triggers
    //m_HLT_2mu14							= 0;
    //m_HLT_e26_lhmedium_nod0_mu8noL1				= 0;
    //m_HLT_e7_lhmedium_nod0_mu24					= 0;
    //m_HLT_mu20_2mu4noL1						= 0; 
    //m_HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH	= 0; 
    // unused triggers
    m_isTrigMatched 						= 0;
    m_has_PV	 						= 0;
    // truth event info
    m_nTruthPromptLep						= 0;
    m_higgsMode 						= 0;
//    m_W_lep							= 0; 
//    m_W_had							= 0;
//    m_Z_lep							= 0;
//    m_Z_inv							= 0;
//    m_Z_had							= 0;
    // inv masses
    m_Mllll0123							= 0; 
    m_Mlll012							= 0; 
    m_Mlll013 							= 0;
    m_Mlll023 							= 0;
    m_Mlll123 							= 0;
    m_Mll01 							= 0;	
    m_Mll02 							= 0;	
    m_Mll03 							= 0;	
    m_Mll12 							= 0;	
    m_Mll13 							= 0;	
    m_Mll23 							= 0;	
    m_best_Z_Mll						= 0; 
    m_best_Z_other_Mll						= 0;
    m_Mjj01                                                     = 0;
    m_Mjj02                                                     = 0;
    m_Mjj03                                                     = 0;
    m_Mjj04                                                     = 0;
    m_Mjj05                                                     = 0;
    m_Mjj12                                                     = 0;
    m_Mjj13                                                     = 0;
    m_Mjj14                                                     = 0;
    m_Mjj15                                                     = 0;
    m_Mjj23                                                     = 0;
    m_Mjj24                                                     = 0;
    m_Mjj25                                                     = 0;
    m_Mjj34                                                     = 0;
    m_Mjj35                                                     = 0;
    m_Mjj45                                                     = 0;
    // event kinematics
    m_HT_lep							= 0;
    m_HT_had							= 0;
    // MET
    m_MET                          				= 0; 
    m_MET_phi                      				= 0; 
    m_MET_x                        				= 0; 
    m_MET_y                        				= 0; 
    m_MET_RefFinal_et						= 0;
    m_MET_RefFinal_phi						= 0;
    m_MET_RefFinal_x						= 0;
    m_MET_RefFinal_y						= 0;
    // jets
    m_nJets_mv2c10_60						= 0;   
    m_nJets_mv2c10_70						= 0;   
    m_nJets_mv2c10_77						= 0;   
    m_nJets_mv2c10_85						= 0; 



    if (m_debugMode) std::cout << std::endl << "NEW EVENT" << std::endl;

    ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  
    xAOD::TEvent* event = wk()->xaodEvent();
  
    // print every 100 events, so we know where we are:
    if( (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
    m_eventCounter++;

    if (m_debugMode) std::cout << "number_of_events_before_skimming:		" << m_number_of_events_before_skimming <<std::endl;
    if (m_debugMode) std::cout << "m_sum_of_weights_before_skimming:		" << m_sum_of_weights_before_skimming <<std::endl;




    //----------------------------
    // Event information
    //--------------------------- 
    const xAOD::EventInfo* eventInfo = nullptr;
    ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));  
    bool isData = !(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION )); //bool describing if the events are data or from simulation
    // for data: check if event passes GRL
    if(isData) {
      if(!m_GoodRunsListSelectionTool->passRunLB(*eventInfo)){
        if (m_debugMode) std::cout << "DATA event rejected by GRL" << std::endl;
        return EL::StatusCode::SUCCESS; // go to next event
      }
      else if (m_debugMode) std::cout<< "Passed GRL" << std::endl;
    }

    m_EventNumber 	= eventInfo->eventNumber();
    m_runNumber		= (isData || m_isAODfile) ? eventInfo->runNumber() : m_PileupReweightingTool->getRandomRunNumber(*eventInfo, false);
    if (m_runNumber >= 320000) {
      m_RunYear = 2017;
    } else if (m_runNumber >=290000) {
      m_RunYear = 2016;
    } else if (m_runNumber > 0) {
      m_RunYear = 2015;
    } else {
      std::cout << "WARNING Did you know you're getting a run number = 	" << m_runNumber << "	? :(" << std::endl;
      if (!m_isAODfile) throw std::logic_error("ERROR Not a xAOD input and unknown Run Year: this would cause issues with trigger configuration! Exiting.");
    }

    //----------------------------
    // TDT
    //--------------------------- 
    // get & print list of triggers
    //auto triggerList = m_TrigDecisionTool->getListOfTriggers();
    //for(int i=0; i<triggerList.size(); i++) std::cout << "Trigger = " << triggerList[i] <<std::endl;
    // define Trig::ChainGroup objects whose isPassed() method gives a bool
    // 2015, 2016 and 2017
    const Trig::ChainGroup* TrigChain_mu50 							= m_TrigDecisionTool->getChainGroup("HLT_mu50");
    // 2015, 2016 and 2017
    // 2015 only
    const Trig::ChainGroup* TrigChain_e24_lhmedium_L1EM20VH 					= m_TrigDecisionTool->getChainGroup("HLT_e24_lhmedium_L1EM20VH");
    const Trig::ChainGroup* TrigChain_e60_lhmedium          					= m_TrigDecisionTool->getChainGroup("HLT_e60_lhmedium");
    const Trig::ChainGroup* TrigChain_e120_lhloose          					= m_TrigDecisionTool->getChainGroup("HLT_e120_lhloose");
    const Trig::ChainGroup* TrigChain_mu20_iloose_L1MU15    					= m_TrigDecisionTool->getChainGroup("HLT_mu20_iloose_L1MU15");
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // const Trig::ChainGroup* TrigChain_2e12_lhloose_L12EM10VH					= m_TrigDecisionTool->getChainGroup("HLT_2e12_lhloose_L12EM10VH");
    // const Trig::ChainGroup* TrigChain_mu18_mu8noL1          					= m_TrigDecisionTool->getChainGroup("HLT_mu18_mu8noL1");
    // const Trig::ChainGroup* TrigChain_e17_lhloose_mu14      					= m_TrigDecisionTool->getChainGroup("HLT_e17_lhloose_mu14");
    // 2015 only
    // 2016 and 2017 
    const Trig::ChainGroup* TrigChain_mu26_ivarmedium 						= m_TrigDecisionTool->getChainGroup("HLT_mu26_ivarmedium");
    const Trig::ChainGroup* TrigChain_e26_lhtight_nod0_ivarloose 				= m_TrigDecisionTool->getChainGroup("HLT_e26_lhtight_nod0_ivarloose");
    const Trig::ChainGroup* TrigChain_e60_lhmedium_nod0  					= m_TrigDecisionTool->getChainGroup("HLT_e60_lhmedium_nod0");
    const Trig::ChainGroup* TrigChain_e140_lhloose_nod0 					= m_TrigDecisionTool->getChainGroup("HLT_e140_lhloose_nod0");
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // const Trig::ChainGroup* TrigChain_mu22_mu8noL1 						= m_TrigDecisionTool->getChainGroup("HLT_mu22_mu8noL1");
    // const Trig::ChainGroup* TrigChain_e17_lhloose_nod0_mu14					= m_TrigDecisionTool->getChainGroup("HLT_e17_lhloose_nod0_mu14");
    // // 2016 and 2017 
    // // 2016 only
    // const Trig::ChainGroup* TrigChain_2e17_lhvloose_nod0					= m_TrigDecisionTool->getChainGroup("HLT_2e17_lhvloose_nod0");
    // // 2016 only
    // // 2017 only
    // const Trig::ChainGroup* TrigChain_2e24_lhvloose_nod0					= m_TrigDecisionTool->getChainGroup("HLT_2e24_lhvloose_nod0");
    // 2017 only
    // unused triggers
    //const Trig::ChainGroup* TrigChain_e26_lhmedium_nod0_mu8noL1				= m_TrigDecisionTool->getChainGroup("HLT_e26_lhmedium_nod0_mu8noL1");
    //const Trig::ChainGroup* TrigChain_e7_lhmedium_nod0_mu24					= m_TrigDecisionTool->getChainGroup("HLT_e7_lhmedium_nod0_mu24");
    //const Trig::ChainGroup* TrigChain_mu20_2mu4noL1 						= m_TrigDecisionTool->getChainGroup("HLT_mu20_2mu4noL1");
    //const Trig::ChainGroup* TrigChain_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH	= m_TrigDecisionTool->getChainGroup("HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH");
    //const Trig::ChainGroup* TrigChain_2mu14							= m_TrigDecisionTool->getChainGroup("HLT_2mu14");
    // unused triggers
    // trigger booleans
    // 2015, 2016 and 2017
    m_HLT_mu50 								= TrigChain_mu50->isPassed();
    // 2015, 2016 and 2017
    // 2015 only
    m_HLT_e24_lhmedium_L1EM20VH 					= TrigChain_e24_lhmedium_L1EM20VH->isPassed();
    m_HLT_e60_lhmedium          					= TrigChain_e60_lhmedium->isPassed();
    m_HLT_e120_lhloose          					= TrigChain_e120_lhloose->isPassed();
    m_HLT_mu20_iloose_L1MU15    					= TrigChain_mu20_iloose_L1MU15->isPassed();
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // m_HLT_2e12_lhloose_L12EM10VH					= TrigChain_2e12_lhloose_L12EM10VH->isPassed();
    // m_HLT_mu18_mu8noL1          					= TrigChain_mu18_mu8noL1->isPassed();
    // m_HLT_e17_lhloose_mu14      					= TrigChain_e17_lhloose_mu14->isPassed();
    // 2015 only
    // 2016 and 2017 
    m_HLT_mu26_ivarmedium						= TrigChain_mu26_ivarmedium->isPassed();
    m_HLT_e26_lhtight_nod0_ivarloose 					= TrigChain_e26_lhtight_nod0_ivarloose->isPassed();
    m_HLT_e60_lhmedium_nod0 						= TrigChain_e60_lhmedium_nod0->isPassed();
    m_HLT_e140_lhloose_nod0						= TrigChain_e140_lhloose_nod0->isPassed();
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // m_HLT_mu22_mu8noL1							= TrigChain_mu22_mu8noL1->isPassed();
    // m_HLT_e17_lhloose_nod0_mu14						= TrigChain_e17_lhloose_nod0_mu14->isPassed();
    // // 2016 and 2017 
    // // 2016 only
    // m_HLT_2e17_lhvloose_nod0						= TrigChain_2e17_lhvloose_nod0->isPassed();
    // // 2016 only
    // // 2017 only
    // m_HLT_2e24_lhvloose_nod0						= TrigChain_2e24_lhvloose_nod0->isPassed();
    // 2017 only
    // unused triggers
    //m_HLT_e26_lhmedium_nod0_mu8noL1					= TrigChain_e26_lhmedium_nod0_mu8noL1->isPassed();
    //m_HLT_e7_lhmedium_nod0_mu24					= TrigChain_e7_lhmedium_nod0_mu24->isPassed();
    //m_HLT_mu20_2mu4noL1						= TrigChain_mu20_2mu4noL1->isPassed();
    //m_HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH	= TrigChain_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH->isPassed();
    //m_HLT_2mu14							= TrigChain_2mu14->isPassed();
    // unused triggers
    // combine triggers
    bool passTrigger(false);
    if ( m_RunYear == 2015 ) {
      // 14.08.18: drop DLTs, as -0.1% on signal acceptance
      // passTrigger = (m_HLT_mu50 || m_HLT_e24_lhmedium_L1EM20VH || m_HLT_e60_lhmedium || m_HLT_e120_lhloose || m_HLT_mu20_iloose_L1MU15 || m_HLT_2e12_lhloose_L12EM10VH || m_HLT_mu18_mu8noL1 || m_HLT_e17_lhloose_mu14);
      passTrigger = (m_HLT_mu50 || m_HLT_e24_lhmedium_L1EM20VH || m_HLT_e60_lhmedium || m_HLT_e120_lhloose || m_HLT_mu20_iloose_L1MU15);
    }
    else if ( m_RunYear == 2016 ) {
      // 14.08.18: drop DLTs, as -0.1% on signal acceptance
      //passTrigger = (m_HLT_mu50 || m_HLT_mu26_ivarmedium || m_HLT_e26_lhtight_nod0_ivarloose || m_HLT_e60_lhmedium_nod0 || m_HLT_e140_lhloose_nod0 || m_HLT_mu22_mu8noL1 || m_HLT_e17_lhloose_nod0_mu14 || m_HLT_2e17_lhvloose_nod0);
      passTrigger = (m_HLT_mu50 || m_HLT_mu26_ivarmedium || m_HLT_e26_lhtight_nod0_ivarloose || m_HLT_e60_lhmedium_nod0 || m_HLT_e140_lhloose_nod0);
    }
    else if ( m_RunYear == 2017 ) {
      // 14.08.18: drop DLTs, as -0.1% on signal acceptance
      //passTrigger = (m_HLT_mu50 || m_HLT_mu26_ivarmedium || m_HLT_e26_lhtight_nod0_ivarloose || m_HLT_e60_lhmedium_nod0 || m_HLT_e140_lhloose_nod0 || m_HLT_mu22_mu8noL1 || m_HLT_e17_lhloose_nod0_mu14 || m_HLT_2e24_lhvloose_nod0);
      passTrigger = (m_HLT_mu50 || m_HLT_mu26_ivarmedium || m_HLT_e26_lhtight_nod0_ivarloose || m_HLT_e60_lhmedium_nod0 || m_HLT_e140_lhloose_nod0);
    }
    else {
      std::cout << "WARNING Did you know you're getting a run year = " << m_RunYear << "   ? :(" << std::endl;
    }
    // combine triggers
    //if(m_eventCounter<100) Info("execute()", "# Trigger = %d ", passTrigger);
    m_passTrigger = passTrigger;
    // INFO: trigger matching later, after defining el and mu...
    // NEW: when running on data select events firing triggers 
    if ( isData && !m_passTrigger ) return EL::StatusCode::SUCCESS;


    m_mcChannelNumber 	= isData ? 0 : eventInfo->mcChannelNumber();
    // fetch generator event weight (== 1 for real data)
    if (m_debugMode && !isData) std::cout << "MC event weight: 	" << eventInfo->mcEventWeight() << std::endl;
    m_mcEventWeight 	= isData ? 1. : eventInfo->mcEventWeight();
    m_xsec		= isData ? 1. : WWZanalysis::EventData::retrieve_xsec(m_xsecFile, m_mcChannelNumber);
    if (m_debugMode) std::cout << "xsec*k_fact*f_eff:	" << m_xsec << std::endl;
    if(!m_theres_cutbookkeeper) {
      m_number_of_events_before_skimming 	+= 1.0;
      m_sum_of_weights_before_skimming   	+= m_mcEventWeight*m_xsec;
    }

    // scale variations: retrieve alternative MC event weights
    // see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/StandardModelASG#Accessing_names_of_entries_in_MC
    m_allMC_weights = isData ? m_allMC_weights : eventInfo->mcEventWeights();
    if ( m_allMC_weights.size()!=m_MCWeightNames.size() ) Warning("execute()","MC weight name list and MC weight vector have different size!");
    else if (m_debugMode) {
      for (unsigned int ww=0; ww<m_allMC_weights.size(); ww++) std::cout << "MC event weight called " << m_MCWeightNames[ww] << " is: " << m_allMC_weights[ww] << std::endl;
    }
    

    // correct data mu values to account for latest lumi calculation recommendations
    m_mu 		= (isData && !m_isAODfile) ? m_PileupReweightingTool->getCorrectedAverageInteractionsPerCrossing(*eventInfo, true) : eventInfo->averageInteractionsPerCrossing();
    // Apply nominal pileup reweighting
    // IMPORTANT: the pileup reweighting tool needs to be applied even if the pileup weight is later taken to be to 1.0, because
    // some other tools' operation depends on it!
    m_pileup_weight 		= 1.0;
    m_pileup_weight_UP 		= 1.0;
    m_pileup_weight_DOWN 	= 1.0;
    if (!isData && m_PileupReweightingTool && !m_isAODfile) {
      SystematicVariations::apply(CP::SystematicSet(), m_PileupReweightingTool);
      m_PileupReweightingTool->apply(*eventInfo, true);
      m_pileup_weight = m_PileupReweightingTool->getCombinedWeight(*eventInfo);
      if (m_pileup_weight == 0.0) {
        Warning("execute()", "Pileup reweighting event weight is zero -> <mu> = 	%f",eventInfo->averageInteractionsPerCrossing());
      }
      if (m_debugMode) std::cout << "And now taking care of pileup reweighting SF systematics..." << std::endl;
      if (m_doSystematics) {
        CP::SystematicSet tmpSet_PRW_UP; tmpSet_PRW_UP.insert(CP::SystematicVariation("PRW_DATASF",1));
        m_PileupReweightingTool->applySystematicVariation(tmpSet_PRW_UP);
        m_pileup_weight_UP = m_PileupReweightingTool->getCombinedWeight(*eventInfo);
        CP::SystematicSet tmpSet_PRW_DOWN; tmpSet_PRW_DOWN.insert(CP::SystematicVariation("PRW_DATASF",-1));
        m_PileupReweightingTool->applySystematicVariation(tmpSet_PRW_DOWN);
        m_pileup_weight_DOWN = m_PileupReweightingTool->getCombinedWeight(*eventInfo);
        if (m_debugMode) std::cout << "m_pileup_weight:	" << m_pileup_weight << "	m_pileup_weight_UP:	" << m_pileup_weight_UP << "	m_pileup_weight_DOWN:	" << m_pileup_weight_DOWN << std::endl;
        // Reset tool
        m_PileupReweightingTool->applySystematicVariation(CP::SystematicSet());
      }
    }

    

  

    //
    // Hard scattering vertex selection
    //
    xAOD::VertexContainer const * reco_primary_vertices = nullptr;
    ANA_CHECK(event->retrieve( reco_primary_vertices, "PrimaryVertices" ));
    const auto * primary_vertex = VertexSelection::get_hard_scattering_vertex(reco_primary_vertices);
    bool const has_primary_vertex = (primary_vertex);
    if (m_debugMode) std::cout << "has_primary_vertex:		" << has_primary_vertex << std::endl;
    m_has_PV = has_primary_vertex;
    // remove events w/o a PV candidate
    if (!m_has_PV) { std::cout << "NO PRIMARY VERTEX FOUND!" << std::endl; return EL::StatusCode::SUCCESS; }




    //
    // Muon selection and corrections
    //
    const xAOD::MuonContainer *raw_allRecoMuons = nullptr;
    ANA_CHECK(event->retrieve( raw_allRecoMuons, "Muons" ));
    // shallow copy to calibrate muons
    auto muons_shallowCopy = xAOD::shallowCopyContainer (*raw_allRecoMuons);
    std::unique_ptr<xAOD::MuonContainer>       	shallowMuons     (muons_shallowCopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> 	muons_shallowAux (muons_shallowCopy.second);
    // deep copy
    auto calib_muons         = std::make_unique<xAOD::MuonContainer>();
    auto calib_muonsAux      = std::make_unique<xAOD::AuxContainerBase>();
    calib_muons->setStore (calib_muonsAux.get()); //< Connect the two
    for (auto muon : *shallowMuons) {
      if (m_debugMode) std::cout << "BEFORE pT:  	" << muon->pt() << std::endl;
      if (m_debugMode) std::cout << "BEFORE eta:  	" << muon->eta() << std::endl;
      double abs_eta_max = m_muon_eta_cut + 0.1;// add a bit of abs(eta) margin
      auto object_within_tool_range = (std::abs(muon->eta()) < abs_eta_max);
      if (m_MuonCalibrationAndSmearingTool) {
        if(!m_MuonCalibrationAndSmearingTool->applyCorrection(*muon)) {
          throw std::logic_error("Error in m_MuonCalibrationAndSmearingTool: cannot apply correction!");
        }
        // selecting muons within abs_eta_max = m_muon_eta_cut + 0.1 range
        else if (object_within_tool_range) { 
          m_MuonCalibrationAndSmearingTool->applyCorrection(*muon); 
          xAOD::Muon* calib_muon = new xAOD::Muon();
          calib_muons->push_back (calib_muon);
          *calib_muon = *muon; 
        }
        //else if (m_debugMode) std::cout << "I'm outta range! eta:	" << std::abs(muon->eta()) << std::endl;
      }
      else { throw std::logic_error("MuonCalibrationAndSmearingTool: NOT DEFINED!"); }
      if (m_debugMode) std::cout << "AFTER pT:  	" << muon->pt() << std::endl;
      if (m_debugMode) std::cout << "AFTER eta:  	" << muon->eta() << std::endl;
    } //for (auto muon : *calib_muons)

    // Put all muon scale factor tools into a vector
    //auto const muon_scale_factor_tools = !isData  ?
    //std::vector<CP::MuonEfficiencyScaleFactors *> { m_muon_efficiency_scale_factor_tool, m_muon_vertex_association_scale_factor_tool, m_muon_isolation_scale_factor_tool } :
    //std::vector<CP::MuonEfficiencyScaleFactors *> {}; // no scale factors applied to real data
    // Apply scale factor weights to the muons
    //auto const corrected_muons = ScaleFactors::get_decorated_with_weights<xAOD::Muon>(calibMuons, muon_scale_factor_tools);

    // Apply basic muon selection using CP tool
    // FIXME: when SFs will be available here...
    auto beforeOLR_muons         = std::make_unique<xAOD::MuonContainer>();
    auto beforeOLR_muonsAux      = std::make_unique<xAOD::AuxContainerBase>();
    beforeOLR_muons->setStore (beforeOLR_muonsAux.get()); //< Connect the two 
    for (auto muon : *calib_muons) {
       // MuonSelectionTool
       if(!m_MuonSelectionTool_looseID->accept(*muon)) 		continue;
       // isolation
       if(m_opt_isosel_doLepIso && !m_IsolationSelectionTool->accept(*muon))    	continue;
       // muon pT cut
       if(!(muon->pt()/1000. > m_muon_pt_cut)) 		continue;
       // z0sintheta cut
       auto const *track  				= muon->primaryTrackParticle();
       auto const muon_z0sinT 				= GenericObjectSelection::calculate_abs_z0sintheta(track, primary_vertex);
       if(!(muon_z0sinT < m_muon_z0sinT_cut))		continue;
       // d0 sig cut
       auto const abs_muon_sigd0			= std::abs(xAOD::TrackingHelpers::d0significance(track, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY()));
       if(!(abs_muon_sigd0 < m_muon_sigd0_cut))		continue;	
       // d0 cut
       auto const abs_d0 				= std::abs(track->d0());
       if(!(abs_d0 < m_muon_d0_cut))			continue;

       // fill deep copy with muons passing the overall (- OLR) selection
       xAOD::Muon* beforeOLR_muon = new xAOD::Muon();
       beforeOLR_muons->push_back (beforeOLR_muon);
       *beforeOLR_muon = *muon;
       
       // fill related branches
       m_beforeOLR_vmuon_E		.push_back( muon->e()										);
       m_beforeOLR_vmuon_pt		.push_back( muon->pt()										);
       m_beforeOLR_vmuon_eta		.push_back( muon->eta()										);
       m_beforeOLR_vmuon_phi		.push_back( muon->phi()										);
       m_beforeOLR_vmuon_q		.push_back( muon->charge()									);
       m_beforeOLR_vmuon_d0		.push_back( track->d0()										);
       m_beforeOLR_vmuon_z0		.push_back( fabs(track->z0() + track->vz() - primary_vertex->z())				);
       m_beforeOLR_vmuon_z0sinT		.push_back( fabs(track->z0() + track->vz() - primary_vertex->z()) * sin(track->theta())		);
    } //for (auto muon : *calib_muons)
    if (m_debugMode) std::cout << "RAW muon size:                       	" << muons_shallowCopy.first->size() << std::endl;
    if (m_debugMode) std::cout << "SELECTED muon size:          		" << beforeOLR_muons->size() << std::endl;




    //
    // Electron selection and corrections
    //
    const xAOD::ElectronContainer  *raw_RecoElectrons = nullptr;
    ANA_CHECK(event->retrieve( raw_RecoElectrons, "Electrons" ));
    // shallow copy to calibrate electrons
    auto electrons_shallowCopy = xAOD::shallowCopyContainer (*raw_RecoElectrons);
    std::unique_ptr<xAOD::ElectronContainer>    shallowElectrons     	(electrons_shallowCopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> 	electrons_shallowAux 	(electrons_shallowCopy.second);

    // Put all electron scale factor tools into a vector
    //auto const electron_scale_factor_tools =  is_MC_event ?
    //  std::vector<AsgElectronEfficiencyCorrectionTool *> {
    //    m_electron_reconstruction_efficiency_tool,
    //    m_electron_identification_efficiency_tool,
    //    m_electron_isolation_efficiency_tool} :
    //  std::vector<AsgElectronEfficiencyCorrectionTool *> {}; // no scale factors applied to real data
    // Apply scale factor weights to the electrons
    //auto const corrected_electrons = ScaleFactors::get_decorated_with_weights<xAOD::Electron>(objects.reco_corrected_electrons, electron_scale_factor_tools);

    for(auto ele: *shallowElectrons) {
      if (m_debugMode) std::cout << "BEFORE pT:  	" << ele->pt() << std::endl;
      if (m_debugMode) std::cout << "BEFORE eta:  	" << ele->eta() << std::endl;
      if(m_EgammaCalibrationAndSmearingTool && !m_EgammaCalibrationAndSmearingTool->applyCorrection(*ele)) {
        throw std::logic_error("Error in EgammaCalibrationAndSmearingTool: cannot apply correction!");
      }
      // Commenting out isolation correction, see https://its.cern.ch/jira/browse/ATLEGAMDPD-75
      // Application of iso corrections on electrons is currently (r21.2.25) not needed, see https://its.cern.ch/jira/browse/ATLEGAMDPD-75
//      if(m_IsolationCorrectionTool && !m_IsolationCorrectionTool->applyCorrection(*ele)) {
//        throw std::logic_error("Error in IsolationCorrectionTool: cannot apply correction!");
//      }
      m_EgammaCalibrationAndSmearingTool->applyCorrection(*ele);
      // Commenting out isolation correction, see https://its.cern.ch/jira/browse/ATLEGAMDPD-75
      // Application of iso corrections on electrons is currently (r21.2.25) not needed, see https://its.cern.ch/jira/browse/ATLEGAMDPD-75
//      m_IsolationCorrectionTool->applyCorrection(*ele);
      if (m_debugMode) std::cout << "AFTER pT:  		" << ele->pt() << std::endl;
      if (m_debugMode) std::cout << "AFTER eta:  	" << ele->eta() << std::endl;
    } //for(auto electron: *electrons_shallowCopy)
    // Apply basic electron selection using CP tool
    // FIXME: when SFs will be available here...
    auto beforeOLR_electrons         = std::make_unique<xAOD::ElectronContainer>();
    auto beforeOLR_electronsAux      = std::make_unique<xAOD::AuxContainerBase>();
    beforeOLR_electrons->setStore (beforeOLR_electronsAux.get()); //< Connect the two 
    // could do the two things in one go...
    for(auto ele: *shallowElectrons) {
      // eta cut
      auto const electron_cluster_eta = std::abs(ele->caloCluster()->etaBE(2));
      // Removing el within crack region (1.37<eta<1.52)
      if (m_debugMode) std::cout << "m_electron_eta_cut:		" << m_electron_eta_cut << std::endl;
      if (m_debugMode) std::cout << "m_electron_pt_cut:			" << m_electron_pt_cut << std::endl;
      if (m_debugMode) std::cout << "m_electron_z0sinT_cut:		" << m_electron_z0sinT_cut << std::endl;
      auto const out_of_crack						= (electron_cluster_eta<1.37 || electron_cluster_eta>1.52);
      auto const in_eta_range						= (electron_cluster_eta < m_electron_eta_cut);
      if (!(in_eta_range && out_of_crack)) 				continue;
      // pT cut
      if(!(ele->pt()/1000. > m_electron_pt_cut))			continue;
      // z0sinT cut
      auto const *track                 				= ele->trackParticle();
      auto const ele_z0sinT             				= GenericObjectSelection::calculate_abs_z0sintheta(track, primary_vertex);
      if(!(ele_z0sinT < m_electron_z0sinT_cut))   			continue;
      // d0 sig cut
      auto const abs_ele_sigd0						= std::abs(xAOD::TrackingHelpers::d0significance(track, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY()));
      if(!(abs_ele_sigd0 < m_electron_sigd0_cut))			continue;	
      // isolation cut
      if(m_opt_isosel_doLepIso && !m_IsolationSelectionTool->accept(*ele) && m_debugMode)	{ std::cout << "I didn't pass the electron isolation reqs!" << std::endl; }
      if(m_opt_isosel_doLepIso && !m_IsolationSelectionTool->accept(*ele)) 			continue;
      // electron ID
      // Electron LH is not supposed to be computed by analyses FWs anymore, but retrieved from DAODs, see https://groups.cern.ch/group/hn-atlas-EGammaWG/Lists/Archive/Flat.aspx?RootFolder=%2fgroup%2fhn%2datlas%2dEGammaWG%2fLists%2fArchive%2fMissing%20likelihood%20variables%20in%20default%20derivation%20lists&FolderCTID=0x01200200D17DAC8476E41D4F8451088E75A5CE34
      bool passes(false);
      if(ele->isAvailable<char>(("DFCommonElectronsLHLooseBL"))) {
        passes								= static_cast<bool>(ele->auxdata<char>("DFCommonElectronsLHLooseBL"));
        if (m_debugMode) std::cout << "DFCommonElectronsLHLooseBL is available! -> passes:		" << passes << std::endl;
      }
      else {
        if (m_debugMode) std::cout << "DFCommonElectronsLHLooseBL aux decoration is NOT available!" << std::endl;
	passes 								= m_AsgElectronLikelihoodTool_looseID->accept(ele);
      }

      if(!passes) 							continue;

      // fill deep copy with electrons passing the overall (- OLR) selection
      xAOD::Electron* beforeOLR_electron = new xAOD::Electron();
      beforeOLR_electrons->push_back (beforeOLR_electron);
      *beforeOLR_electron = *ele;

      m_beforeOLR_velectron_E                 .push_back( ele->e()                     		            	);
      m_beforeOLR_velectron_pt                .push_back( ele->pt()                    		            	);
      m_beforeOLR_velectron_eta               .push_back( ele->eta()                   		            	);
      m_beforeOLR_velectron_phi               .push_back( ele->phi()                   		            	);
      m_beforeOLR_velectron_q                 .push_back( ele->charge()                		            	);
      m_beforeOLR_velectron_d0                .push_back( track->d0()                                                                         	);
      m_beforeOLR_velectron_z0                .push_back( fabs(track->z0() + track->vz() - primary_vertex->z())                               	);
      m_beforeOLR_velectron_z0sinT            .push_back( fabs(track->z0() + track->vz() - primary_vertex->z()) * sin(track->theta())         	);

    }
    if (m_debugMode) std::cout << "FRESH beforeOLR_electrons size:	" << beforeOLR_electrons->size() <<std::endl;



    //
    // Jet selection and corrections
    //
    // get jet container of interest
    const xAOD::JetContainer *jets = nullptr;
    ANA_CHECK(event->retrieve( jets, "AntiKt4EMTopoJets" ));

    // N.B. if you want to apply some sort of modification/calibration to all objects in an input container, but you don't want to perform an object selection at the same time, then the best idea is to make a "shallow copy" of the input container. This is done with the help of xAOD::shallowCopyContainer (https://gitlab.cern.ch/atlas/athena/blob/21.2/Event/xAOD/xAODCore/xAODCore/ShallowCopy.h). 
    // shallow copy of all reco jets
    auto jets_shallowCopy = xAOD::shallowCopyContainer (*jets);
    std::unique_ptr<xAOD::JetContainer> 	shallowJets 	(jets_shallowCopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> 	jets_shallowAux (jets_shallowCopy.second);
    // iterate over the shallow copy
    for (auto jetSC : *jets_shallowCopy.first) {
      if (m_debugMode) std::cout << std::endl; 
      if (m_debugMode) std::cout << "BEFORE:	" << jetSC->pt() << std::endl;
      if(!m_JetCalibrationTool->applyCorrection(*jetSC)) {
        throw std::logic_error("Error in JetCalibrationTool: cannot apply correction!");
      }
      if (m_debugMode) std::cout << "AFTER:	" << jetSC->pt() << std::endl;
    }

    // Jet energy scale correction
    // Nominal energy scale, resolution and JES-corrected jets
    // iterate over the shallow copy
    if (m_debugMode) std::cout << "JetUncertaintiesTool..." << std::endl;
    for (auto jetSC : *jets_shallowCopy.first) {
      if (m_debugMode) std::cout << std::endl; 
      if (m_debugMode) std::cout << "BEFORE:        " << jetSC->pt() << std::endl;
      if(!m_JetUncertaintiesTool->applyCorrection(*jetSC)) {
        throw std::logic_error("Error in JetUncertaintiesTool: cannot apply correction!");
      }
      if (m_debugMode) std::cout << "AFTER:    " << jetSC->pt() << std::endl;
    }
    // Jet energy smearing correction
    // Finally... calibrated jets
    if (m_debugMode) std::cout << "JERSmearingTool..." << std::endl;
    for (auto jetSC : *jets_shallowCopy.first) {
      if (m_debugMode) std::cout << std::endl; 
      if (m_debugMode) std::cout << "BEFORE:        " << jetSC->pt() << std::endl;
      if(!m_JERSmearingTool->applyCorrection(*jetSC)) {
        throw std::logic_error("Error in JERSmearingTool: cannot apply correction!");
      }
      if (m_debugMode) std::cout << "AFTER:    " << jetSC->pt() << std::endl;
    }
    // Decorating xAOD::JetContainer with updated JVT
    JetSelection::decorate_with_updated_JVT(jets_shallowCopy.first, m_JetVertexTaggerTool);
    // Decorating xAOD::JetContainer with b-tagging SFs
    BTaggingScaleFactors::decorate_with_bTagSFs(jets_shallowCopy.first, m_BTaggingEfficiencyTool, m_BTaggingSelectionTool);
    // Forward JVT: NEEDED ??
    //JetSelection::decorate_with_fwd_JVT(jets_shallowCopy.first, m_JetVertexTaggerTool);
    // N.B. deep copying will create new objects/containers that have all the attributes (aka variables) of the original container. This is useful when you are only interested in objects that pass certain criteria.
    // Create the new container and its auxiliary store.
    auto beforeOLR_jets 	= std::make_unique<xAOD::JetContainer>();
    auto beforeOLR_jetsAux 	= std::make_unique<xAOD::AuxContainerBase>();
    beforeOLR_jets->setStore (beforeOLR_jetsAux.get()); //< Connect the two
    for(auto jet : *jets_shallowCopy.first ) {
      if (m_debugMode) std::cout << "Updated JVT:      " << jet->auxdata<float>("JVTName") << std::endl;
      float jet_jvt = jet->auxdata<float>("JVTName");
      // apply jet cleaning
      if( !m_jetCleaning->accept( *jet) )                                     	continue;

      // including forward jets after cleaning
      int jet_coneLabel	= isData ? -1 : jet->getAttribute<int>("HadronConeExclTruthLabelID");;//jet->getAttribute<int>("ConeTruthLabelID");
      if( fabs(jet->eta())>=m_jet_eta_cut ) {
        m_forward_vjet_E                	.push_back( jet->e()            );
        m_forward_vjet_pt               	.push_back( jet->pt()           );
        m_forward_vjet_eta              	.push_back( jet->eta()          );
        m_forward_vjet_phi              	.push_back( jet->phi()          );
        m_forward_vjet_jvt              	.push_back( jet_jvt             );
        m_forward_vjet_coneLabel        	.push_back( jet_coneLabel       ); 
      }
      // including forward jets after cleaning

      // select jets before OLR
      if( jet->pt()/1000.<m_jet_pt_cut || fabs(jet->eta())>m_jet_eta_cut || (jet->pt()/1000.<60. && jet_jvt<m_jet_jvt_cut) )   		continue;

      // Copy this jet to the output container:
      xAOD::Jet* beforeOLR_jet = new xAOD::Jet();
      beforeOLR_jets->push_back (beforeOLR_jet); // jet acquires the goodJets auxstore
      *beforeOLR_jet = *jet; // copies auxdata from one auxstore to the other

      // Fill branches
      m_beforeOLR_vjet_E    		.push_back( jet->e()		);
      m_beforeOLR_vjet_pt   		.push_back( jet->pt()		);
      m_beforeOLR_vjet_eta  		.push_back( jet->eta()		);
      m_beforeOLR_vjet_phi 		.push_back( jet->phi()		);
      m_beforeOLR_vjet_jvt  		.push_back( jet_jvt		);
      m_beforeOLR_vjet_coneLabel 	.push_back( jet_coneLabel	);
    }
    if (m_debugMode) std::cout << "RAW jet size:			" << jets_shallowCopy.first->size() << std::endl;
    if (m_debugMode) std::cout << "SELECTED jet size:		" << beforeOLR_jets->size() << std::endl;





    //
    // Overlap removal procedure 
    //
    // with get() we're losing ownership of unique pointers and they're empty afterwards...
    // EventData::decorate_all( beforeOLR_electrons   	,"include_in_OLR", static_cast<char>(true));
    // EventData::decorate_all( beforeOLR_muons   	,"include_in_OLR", static_cast<char>(true));
    // EventData::decorate_all( beforeOLR_jets   	,"include_in_OLR", static_cast<char>(true));
    // hard-coded version atm...
    // decorate objects
    char def_char('1');
    if (m_debugMode) std::cout << "beforeOLR_electrons size:	" << beforeOLR_electrons->size() <<std::endl;
    for(auto ele: *beforeOLR_electrons) {
      if(!ele) throw std::logic_error("Error in ele OLR decoration: object pointer is null!");
      ele->auxdecor<char>("include_in_OLR") = def_char;
      //if (m_debugMode) std::cout << "char:	" << ele->auxdata<char>("include_in_OLR") << std::endl;
    }
    for(auto muon: *beforeOLR_muons) {
      if(!muon) throw std::logic_error("Error in muon OLR decoration: object pointer is null!");
      muon->auxdecor<char>("include_in_OLR") = def_char;
      //if (m_debugMode) std::cout << "char:	" << muon->auxdata<char>("include_in_OLR") << std::endl;
    }
    for(auto jet: *beforeOLR_jets) {
      if(!jet) throw std::logic_error("Error in jet OLR decoration: object pointer is null!");
      jet->auxdecor<char>("include_in_OLR") = def_char;
      //if (m_debugMode) std::cout << "char:	" << jet->auxdata<char>("include_in_OLR") << std::endl;
    }

    // remove overlap through ASG OLR tool
    auto &tool = m_OLR->masterTool;
    ANA_CHECK( tool->removeOverlaps(beforeOLR_electrons.get(), beforeOLR_muons.get(), beforeOLR_jets.get(), 0, 0, 0) );

/*    
    for(auto ele: *beforeOLR_electrons) {
      if (m_debugMode) std::cout << "Electron: DO I OVERLAP?	->	"<< static_cast<bool>(ele->auxdata<char>("overlaps") )  << std::endl;
    }   
    for(auto muon: *beforeOLR_muons) {
      if (m_debugMode) std::cout << "Muon: DO I OVERLAP?		->	"<< static_cast<bool>(muon->auxdata<char>("overlaps"))  << std::endl;
    }
    for(auto jet: *beforeOLR_jets) {
      if (m_debugMode) std::cout << "Jet: DO I OVERLAP?		->	"<< static_cast<bool>(jet->auxdata<char>("overlaps") )  << std::endl;
    }
*/

    std::vector<xAOD::Electron	*> 		final_electrons;
    std::vector<xAOD::Muon    	*> 		final_muons;
    std::vector<xAOD::Jet     	*> 		final_jets;
    for(auto ele:  *beforeOLR_electrons) 	{ if( !static_cast<bool>(ele->auxdata<char>("overlaps")) ) 	final_electrons.push_back(ele); 	}
    for(auto ele:  *beforeOLR_electrons) 	{ if (m_debugMode) std::cout << "ele pt:		" << ele->pt() << std::endl; 	}
    for(auto muon: *beforeOLR_muons) 		{ if( !static_cast<bool>(muon->auxdata<char>("overlaps")) ) 	final_muons.push_back(muon); 		}
 
    // need jet container for applyAllEfficiencyScaleFactor() method
    //for(auto jet:  *beforeOLR_jets) 		{ if( !static_cast<bool>(jet->auxdata<char>("overlaps")) ) 	final_jets.push_back(jet); 		}
    ConstDataVector<xAOD::JetContainer> afterOLR_jets(SG::VIEW_ELEMENTS);
    for(auto jet:  *beforeOLR_jets) 		{ 
      if( static_cast<bool>(jet->auxdata<char>("overlaps")) ) 	continue;
      final_jets.push_back(jet); 		
      afterOLR_jets.push_back(jet);
    }
    //
    // Overlap removal procedure 
    //


    // after OLR: event cleaning
    // performed as done here: https://gitlab.cern.ch/atlasHTop/ttHMultiAna/blob/ttHMultiAna-02-04-32/Root/ttHMultileptonLooseEventSaver.cxx#L546 
    // plus see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJets2017#Event_Cleaning_Tool
//    bool eventCleaning 	= m_EventCleaningTool->acceptEvent(afterOLR_jets.asDataVector());
//    m_passEventCleaning	= ( (eventCleaning) && (eventInfo->errorState(xAOD::EventInfo::Tile)!=xAOD::EventInfo::Error) && (eventInfo->errorState(xAOD::EventInfo::LAr)!=xAOD::EventInfo::Error) );
    m_passEventCleaning	= ( (eventInfo->errorState(xAOD::EventInfo::Tile)!=xAOD::EventInfo::Error) && (eventInfo->errorState(xAOD::EventInfo::LAr)!=xAOD::EventInfo::Error) );
 



    //
	    // TRIGGER MATCHING 
	    //
    // the MatchingTool takes a vector of IParticles, and a trigger chain expression
    // need double for loop for dilepton triggers, see https://twiki.cern.ch/twiki/bin/viewauth/Atlas/XAODMatchingTool#Configuring_and_using_the_matchi
    // For an example see https://svnweb.cern.ch/trac/atlasoff/browser/Trigger/TrigAnalysis/TriggerMatchingTool/trunk/src/TestMatchingToolAlg.cxx
    m_isTrigMatched = false;
    std::vector<const xAOD::IParticle*> myParticles;
    // single lepton triggers
    // el triggers
    for(uint e=0; e<final_electrons.size(); e++) {
      if( m_RunYear==2015 ) {
        if( final_electrons[e]->pt()<=25e3 ) continue;
        myParticles.clear();
        myParticles.push_back( final_electrons[e] ); 
        if( m_MatchingTool->match(myParticles,"HLT_e24_lhmedium_L1EM20VH", 0.07) )				m_isTrigMatched = true;
        if( m_MatchingTool->match(myParticles,"HLT_e60_lhmedium", 0.07) )					m_isTrigMatched = true;
        if( m_MatchingTool->match(myParticles,"HLT_e120_lhloose", 0.07) ) 					m_isTrigMatched = true;
      }
      else if( m_RunYear==2016 || m_RunYear==2017 ) {
        if( final_electrons[e]->pt()<=27e3 ) continue;
        myParticles.clear();
        myParticles.push_back( final_electrons[e] ); 
        if( m_MatchingTool->match(myParticles,"HLT_e26_lhtight_nod0_ivarloose", 0.07) )				m_isTrigMatched = true;
        if( m_MatchingTool->match(myParticles,"HLT_e60_lhmedium_nod0", 0.07) )					m_isTrigMatched = true;
        if( m_MatchingTool->match(myParticles,"HLT_e140_lhloose_nod0", 0.07) ) 					m_isTrigMatched = true;
      }
    }
    // mu triggers
    for(uint m=0; m<final_muons.size(); m++) {
      if( m_RunYear==2015 ) {
        if( final_muons[m]->pt()<=21e3 ) continue;
        myParticles.clear();
        myParticles.push_back( final_muons[m] );
        if( m_MatchingTool->match(myParticles,"HLT_mu20_iloose_L1MU15", 0.07) )					m_isTrigMatched = true;
        if( m_MatchingTool->match(myParticles,"HLT_mu50", 0.07) ) 						m_isTrigMatched = true;
      }
      else if( m_RunYear==2016 || m_RunYear==2017 ) {
        if( final_muons[m]->pt()<=27e3 ) continue;
        myParticles.clear();
        myParticles.push_back( final_muons[m] );
        if( m_MatchingTool->match(myParticles,"HLT_mu26_ivarmedium", 0.07) )					m_isTrigMatched = true;
        if( m_MatchingTool->match(myParticles,"HLT_mu50", 0.07) ) 						m_isTrigMatched = true;
      }
    }
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // dilepton triggers need special treatment, see https://svnweb.cern.ch/trac/atlasoff/browser/Trigger/TrigAnalysis/TriggerMatchingTool/trunk/src/TestMatchingToolAlg.cxx#L88
    // mu triggers
    /*
    for(uint i=0; i<final_muons.size(); i++) {
      if( final_muons[i]->pt()<=9e3 ) continue;
      for(uint j=0; j<final_muons.size(); j++) {
        if( final_muons[j]->pt()<=9e3 ) continue;
        if (i>=j) continue;
        myParticles.clear();
        myParticles.push_back( final_muons[i] );
        myParticles.push_back( final_muons[j] );
        if( m_RunYear==2015 ) {
          if( m_MatchingTool->match(myParticles,"HLT_mu18_mu8noL1", 0.07) )          				m_isTrigMatched = true;
        }
        else if( m_RunYear==2016 || m_RunYear==2017 ) {
          if( m_MatchingTool->match(myParticles,"HLT_mu22_mu8noL1", 0.07) )          				m_isTrigMatched = true;
        }
        //if( m_MatchingTool->match(myParticles,"HLT_2mu14", 0.07) )          					m_isTrigMatched = true;
      }
    }
    // el triggers
    for(uint i=0; i<final_electrons.size(); i++) {
      if( m_RunYear==2015 ) {
        if( final_electrons[i]->pt()<=13e3 ) continue;
        for(uint j=0; j<final_electrons.size(); j++) {
          if( final_electrons[j]->pt()<=13e3 ) continue;
          if (i>=j) continue;
          myParticles.clear();
          myParticles.push_back( final_electrons[i] );
          myParticles.push_back( final_electrons[j] );
          if( m_MatchingTool->match(myParticles,"HLT_2e12_lhloose_L12EM10VH", 0.07) )   			m_isTrigMatched = true;
        }
      }
      else if( m_RunYear==2016 ) {
        if( final_electrons[i]->pt()<=18e3 ) continue;
        for(uint j=0; j<final_electrons.size(); j++) {
          if( final_electrons[j]->pt()<=18e3 ) continue;
          if (i>=j) continue;
          myParticles.clear();
          myParticles.push_back( final_electrons[i] );
          myParticles.push_back( final_electrons[j] );
          if( m_MatchingTool->match(myParticles,"HLT_2e17_lhvloose_nod0", 0.07) )     				m_isTrigMatched = true;
        }
      }
      else if( m_RunYear==2017 ) {
        if( final_electrons[i]->pt()<=25e3 ) continue;
        for(uint j=0; j<final_electrons.size(); j++) {
          if( final_electrons[j]->pt()<=25e3 ) continue;
          if (i>=j) continue;
          myParticles.clear();
          myParticles.push_back( final_electrons[i] );
          myParticles.push_back( final_electrons[j] );
          if( m_MatchingTool->match(myParticles,"HLT_2e24_lhvloose_nod0", 0.07) )     				m_isTrigMatched = true;
        }
      }
    }
    // combined triggers need special treatment, see https://svnweb.cern.ch/trac/atlasoff/browser/Trigger/TrigAnalysis/TriggerMatchingTool/trunk/src/TestMatchingToolAlg.cxx#L45 
    for(uint i=0; i<final_electrons.size(); i++) {
      for(uint j=0; j<final_muons.size(); j++) {
        if( final_muons[j]->pt()<=15e3 ) continue;
        myParticles.clear();
        myParticles.push_back( final_electrons[i] );
        myParticles.push_back( final_muons[j] ); 
        if( m_RunYear==2015 ) {
          if( m_MatchingTool->match(myParticles,"HLT_e17_lhloose_mu14", 0.07) )     					m_isTrigMatched = true;
        }
        else if( m_RunYear==2016 || m_RunYear==2017 ) {
          if( m_MatchingTool->match(myParticles,"HLT_e17_lhloose_nod0_mu14", 0.07) )     				m_isTrigMatched = true;
        }
        //if( m_MatchingTool->match(myParticles,"HLT_e26_lhmedium_nod0_mu8noL1", 0.07) )   				m_isTrigMatched = true;
        //if( m_MatchingTool->match(myParticles,"HLT_e7_lhmedium_nod0_mu24", 0.07) )   					m_isTrigMatched = true;
      } 
    }
    // trilepton triggers
    // mu triggers
    for(uint i=0; i<final_muons.size(); i++) { 
      for(uint j=0; j<final_muons.size(); j++) {
        for(uint l=0; l<final_muons.size(); l++) {
          if (i==j || i==l || j==l) continue;
          myParticles.clear();
          myParticles.push_back( final_muons[i] );
          myParticles.push_back( final_muons[j] );
          myParticles.push_back( final_muons[l] );
          if( m_MatchingTool->match(myParticles,"HLT_mu20_2mu4noL1", 0.07) )          				m_isTrigMatched = true;
        }
      }
    }
    // el triggers
    for(uint i=0; i<final_electrons.size(); i++) {
      for(uint j=0; j<final_electrons.size(); j++) {
        for(uint l=0; l<final_electrons.size(); l++) {
          if (i==j || i==l || j==l) continue;
          myParticles.clear();
          myParticles.push_back( final_electrons[i] );
          myParticles.push_back( final_electrons[j] );
          myParticles.push_back( final_electrons[l] );
          if( m_MatchingTool->match(myParticles,"HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH", 0.07) )	m_isTrigMatched = true;
        }
      }
    }
    */
    //Info("execute()", "Trigger-matching = %d", m_isTrigMatched);
    

    // NEW: when running on data select events passing trigger matching
    //if ( isData && !m_isTrigMatched ) return EL::StatusCode::SUCCESS;



    //
    // Final event selection 
    //

    // minimum number of leptons
    if( final_electrons.size()+final_muons.size() < m_Nleptons_cut ) 	return EL::StatusCode::SUCCESS; // go to next event
    // minimum number of jets
    if( final_jets.size() < m_Njets_cut ) 				return EL::StatusCode::SUCCESS; // go to next event

    //
    // Final event selection 
    //







 

    //
    // Sort objects by pT
    //

    // combine electrons and muons in an unique vector
    auto const final_leptons	= UtilitiesFunctions::combine(final_electrons, final_muons);

    //for(unsigned int e=0; e<final_electrons.size(); e++) 	if (m_debugMode) std::cout << "UNSORTED ele pt:	" << final_electrons[e]->pt()/1000. << "	ele eta:	" << final_electrons[e]->eta() << std::endl;

    //if (m_debugMode) std::cout << "final_leptons.size(): 	" << final_leptons.size() << std::endl;
    auto const sorted_leptons	= Utilities::sort_by_pT_pointers(final_leptons);
    //if (m_debugMode) std::cout << "sorted_leptons.size(): 	" << sorted_leptons.size() << std::endl;
    auto const sorted_electrons = Utilities::sort_by_pT_pointers(final_electrons);
    auto const sorted_muons 	= Utilities::sort_by_pT_pointers(final_muons);
    auto const sorted_jets 	= Utilities::sort_by_pT_pointers(final_jets);

    //for(unsigned int l=0; l<sorted_leptons.size(); l++) 	if (m_debugMode) std::cout << "SORTED lep pt:	" << sorted_leptons[l]->pt()/1000. << "        lep eta:        " << sorted_leptons[l]->eta() << std::endl;
    //for(unsigned int e=0; e<sorted_electrons.size(); e++) 	if (m_debugMode) std::cout << "SORTED ele pt:	" << sorted_electrons[e]->pt()/1000. << "        ele eta:        " << sorted_electrons[e]->eta() << std::endl;
    

    //
    // Sort objects by pT
    //










    //
    // Final filling of tree branches with final objects
    //
    // PromptLeptonVeto, see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PromptLeptonTagging#How_to_retrieve_decorated_variab
    // Instance of ConstAccessor for the PromptLeptonIsolation BDT weight
    SG::AuxElement::ConstAccessor<float> accessPromptVar( "PromptLeptonVeto" );
    //SG::AuxElement::ConstAccessor<float> accessPromptIsoVar( "PromptLeptonIso_TagWeight" );
    SG::AuxElement::ConstAccessor<float> accessPromptIsoVar( "PromptLeptonIso" );

    // sorted leptons
    if (m_debugMode) std::cout << "sorted_leptons.size():      " << sorted_leptons.size() << std::endl;
    int n_el(-1.), n_mu(-1.);
    for(unsigned int l=0; l<sorted_leptons.size(); l++) {
      // is it el or mu?
      bool isEle 	= (sorted_leptons[l]->type() == xAOD::Type::Electron);
      bool isMu 	= (sorted_leptons[l]->type() == xAOD::Type::Muon);
      if (m_debugMode) std::cout << "particle type:      " << sorted_leptons[l]->type() << std::endl;
      if(isEle)		n_el++; 
      else if(isMu)	n_mu++; 
      else 		throw std::logic_error("IParticle: I'm not an el and nor a mu; What am I?!");
      if (m_debugMode) std::cout << "n_el	:" << n_el << "		n_mu	:" << n_mu << std::endl;
      // fill branches 
      // PromptLeptonVeto
      float plv_weight = -99.;
      if(accessPromptVar.isAvailable(*sorted_leptons[l])) 	plv_weight = accessPromptVar(*sorted_leptons[l]);
      else if(m_eventCounter<500)				Warning("execute()", "PromptLeptonVeto is not available!");
      m_lepton_PromptLeptonVeto					.push_back( plv_weight );
      // PromptLeptonIso
      float pliso_weight = -99.;
      if(accessPromptIsoVar.isAvailable(*sorted_leptons[l])) 	pliso_weight = accessPromptIsoVar(*sorted_leptons[l]);
      else if(m_eventCounter<500)				Warning("execute()", "PromptLeptonIso_TagWeight is not available!");
      m_lepton_PromptLeptonIso_TagWeight			.push_back( pliso_weight );
      
      // Electron ID TightLH WP
      bool isTightLH(1.);
      // Muon ID Tight WP
      bool isTightID(1.);
      // electron
      if(isEle) {
        auto lepton			= sorted_electrons[n_el];
        m_vlepton_ID				.push_back( (-11)*lepton->charge()					);
        m_vlepton_E             		.push_back( lepton->e()                                                	);
        m_vlepton_pt            		.push_back( lepton->pt()                                               	);
        m_vlepton_eta           		.push_back( lepton->eta()                                              	);
        m_vlepton_phi           		.push_back( lepton->phi()                                              	);
        m_vlepton_q             		.push_back( lepton->charge()                                           	);
        const xAOD::TrackParticle *trk 	= lepton->trackParticle();
        m_vlepton_d0            		.push_back( trk->d0()                                                                         	);
        m_vlepton_z0            		.push_back( fabs(trk->z0() + trk->vz() - primary_vertex->z())                               	);
        m_vlepton_z0sinT        		.push_back( fabs(trk->z0() + trk->vz() - primary_vertex->z()) * sin(trk->theta())         	);
        m_vlepton_sigd0         		.push_back( xAOD::TrackingHelpers::d0significance(trk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY())                   ); 
        // Tight ID WP is for muons, just push 1.
        m_vlepton_isTightID			.push_back( isTightID );
        // Tight LH WP
        // Electron LH is not supposed to be computed by analyses FWs anymore, but retrieved from DAODs, see https://groups.cern.ch/group/hn-atlas-EGammaWG/Lists/Archive/Flat.aspx?RootFolder=%2fgroup%2fhn%2datlas%2dEGammaWG%2fLists%2fArchive%2fMissing%20likelihood%20variables%20in%20default%20derivation%20lists&FolderCTID=0x01200200D17DAC8476E41D4F8451088E75A5CE34
        if(lepton->isAvailable<char>(("DFCommonElectronsLHTight"))) {
          isTightLH				= static_cast<bool>(lepton->auxdata<char>("DFCommonElectronsLHTight"));
          if (m_debugMode) std::cout << "DFCommonElectronsLHTight is available! -> isTightLH:              " << isTightLH << std::endl;
        }
        else {
          if (m_debugMode) std::cout << "DFCommonElectronsLHTight aux decoration is NOT available!" << std::endl;
          isTightLH				= m_AsgElectronLikelihoodTool->accept(lepton);
        }
        m_vlepton_isTightLH			.push_back( isTightLH );
        m_vlepton_chargeIDBDTTight		.push_back( (float)m_AsgElectronChargeIDSelectorTool->calculate(lepton) );
        // ambiguity type
        unsigned char ambType(0x09);
        if(lepton->isAvailable<unsigned char>(("ambiguityType"))) {
          ambType				= lepton->auxdata<unsigned char>(("ambiguityType"));
          if (m_debugMode) std::cout << "Ambiguity type decoration is available! -> ambiguityType:		" << ambType << std::endl;
        }
        else std::cout << "WARNING	Ambiguity type decoration is NOT available!" << std::endl;
        m_vlepton_ambiguityType			.push_back( ambType );
        // isolation
        short IsoFixedCutLoose(-1);
        IsoFixedCutLoose = m_IsolationSelectionTool_FixedCutLoose->accept(*lepton) ? 1 : 0;
        m_vlepton_isolationFixedCutLoose	.push_back( IsoFixedCutLoose );
        // lepton SFs
        double SFIDLoose(1.0);      
        if (m_debugMode) std::cout << "electron ID SF before:	" << SFIDLoose << std::endl;
        if (!isData)				m_AsgElectronEfficiencyCorrectionTool_ID->getEfficiencyScaleFactor(*lepton, SFIDLoose);
        if (m_debugMode) std::cout << "electron ID SF after:	" << SFIDLoose << std::endl;
        m_vlepton_SFIDLoose			.push_back( static_cast<float>(SFIDLoose)				); 
        double SFIDTightLH(1.0);      
        if (!isData)				m_AsgElectronEfficiencyCorrectionTool_ID_TightLH->getEfficiencyScaleFactor(*lepton, SFIDTightLH);
        m_vlepton_SFIDTightLH			.push_back( static_cast<float>(SFIDTightLH)				); 
        // isolation
        double SFisoLoose(1.0);
        if (!isData)				m_AsgElectronEfficiencyCorrectionTool_isolation->getEfficiencyScaleFactor(*lepton, SFisoLoose);      
        m_vlepton_SFIsoLoose			.push_back( static_cast<float>(SFisoLoose)				);
        // --- NEW--- PLV isolation
        double SFisoPLV(1.0);
        if (!isData)                            m_AsgElectronEfficiencyCorrectionTool_PLV->getEfficiencyScaleFactor(*lepton, SFisoPLV);
        m_vlepton_SFIsoPLV			.push_back( static_cast<float>(SFisoPLV)				);
        // --- NEW--- PLV isolation
        // reconstruction
        double SFreco(1.0);
        if (!isData)				m_AsgElectronEfficiencyCorrectionTool_reco->getEfficiencyScaleFactor(*lepton, SFreco);
        m_vlepton_SFReco			.push_back( static_cast<float>(SFreco)					);
        // Track-to-vertex association is const 1.0 for electrons!
        const double SFTTVA(1.0);
        m_vlepton_SFTTVA			.push_back( static_cast<float>(SFTTVA)					);
        // total loose object SF
        double SFObjLoose(1.0), SFObjTightLH(1.0);
        SFObjLoose 	= SFIDLoose*SFreco*SFTTVA;
        // NEW: PLV calibration is available, applying additional isolation to tight leptons
        SFObjTightLH 	= SFIDTightLH*SFreco*SFTTVA*SFisoPLV;
        m_vlepton_SFObjLoose			.push_back( static_cast<float>(SFObjLoose)				);
        m_vlepton_SFObjTightLH			.push_back( static_cast<float>(SFObjTightLH)				);
        // electron trigger SF
        double SFtrig(1.0);
        if (!isData)				m_AsgElectronEfficiencyCorrectionTool_trigSF->getEfficiencyScaleFactor(*lepton, SFtrig);
        if(m_debugMode) std::cout << "electron trig SF:		" << SFtrig << std::endl;
        // SFs systematics
        if (m_doSystematics) {
          // electron ID
	  // [0] = UP, [1] = DOWN          
	  std::vector<double> SFIDLoose_EL_SF_ID = {1.,1.};          
          SFIDLoose_EL_SF_ID = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_AsgElectronEfficiencyCorrectionTool_ID, lepton, "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR", SFIDLoose_EL_SF_ID); 
          m_vlepton_SFObjLoose_EL_SF_ID_UP		.push_back( static_cast<float>(SFObjLoose/SFIDLoose*SFIDLoose_EL_SF_ID[0])		);
          m_vlepton_SFObjLoose_EL_SF_ID_DOWN		.push_back( static_cast<float>(SFObjLoose/SFIDLoose*SFIDLoose_EL_SF_ID[1])		);
          if (m_debugMode) std::cout << "SFIDLoose:	" << SFIDLoose << "	 SFIDLoose_EL_SF_ID_UP:		" <<  SFIDLoose_EL_SF_ID[0] << "		SFIDLoose_EL_SF_ID_DOWN:		" << SFIDLoose_EL_SF_ID[1] << std::endl;
          // Tight LH ID
 	  std::vector<double> SFIDTightLH_EL_SF_ID = {1.,1.};          
          SFIDTightLH_EL_SF_ID = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_AsgElectronEfficiencyCorrectionTool_ID_TightLH, lepton, "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR", SFIDTightLH_EL_SF_ID); 
          m_vlepton_SFObjTightLH_EL_SF_ID_UP		.push_back( static_cast<float>(SFObjTightLH/SFIDTightLH*SFIDTightLH_EL_SF_ID[0])	);
          m_vlepton_SFObjTightLH_EL_SF_ID_DOWN		.push_back( static_cast<float>(SFObjTightLH/SFIDTightLH*SFIDTightLH_EL_SF_ID[1])	);
          if (m_debugMode) std::cout << "SFIDTightLH:	" << SFIDTightLH << "	 SFIDTightLH_EL_SF_ID_UP:	" <<  SFIDTightLH_EL_SF_ID[0] << "		SFIDTightLH_EL_SF_ID_DOWN:		" << SFIDTightLH_EL_SF_ID[1] << std::endl;
          // electron Reco 
	  std::vector<double> SFreco_EL_SF_Reco = {1.,1.};          
          SFreco_EL_SF_Reco = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_AsgElectronEfficiencyCorrectionTool_reco, lepton, "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", SFreco_EL_SF_Reco); 
          m_vlepton_SFObjLoose_EL_SF_Reco_UP		.push_back( static_cast<float>(SFObjLoose/SFreco*SFreco_EL_SF_Reco[0])			);
          m_vlepton_SFObjLoose_EL_SF_Reco_DOWN		.push_back( static_cast<float>(SFObjLoose/SFreco*SFreco_EL_SF_Reco[1])			);
          m_vlepton_SFObjTightLH_EL_SF_Reco_UP		.push_back( static_cast<float>(SFObjTightLH/SFreco*SFreco_EL_SF_Reco[0])		);
          m_vlepton_SFObjTightLH_EL_SF_Reco_DOWN	.push_back( static_cast<float>(SFObjTightLH/SFreco*SFreco_EL_SF_Reco[1])		);
          if (m_debugMode) std::cout << "SFreco:	" << SFreco << "	 SFreco_EL_SF_Reco_UP:		" <<  SFreco_EL_SF_Reco[0] << "		SFreco_EL_SF_Reco_DOWN:		" << SFreco_EL_SF_Reco[1] << std::endl;
          // electron Iso 
	  std::vector<double> SFisoLoose_EL_SF_Iso = {1.,1.};          
          SFisoLoose_EL_SF_Iso = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_AsgElectronEfficiencyCorrectionTool_isolation, lepton, "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", SFisoLoose_EL_SF_Iso); 
          m_vlepton_SFObjLoose_EL_SF_Iso_UP		.push_back( static_cast<float>(SFObjLoose/SFisoLoose*SFisoLoose_EL_SF_Iso[0])		);
          m_vlepton_SFObjLoose_EL_SF_Iso_DOWN		.push_back( static_cast<float>(SFObjLoose/SFisoLoose*SFisoLoose_EL_SF_Iso[1])		);
          // NEW: PLV calibration is available, applying additional isolation to tight leptons
          std::vector<double> SFisoLoose_EL_SF_PLV = {1.,1.};
          SFisoLoose_EL_SF_PLV = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_AsgElectronEfficiencyCorrectionTool_PLV, lepton, "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", SFisoLoose_EL_SF_PLV); 
          m_vlepton_SFObjTightLH_EL_SF_Iso_UP		.push_back( static_cast<float>(SFObjTightLH/SFisoPLV*SFisoLoose_EL_SF_PLV[0])		);
          m_vlepton_SFObjTightLH_EL_SF_Iso_DOWN		.push_back( static_cast<float>(SFObjTightLH/SFisoPLV*SFisoLoose_EL_SF_PLV[1])		);
          if (m_debugMode) std::cout << "SFisoLoose:	" << SFisoLoose << "	 SFisoLoose_EL_SF_Iso_UP:		" <<  SFisoLoose_EL_SF_Iso[0] << "		SFisoLoose_EL_SF_Iso_DOWN:		" << SFisoLoose_EL_SF_Iso[1] << std::endl;
          if (m_debugMode) std::cout << "SFisoPLV:	" << SFisoPLV << "	 SFisoLoose_EL_SF_PLV_UP:		" <<  SFisoLoose_EL_SF_PLV[0] << "		SFisoLoose_EL_SF_PLV_DOWN:		" << SFisoLoose_EL_SF_PLV[1] << std::endl;

          // muon ID - does not affect electron SFs
          m_vlepton_SFObjLoose_MU_SF_RECO_STAT_UP   	.push_back( static_cast<float>(SFObjLoose)                              );  
          m_vlepton_SFObjLoose_MU_SF_RECO_STAT_DOWN 	.push_back( static_cast<float>(SFObjLoose)                              );
          m_vlepton_SFObjLoose_MU_SF_RECO_SYST_UP   	.push_back( static_cast<float>(SFObjLoose)                              );  
          m_vlepton_SFObjLoose_MU_SF_RECO_SYST_DOWN 	.push_back( static_cast<float>(SFObjLoose)                              );
          m_vlepton_SFObjTight_MU_SF_RECO_STAT_UP   	.push_back( static_cast<float>(SFObjTightLH)                       	);  
          m_vlepton_SFObjTight_MU_SF_RECO_STAT_DOWN 	.push_back( static_cast<float>(SFObjTightLH)                       	);
          m_vlepton_SFObjTight_MU_SF_RECO_SYST_UP   	.push_back( static_cast<float>(SFObjTightLH)                       	);  
          m_vlepton_SFObjTight_MU_SF_RECO_SYST_DOWN 	.push_back( static_cast<float>(SFObjTightLH)                       	);
          // muon Iso - does not affect electron SFs
          m_vlepton_SFObjLoose_MU_SF_Iso_STAT_UP   	.push_back( static_cast<float>(SFObjLoose)                              );  
          m_vlepton_SFObjLoose_MU_SF_Iso_STAT_DOWN 	.push_back( static_cast<float>(SFObjLoose)                              );
          m_vlepton_SFObjLoose_MU_SF_Iso_SYST_UP   	.push_back( static_cast<float>(SFObjLoose)                              );  
          m_vlepton_SFObjLoose_MU_SF_Iso_SYST_DOWN 	.push_back( static_cast<float>(SFObjLoose)                              );
          m_vlepton_SFObjTight_MU_SF_Iso_STAT_UP	.push_back( static_cast<float>(SFObjTightLH)				);
          m_vlepton_SFObjTight_MU_SF_Iso_STAT_DOWN	.push_back( static_cast<float>(SFObjTightLH)				);
          m_vlepton_SFObjTight_MU_SF_Iso_SYST_UP	.push_back( static_cast<float>(SFObjTightLH)				);
          m_vlepton_SFObjTight_MU_SF_Iso_SYST_DOWN	.push_back( static_cast<float>(SFObjTightLH)				);
          // muon TTVA - does not affect electron SFs
          m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_UP   	.push_back( static_cast<float>(SFObjLoose)                           	);  
          m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN 	.push_back( static_cast<float>(SFObjLoose)                           	);
          m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_UP   	.push_back( static_cast<float>(SFObjLoose)                           	);  
          m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN 	.push_back( static_cast<float>(SFObjLoose)                           	);
          m_vlepton_SFObjTight_MU_SF_TTVA_STAT_UP   	.push_back( static_cast<float>(SFObjTightLH)                         	);  
          m_vlepton_SFObjTight_MU_SF_TTVA_STAT_DOWN 	.push_back( static_cast<float>(SFObjTightLH)                         	);
          m_vlepton_SFObjTight_MU_SF_TTVA_SYST_UP   	.push_back( static_cast<float>(SFObjTightLH)                         	);  
          m_vlepton_SFObjTight_MU_SF_TTVA_SYST_DOWN 	.push_back( static_cast<float>(SFObjTightLH)                         	);
        }

        // truth info
        if( !isData && xAOD::TruthHelpers::getTruthParticle(*lepton) ) {
          int type   = xAOD::TruthHelpers::getParticleTruthType(*lepton);
          int origin = xAOD::TruthHelpers::getParticleTruthOrigin(*lepton);        
          m_vlepton_truthType           .push_back( type                                                        	);
          m_vlepton_truthOrigin         .push_back( origin                                                      	);
        }
        else {
          m_vlepton_truthType           .push_back( -1.                                                         	);
          m_vlepton_truthOrigin         .push_back( -1.                                                         	);
        }
        // total lepton charge
        m_tot_charge			+= lepton->charge();
      }
      // muon
      else {
        auto lepton			= sorted_muons[n_mu];
        m_vlepton_ID				.push_back( (-13)*lepton->charge()					);
        m_vlepton_E             		.push_back( lepton->e()                                                	);
        m_vlepton_pt            		.push_back( lepton->pt()                                               	);
        m_vlepton_eta           		.push_back( lepton->eta()                                              	);
        m_vlepton_phi           		.push_back( lepton->phi()                                              	);
        m_vlepton_q             		.push_back( lepton->charge()                                           	);
        const xAOD::TrackParticle *trk	= lepton->primaryTrackParticle();
        m_vlepton_d0            		.push_back( trk->d0()                                                                         	);
        m_vlepton_z0            		.push_back( fabs(trk->z0() + trk->vz() - primary_vertex->z())                               	);
        m_vlepton_z0sinT        		.push_back( fabs(trk->z0() + trk->vz() - primary_vertex->z()) * sin(trk->theta())         	);
        m_vlepton_sigd0         		.push_back( xAOD::TrackingHelpers::d0significance(trk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY())                   ); 
        // Tight LH WP is for electrons, just push 1.
        m_vlepton_isTightLH                     .push_back( isTightLH );
        m_vlepton_chargeIDBDTTight		.push_back( 1. );
        m_vlepton_ambiguityType			.push_back( 0 );
        // isolation
        short IsoFixedCutLoose(-1);
        IsoFixedCutLoose = m_IsolationSelectionTool_FixedCutLoose->accept(*lepton) ? 1 : 0;
        m_vlepton_isolationFixedCutLoose	.push_back( IsoFixedCutLoose );
        // Muon tight ID WP
        isTightID = m_MuonSelectionTool->accept(*lepton);
        m_vlepton_isTightID			.push_back( isTightID );
        // lepton SFs
        // ID
        float SFIDLoose(1.0);      
        if (!isData)				m_MuonEfficiencyScaleFactors_ID->getEfficiencyScaleFactor(*lepton, SFIDLoose);
        m_vlepton_SFIDLoose			.push_back( SFIDLoose							); 
        // Tight LH WP is for electrons, just push the same as muon Loose ID
        m_vlepton_SFIDTightLH			.push_back( SFIDLoose							); 
        // isolation
        float SFisoLoose(1.0);
        if (!isData)				m_MuonEfficiencyScaleFactors_isolation->getEfficiencyScaleFactor(*lepton, SFisoLoose);
        m_vlepton_SFIsoLoose			.push_back( SFisoLoose							);
        // --- NEW--- PLV isolation
        float SFisoPLV(1.0);
        if (!isData)				m_MuonEfficiencyScaleFactors_PLV->getEfficiencyScaleFactor(*lepton, SFisoPLV);
        m_vlepton_SFIsoPLV			 .push_back( SFisoPLV							);
        // --- NEW--- PLV isolation
        // reconstruction, const 1.0 for muons!
        const float SFreco(1.0);
        m_vlepton_SFReco			.push_back( SFreco							);
        // Muon track-to-vertex-association (TTVA) efficiency scale factor tool
        // See: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesMC16#Muon_track_to_vertex_association
        float SFTTVA(1.0);
        if (!isData)				m_MuonEfficiencyScaleFactors_TTVA->getEfficiencyScaleFactor(*lepton, SFTTVA);
        m_vlepton_SFTTVA			.push_back( SFTTVA							);
        // total loose object SF
        double SFObjLoose(1.0), SFObjTightLH(1.0);
        SFObjLoose 	= SFIDLoose*SFreco*SFTTVA;
        // NEW: PLV calibration is available, applying additional isolation to tight leptons
        SFObjTightLH  	= SFIDLoose*SFreco*SFTTVA*SFisoPLV;
        m_vlepton_SFObjLoose			.push_back( SFObjLoose							);
        m_vlepton_SFObjTightLH			.push_back( SFObjTightLH						);
        // SFs systematics
        if (m_doSystematics) {
          // NEW: PLV calibration is available, applying additional isolation to tight leptons
          m_vlepton_SFObjLoose_EL_SF_ID_UP	.push_back( SFObjLoose						  	);
          m_vlepton_SFObjLoose_EL_SF_ID_DOWN	.push_back( SFObjLoose						  	);
          m_vlepton_SFObjTightLH_EL_SF_ID_UP	.push_back( SFObjTightLH					  	);
          m_vlepton_SFObjTightLH_EL_SF_ID_DOWN	.push_back( SFObjTightLH					  	);
          // NEW: PLV calibration is available, applying additional isolation to tight leptons
	  m_vlepton_SFObjLoose_EL_SF_Reco_UP	.push_back( SFObjLoose						  	);
          m_vlepton_SFObjLoose_EL_SF_Reco_DOWN	.push_back( SFObjLoose						  	);
	  m_vlepton_SFObjTightLH_EL_SF_Reco_UP	.push_back( SFObjTightLH					  	);
          m_vlepton_SFObjTightLH_EL_SF_Reco_DOWN.push_back( SFObjTightLH					  	);
          // NEW: PLV calibration is available, applying additional isolation to tight leptons
          m_vlepton_SFObjLoose_EL_SF_Iso_UP	.push_back( SFObjLoose                                                  );	
	  m_vlepton_SFObjLoose_EL_SF_Iso_DOWN	.push_back( SFObjLoose                                                  );	
          m_vlepton_SFObjTightLH_EL_SF_Iso_UP	.push_back( SFObjTightLH                                              	);	
	  m_vlepton_SFObjTightLH_EL_SF_Iso_DOWN	.push_back( SFObjTightLH                                              	);	

          // muon ID
          // no reco Eff since 2.4.22, see https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/TopPhys/xAOD/TopCorrections/Root/MuonScaleFactorCalculator.cxx#L313
          // stat
          std::vector<float> SFIDLoose_MU_SF_RECO_STAT = {1.,1.};
          SFIDLoose_MU_SF_RECO_STAT = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_MuonEfficiencyScaleFactors_ID, lepton, "MUON_EFF_RECO_STAT", SFIDLoose_MU_SF_RECO_STAT);
          m_vlepton_SFObjLoose_MU_SF_RECO_STAT_UP   	.push_back( SFObjLoose/SFreco*SFIDLoose_MU_SF_RECO_STAT[0]  			);
          m_vlepton_SFObjLoose_MU_SF_RECO_STAT_DOWN 	.push_back( SFObjLoose/SFreco*SFIDLoose_MU_SF_RECO_STAT[1]     			);
          if (m_debugMode) std::cout << "SFIDLoose:	" << SFIDLoose << "	 SFIDLoose_MU_SF_RECO_STAT_UP:		" <<  SFIDLoose_MU_SF_RECO_STAT[0] << "		SFIDLoose_MU_SF_RECO_STAT_DOWN:		" << SFIDLoose_MU_SF_RECO_STAT[1] << std::endl;
          // tight
          m_vlepton_SFObjTight_MU_SF_RECO_STAT_UP   	.push_back( SFObjTightLH/SFreco*SFIDLoose_MU_SF_RECO_STAT[0]  			);
          m_vlepton_SFObjTight_MU_SF_RECO_STAT_DOWN 	.push_back( SFObjTightLH/SFreco*SFIDLoose_MU_SF_RECO_STAT[1]     		);
          // syst 
          std::vector<float> SFIDLoose_MU_SF_RECO_SYST = {1.,1.};
          SFIDLoose_MU_SF_RECO_SYST = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_MuonEfficiencyScaleFactors_ID, lepton, "MUON_EFF_RECO_SYS", SFIDLoose_MU_SF_RECO_SYST);
          m_vlepton_SFObjLoose_MU_SF_RECO_SYST_UP    	.push_back( SFObjLoose/SFreco*SFIDLoose_MU_SF_RECO_SYST[0]                    	);
          m_vlepton_SFObjLoose_MU_SF_RECO_SYST_DOWN  	.push_back( SFObjLoose/SFreco*SFIDLoose_MU_SF_RECO_SYST[1]                    	);
          if (m_debugMode) std::cout << "SFIDLoose:     " << SFIDLoose << "      SFIDLoose_MU_SF_RECO_SYST_UP:               " <<  SFIDLoose_MU_SF_RECO_SYST[0] << "              SFIDLoose_MU_SF_RECO_SYST_DOWN:              " << SFIDLoose_MU_SF_RECO_SYST[1] << std::endl;
          // tight
          m_vlepton_SFObjTight_MU_SF_RECO_SYST_UP    	.push_back( SFObjTightLH/SFreco*SFIDLoose_MU_SF_RECO_SYST[0]                   	);
          m_vlepton_SFObjTight_MU_SF_RECO_SYST_DOWN  	.push_back( SFObjTightLH/SFreco*SFIDLoose_MU_SF_RECO_SYST[1]                   	);
          // muon Iso
          // NEW: PLV calibration is available, applying additional isolation to tight leptons
          // stat
          std::vector<float> SFisoLoose_MU_SF_Iso_STAT = {1.,1.};
          SFisoLoose_MU_SF_Iso_STAT = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_MuonEfficiencyScaleFactors_isolation, lepton, "MUON_EFF_ISO_STAT", SFisoLoose_MU_SF_Iso_STAT);
          m_vlepton_SFObjLoose_MU_SF_Iso_STAT_UP   	.push_back( SFObjLoose/SFisoLoose*SFisoLoose_MU_SF_Iso_STAT[0]  		);
          m_vlepton_SFObjLoose_MU_SF_Iso_STAT_DOWN 	.push_back( SFObjLoose/SFisoLoose*SFisoLoose_MU_SF_Iso_STAT[1]     		);
          if (m_debugMode) std::cout << "SFisoLoose:	" << SFisoLoose << "	 SFisoLoose_MU_SF_Iso_STAT_UP:		" <<  SFisoLoose_MU_SF_Iso_STAT[0] << "		SFisoLoose_MU_SF_Iso_STAT_DOWN:		" << SFisoLoose_MU_SF_Iso_STAT[1] << std::endl;
          // PLV stat
          std::vector<float> SFisoPLV_MU_SF_Iso_STAT = {1.,1.};
          SFisoPLV_MU_SF_Iso_STAT = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_MuonEfficiencyScaleFactors_PLV, lepton, "MUON_EFF_ISO_STAT", SFisoPLV_MU_SF_Iso_STAT);
          m_vlepton_SFObjTight_MU_SF_Iso_STAT_UP	.push_back( SFObjTightLH/SFisoPLV*SFisoPLV_MU_SF_Iso_STAT[0]			);
          m_vlepton_SFObjTight_MU_SF_Iso_STAT_DOWN	.push_back( SFObjTightLH/SFisoPLV*SFisoPLV_MU_SF_Iso_STAT[1]			);
          if (m_debugMode) std::cout << "SFisoPLV:	" << SFisoPLV 	<< "	 SFisoPLV_MU_SF_Iso_STAT_UP:		" <<  SFisoPLV_MU_SF_Iso_STAT[0] << "		SFisoPLV_MU_SF_Iso_STAT_DOWN:		" << SFisoPLV_MU_SF_Iso_STAT[1] << std::endl;
          // syst
          std::vector<float> SFisoLoose_MU_SF_Iso_SYST = {1.,1.};
          SFisoLoose_MU_SF_Iso_SYST = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_MuonEfficiencyScaleFactors_isolation, lepton, "MUON_EFF_ISO_SYS", SFisoLoose_MU_SF_Iso_SYST);
          m_vlepton_SFObjLoose_MU_SF_Iso_SYST_UP   	.push_back( SFObjLoose/SFisoLoose*SFisoLoose_MU_SF_Iso_SYST[0]  		);
          m_vlepton_SFObjLoose_MU_SF_Iso_SYST_DOWN 	.push_back( SFObjLoose/SFisoLoose*SFisoLoose_MU_SF_Iso_SYST[1]     		);
          if (m_debugMode) std::cout << "SFisoLoose:	" << SFisoLoose << "	 SFisoLoose_MU_SF_Iso_SYST_UP:		" <<  SFisoLoose_MU_SF_Iso_SYST[0] << "		SFisoLoose_MU_SF_Iso_SYST_DOWN:		" << SFisoLoose_MU_SF_Iso_SYST[1] << std::endl;
          // PLV syst
          std::vector<float> SFisoPLV_MU_SF_Iso_SYST = {1.,1.};
          SFisoPLV_MU_SF_Iso_SYST = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_MuonEfficiencyScaleFactors_PLV, lepton, "MUON_EFF_ISO_SYS", SFisoPLV_MU_SF_Iso_SYST);
          m_vlepton_SFObjTight_MU_SF_Iso_SYST_UP	.push_back( SFObjTightLH/SFisoPLV*SFisoPLV_MU_SF_Iso_SYST[0]			);
          m_vlepton_SFObjTight_MU_SF_Iso_SYST_DOWN	.push_back( SFObjTightLH/SFisoPLV*SFisoPLV_MU_SF_Iso_SYST[1]			);
          // muon TTVA
          // stat 
          std::vector<float> SFTTVA_MU_SF_TTVA_STAT = {1.,1.};
          SFTTVA_MU_SF_TTVA_STAT = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_MuonEfficiencyScaleFactors_TTVA, lepton, "MUON_EFF_TTVA_STAT", SFTTVA_MU_SF_TTVA_STAT);
          m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_UP   	.push_back( SFObjLoose/SFTTVA*SFTTVA_MU_SF_TTVA_STAT[0]  			);
          m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN 	.push_back( SFObjLoose/SFTTVA*SFTTVA_MU_SF_TTVA_STAT[1]     			);
          if (m_debugMode) std::cout << "SFisoLoose:	" << SFisoLoose << "	 SFTTVA_MU_SF_TTVA_STAT_UP:		" <<  SFTTVA_MU_SF_TTVA_STAT[0] << "		SFTTVA_MU_SF_TTVA_STAT_DOWN:		" << SFTTVA_MU_SF_TTVA_STAT[1] << std::endl;
          // tight
          m_vlepton_SFObjTight_MU_SF_TTVA_STAT_UP   	.push_back( SFObjTightLH/SFTTVA*SFTTVA_MU_SF_TTVA_STAT[0]  			);
          m_vlepton_SFObjTight_MU_SF_TTVA_STAT_DOWN 	.push_back( SFObjTightLH/SFTTVA*SFTTVA_MU_SF_TTVA_STAT[1]     			);
          // syst
          std::vector<float> SFTTVA_MU_SF_TTVA_SYST = {1.,1.};
          SFTTVA_MU_SF_TTVA_SYST = WWZanalysis::SystematicVariations::return_lepton_SF_variations(m_MuonEfficiencyScaleFactors_TTVA, lepton, "MUON_EFF_TTVA_SYS", SFTTVA_MU_SF_TTVA_SYST);
          m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_UP   	.push_back( SFObjLoose/SFTTVA*SFTTVA_MU_SF_TTVA_SYST[0]  			);
          m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN 	.push_back( SFObjLoose/SFTTVA*SFTTVA_MU_SF_TTVA_SYST[1]     			);
          if (m_debugMode) std::cout << "SFisoLoose:	" << SFisoLoose << "	 SFTTVA_MU_SF_TTVA_SYST_UP:		" <<  SFTTVA_MU_SF_TTVA_SYST[0] << "		SFTTVA_MU_SF_TTVA_SYST_DOWN:		" << SFTTVA_MU_SF_TTVA_SYST[1] << std::endl;
          m_vlepton_SFObjTight_MU_SF_TTVA_SYST_UP   	.push_back( SFObjTightLH/SFTTVA*SFTTVA_MU_SF_TTVA_SYST[0]  			);
          m_vlepton_SFObjTight_MU_SF_TTVA_SYST_DOWN 	.push_back( SFObjTightLH/SFTTVA*SFTTVA_MU_SF_TTVA_SYST[1]     			);
        }

        // truth info
        const xAOD::TruthParticle *matched_truth_lepton = nullptr;
        if( !isData && lepton->isAvailable<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink") ) {
          ElementLink<xAOD::TruthParticleContainer> link = lepton->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
          if(link.isValid()) {
            matched_truth_lepton = *link;
            if (m_debugMode) std::cout << "truthType available?:	" << matched_truth_lepton->isAvailable<int>("truthType") << std::endl;
            if (m_debugMode) std::cout << "truthOrigin available?:	" << matched_truth_lepton->isAvailable<int>("truthOrigin") << std::endl;
            int type   = matched_truth_lepton->auxdata<int>("truthType");
            int origin = matched_truth_lepton->auxdata<int>("truthOrigin");
            m_vlepton_truthType           .push_back( type                                                          	);
            m_vlepton_truthOrigin         .push_back( origin                                                        	);
          }
          else {
            delete matched_truth_lepton;
            m_vlepton_truthType           .push_back( -1.                                                           	);
            m_vlepton_truthOrigin         .push_back( -1.                                                           	);
          }
        }
        // total lepton charge
        m_tot_charge			+= lepton->charge();
      }
    } //sorted leptons loop
    if (m_debugMode) std::cout << "m_vlepton_pt.size():      " << m_vlepton_pt.size() << std::endl;
    // trigger SFs
    float trigSF 	= isData ? 1 : TriggerScaleFactors::get_muon_trigger_weight(m_MuonTriggerScaleFactors, "HLT_mu26_ivarmedium", sorted_muons);
    float trigSF_1 	= isData ? 1 : TriggerScaleFactors::get_muon_trigger_weight(m_MuonTriggerScaleFactors, "HLT_mu50", sorted_muons);
    if (m_debugMode) std::cout << "trigger SFs:		" << trigSF << "	" << trigSF_1 << std::endl;

    // sorted jets
    // b-tagging SF
    float btagSF(1.0);
    std::vector<float> btagSF_Sys_B_UP,		btagSF_Sys_C_UP, 	btagSF_Sys_Light_UP;
    std::vector<float> btagSF_Sys_B_DOWN,	btagSF_Sys_C_DOWN, 	btagSF_Sys_Light_DOWN;
    // JVT SFs
    float jvtSF(1.0), jvtSF_UP(1.0), jvtSF_DOWN(1.0);
    // need jet container for applyAllEfficiencyScaleFactor() method
    if (!isData && !m_isAODfile)	m_JetVertexTaggerToolSF->applyAllEfficiencyScaleFactor(afterOLR_jets.asDataVector(), jvtSF);
    m_JVT_EventWeight = jvtSF;
    m_JVT_EventWeight_UP 	= 1.0;
    m_JVT_EventWeight_DOWN	= 1.0;
    if (m_doSystematics && !isData && !m_isAODfile) {
      CP::SystematicSet tmpSet_jvtSF_UP; tmpSet_jvtSF_UP.insert(CP::SystematicVariation("JET_JvtEfficiency",1));
      m_JetVertexTaggerToolSF->applySystematicVariation(tmpSet_jvtSF_UP);
      m_JetVertexTaggerToolSF->applyAllEfficiencyScaleFactor(afterOLR_jets.asDataVector(), jvtSF_UP); 
      CP::SystematicSet tmpSet_jvtSF_DOWN; tmpSet_jvtSF_DOWN.insert(CP::SystematicVariation("JET_JvtEfficiency",-1));
      m_JetVertexTaggerToolSF->applySystematicVariation(tmpSet_jvtSF_DOWN);
      m_JetVertexTaggerToolSF->applyAllEfficiencyScaleFactor(afterOLR_jets.asDataVector(), jvtSF_DOWN); 
      m_JVT_EventWeight_UP = jvtSF_UP;
      m_JVT_EventWeight_DOWN = jvtSF_DOWN;
      if (m_debugMode) std::cout << "m_JVT_EventWeight:	" << m_JVT_EventWeight << "	m_JVT_EventWeight_UP:	" << m_JVT_EventWeight_UP << "	m_JVT_EventWeight_DOWN:		" << m_JVT_EventWeight_DOWN << std::endl;
      // Reset tool
      m_JetVertexTaggerToolSF->applySystematicVariation(CP::SystematicSet());
    }
    //std::cout << std::endl;
    //std::cout << "NEW EVENT" << std::endl;
    for(unsigned int j=0; j<sorted_jets.size(); j++) {
      xAOD::Jet		*jet = sorted_jets[j];
      // cut here on JVT? JetJvtEfficiency tool assumes that the jet container passed to the tool contains signal jets *after Overlap Removal* (if your analysis applies OR) but *before applying JVT* (CRUCIAL)
      // WAITING FOR A FEEDBACK
      // see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVTCalibration#Overlap_Removal and https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVTCalibration#JetJvtEfficiency_Tool_Usage
      float jet_jvt 	= jet->auxdata<float>("JVTName");
      //if( jet->pt()<60e3 && jet_jvt<m_jet_jvt_cut )   	continue;

      m_vjet_E    			.push_back( jet->e()		);
      m_vjet_pt   			.push_back( jet->pt()		);
      m_vjet_eta  			.push_back( jet->eta()		);
      m_vjet_phi 			.push_back( jet->phi()		);
      m_vjet_jvt  			.push_back( jet_jvt		);
      int jet_coneLabel	= isData ? -1 : jet->getAttribute<int>("HadronConeExclTruthLabelID");;//jet->getAttribute<int>("ConeTruthLabelID");
      m_vjet_coneLabel 			.push_back( jet_coneLabel	);
      // b-tagging information
      double mv2c10(-999.), mv2c10mu(-999.); 
      try { mv2c10   	= jet->btagging()->auxdata<double>("MV2c10_discriminant"); }
      catch(...) { Warning("execute()", "Failed to retrieve MV2c10."); }
      //try { mv2c10mu 	= jet->btagging()->auxdata<double>("MV2c10mu_discriminant"); }
      //catch(...) { Warning("execute()", "Failed to retrieve MV2c10mu."); }
      if (m_debugMode) std::cout << "MV2c10:	" << mv2c10 << std::endl;
      m_vjet_mv2c10			.push_back( mv2c10  		);
      m_vjet_mv2c10mu			.push_back( mv2c10mu  		);
      // Find WPs here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease21
      if(mv2c10>0.939)				m_nJets_mv2c10_60++;
      // BTaggingSelectionTool is the recommended way
      if(m_BTaggingSelectionTool->accept(*jet))	m_nJets_mv2c10_70++;
      //if(mv2c10>0.831)			m_nJets_mv2c10_70++;
      if(mv2c10>0.645)				m_nJets_mv2c10_77++;
      if(mv2c10>0.11)				m_nJets_mv2c10_85++;
      // retrieve decoration performed by BTaggingScaleFactors::decorate_with_bTagSFs 
      float btagSF_j 	= jet->auxdata<float>("bTagSF");
      btagSF *= btagSF_j;
      if (m_doSystematics) {
        std::vector<float> btagSF_Sys_B_UP_j, 	btagSF_Sys_C_UP_j, 	btagSF_Sys_Light_UP_j;
        std::vector<float> btagSF_Sys_B_DOWN_j, btagSF_Sys_C_DOWN_j, 	btagSF_Sys_Light_DOWN_j;
        btagSF_Sys_B_UP_j 	= BTaggingScaleFactors::retrieve_Eigen( jet,	m_BTaggingEfficiencyTool,	m_BTaggingSelectionTool,	ftag_systematics_list,	"B",		1	);
        btagSF_Sys_B_DOWN_j 	= BTaggingScaleFactors::retrieve_Eigen( jet,	m_BTaggingEfficiencyTool,	m_BTaggingSelectionTool,	ftag_systematics_list,	"B",		-1	);
        btagSF_Sys_C_UP_j 	= BTaggingScaleFactors::retrieve_Eigen( jet,	m_BTaggingEfficiencyTool,	m_BTaggingSelectionTool,	ftag_systematics_list,	"C",		1	);
        btagSF_Sys_C_DOWN_j 	= BTaggingScaleFactors::retrieve_Eigen( jet,	m_BTaggingEfficiencyTool,	m_BTaggingSelectionTool,	ftag_systematics_list,	"C",		-1	);
        btagSF_Sys_Light_UP_j 	= BTaggingScaleFactors::retrieve_Eigen( jet,	m_BTaggingEfficiencyTool,	m_BTaggingSelectionTool,	ftag_systematics_list,	"Light",	1	);
        btagSF_Sys_Light_DOWN_j	= BTaggingScaleFactors::retrieve_Eigen( jet,	m_BTaggingEfficiencyTool,	m_BTaggingSelectionTool,	ftag_systematics_list,	"Light",	-1	);
        if (m_debugMode) std::cout << " btagSF_Sys_B_UP_j size:\t" << btagSF_Sys_B_UP_j.size() << "\tbtagSF_Sys_B_DOWN_j size:\t" << btagSF_Sys_B_DOWN_j.size() << std::endl;
        if (m_debugMode) std::cout << " btagSF_Sys_C_UP_j size:\t" << btagSF_Sys_C_UP_j.size() << "\tbtagSF_Sys_C_DOWN_j size:\t" << btagSF_Sys_C_DOWN_j.size() << std::endl;
        if (m_debugMode) std::cout << " btagSF_Sys_Light_UP_j size:\t" << btagSF_Sys_Light_UP_j.size() << "\tbtagSF_Sys_Light_DOWN_j size:\t" << btagSF_Sys_Light_DOWN_j.size() << std::endl;
        if (j==0) {
          for (unsigned int s=0; s<btagSF_Sys_B_UP_j.size(); s++) 	{ btagSF_Sys_B_UP.push_back(1.); 	btagSF_Sys_B_DOWN.push_back(1.);	}
          for (unsigned int s=0; s<btagSF_Sys_C_UP_j.size(); s++) 	{ btagSF_Sys_C_UP.push_back(1.); 	btagSF_Sys_C_DOWN.push_back(1.);	}
          for (unsigned int s=0; s<btagSF_Sys_Light_UP_j.size(); s++)	{ btagSF_Sys_Light_UP.push_back(1.); 	btagSF_Sys_Light_DOWN.push_back(1.); 	}
        }
        // element-wise multiplication of vectors
        btagSF_Sys_B_UP 	= UtilitiesFunctions::vectorMultiplication(btagSF_Sys_B_UP, btagSF_Sys_B_UP_j); 	btagSF_Sys_B_DOWN 	= UtilitiesFunctions::vectorMultiplication(btagSF_Sys_B_DOWN, btagSF_Sys_B_DOWN_j);
        btagSF_Sys_C_UP 	= UtilitiesFunctions::vectorMultiplication(btagSF_Sys_C_UP, btagSF_Sys_C_UP_j); 	btagSF_Sys_C_DOWN 	= UtilitiesFunctions::vectorMultiplication(btagSF_Sys_C_DOWN, btagSF_Sys_C_DOWN_j);
        btagSF_Sys_Light_UP 	= UtilitiesFunctions::vectorMultiplication(btagSF_Sys_Light_UP, btagSF_Sys_Light_UP_j);	btagSF_Sys_Light_DOWN  	= UtilitiesFunctions::vectorMultiplication(btagSF_Sys_Light_DOWN, btagSF_Sys_Light_DOWN_j);
        //std::cout << "NEW JET" << std::endl;
        //std::cout << " btagSF_Sys_B_UP:\t" << btagSF_Sys_B_UP[0] << "\tbtagSF_Sys_B_UP_j:\t" << btagSF_Sys_B_UP_j[0] << std::endl;
      }
    }
    m_MV2c10_FixedCutBEff_70_EventWeight = btagSF;
    // store FTAG systematic variations
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_up = btagSF_Sys_B_UP; 		m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_down = btagSF_Sys_B_DOWN;
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_up = btagSF_Sys_C_UP; 		m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_down = btagSF_Sys_C_DOWN;
    m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up = btagSF_Sys_Light_UP; 	m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down = btagSF_Sys_Light_DOWN;
    // normalize to nominal SF
    for (unsigned int s=0; s<btagSF_Sys_B_UP.size(); s++) 	{ m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_up.at(s) /= btagSF; m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_down.at(s) /= btagSF; }
    for (unsigned int s=0; s<btagSF_Sys_C_UP.size(); s++) 	{ m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_up.at(s) /= btagSF; m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_down.at(s) /= btagSF; }
    for (unsigned int s=0; s<btagSF_Sys_Light_UP.size(); s++)	{ m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up.at(s) /= btagSF; m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down.at(s) /= btagSF; }
    if (m_debugMode) std::cout << " JVT SF:	" << jvtSF << std::endl;
    if (m_debugMode) std::cout << " b-tag SF:	" << btagSF << std::endl;
    if (m_debugMode) std::cout << "Done with jets." << std::endl;
   
    // event kinematics info 
    // all events
    m_HT_lep 			= UtilitiesFunctions::scalar_pT_sum(sorted_leptons);
    m_HT_had			= UtilitiesFunctions::scalar_pT_sum(sorted_jets);
    // 2L
    if (m_vlepton_pt.size()==2) {
      m_best_Z_Mll 		= UtilitiesFunctions::bestZmass(sorted_leptons, m_vlepton_ID); 
      m_Mll01			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[0],sorted_leptons[1]);
    }
    // 3L
    if (m_vlepton_pt.size()==3) {
      m_Mlll012 		= UtilitiesFunctions::allLeptonMass(sorted_leptons);
      m_best_Z_Mll 		= UtilitiesFunctions::bestZmass(sorted_leptons, m_vlepton_ID); 
      m_Mll01 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[0],sorted_leptons[1]);
      m_Mll02 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[0],sorted_leptons[2]);
      m_Mll12 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[1],sorted_leptons[2]);
    }
    // 4L
    if (m_vlepton_pt.size()==4) {
      m_Mllll0123 		= UtilitiesFunctions::allLeptonMass(sorted_leptons);
      m_best_Z_Mll		= UtilitiesFunctions::bestZmass(sorted_leptons, m_vlepton_ID);
      m_best_Z_other_Mll	= UtilitiesFunctions::otherBestZmass(sorted_leptons, m_vlepton_ID);
      m_Mll01 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[0],sorted_leptons[1]);
      m_Mll02 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[0],sorted_leptons[2]);
      m_Mll03 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[0],sorted_leptons[3]);
      m_Mll12 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[1],sorted_leptons[2]);
      m_Mll13 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[1],sorted_leptons[3]);
      m_Mll23 			= UtilitiesFunctions::twoObjectsMass(sorted_leptons[2],sorted_leptons[3]);
      m_Mlll012			= UtilitiesFunctions::threeLeptonMass(sorted_leptons[0],sorted_leptons[1],sorted_leptons[2]);
      m_Mlll013            	= UtilitiesFunctions::threeLeptonMass(sorted_leptons[0],sorted_leptons[1],sorted_leptons[3]);
      m_Mlll023            	= UtilitiesFunctions::threeLeptonMass(sorted_leptons[0],sorted_leptons[2],sorted_leptons[3]);
      m_Mlll123            	= UtilitiesFunctions::threeLeptonMass(sorted_leptons[1],sorted_leptons[2],sorted_leptons[3]);
    } //if (m_vlepton_pt.size()==4)
    // di-jet invariant masses
    if (m_vjet_pt.size()>1) {
      m_Mjj01			= UtilitiesFunctions::twoObjectsMass(sorted_jets[0],sorted_jets[1]);
      if (m_vjet_pt.size()>2) {
        m_Mjj02			= UtilitiesFunctions::twoObjectsMass(sorted_jets[0],sorted_jets[2]);
        m_Mjj12			= UtilitiesFunctions::twoObjectsMass(sorted_jets[1],sorted_jets[2]);
      }
      if (m_vjet_pt.size()>3) {
        m_Mjj03			= UtilitiesFunctions::twoObjectsMass(sorted_jets[0],sorted_jets[3]); 
        m_Mjj13			= UtilitiesFunctions::twoObjectsMass(sorted_jets[1],sorted_jets[3]); 
        m_Mjj23			= UtilitiesFunctions::twoObjectsMass(sorted_jets[2],sorted_jets[3]); 
      }
      if (m_vjet_pt.size()>4) {
        m_Mjj04			= UtilitiesFunctions::twoObjectsMass(sorted_jets[0],sorted_jets[4]);
        m_Mjj14			= UtilitiesFunctions::twoObjectsMass(sorted_jets[1],sorted_jets[4]);
        m_Mjj24			= UtilitiesFunctions::twoObjectsMass(sorted_jets[2],sorted_jets[4]);
        m_Mjj34			= UtilitiesFunctions::twoObjectsMass(sorted_jets[3],sorted_jets[4]);
      } 
      if (m_vjet_pt.size()>5) {
        m_Mjj05               	= UtilitiesFunctions::twoObjectsMass(sorted_jets[0],sorted_jets[5]);;
        m_Mjj15               	= UtilitiesFunctions::twoObjectsMass(sorted_jets[1],sorted_jets[5]);;
        m_Mjj25               	= UtilitiesFunctions::twoObjectsMass(sorted_jets[2],sorted_jets[5]);;
        m_Mjj35               	= UtilitiesFunctions::twoObjectsMass(sorted_jets[3],sorted_jets[5]);;
        m_Mjj45               	= UtilitiesFunctions::twoObjectsMass(sorted_jets[4],sorted_jets[5]);;
      }
    }
    // electrons
    for(unsigned int e=0; e<sorted_electrons.size(); e++) {
      xAOD::Electron 	*ele = sorted_electrons[e];
      m_velectron_E                 	.push_back( ele->e()                     		            );
      m_velectron_pt                	.push_back( ele->pt()                    		            );
      m_velectron_eta               	.push_back( ele->eta()                   		            );
      m_velectron_phi               	.push_back( ele->phi()                   		            );
      m_velectron_q                 	.push_back( ele->charge()                		            );
      const xAOD::TrackParticle *eltrk = ele->trackParticle();
      m_velectron_d0                	.push_back( eltrk->d0()                                                                         );
      m_velectron_z0                	.push_back( fabs(eltrk->z0() + eltrk->vz() - primary_vertex->z())                               );
      m_velectron_z0sinT            	.push_back( fabs(eltrk->z0() + eltrk->vz() - primary_vertex->z()) * sin(eltrk->theta())         );
      m_velectron_sigd0           	.push_back( xAOD::TrackingHelpers::d0significance(eltrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY())			);
      //m_velectron_sigz0           	.push_back( xAOD::TrackingHelpers::z0significance(eltrk, primary_vertex)			);
      if( !isData && xAOD::TruthHelpers::getTruthParticle(*ele) ) {
        int type   = xAOD::TruthHelpers::getParticleTruthType(*ele);
        int origin = xAOD::TruthHelpers::getParticleTruthOrigin(*ele);
        //if (m_debugMode) std::cout << "Electron truthType:		" << type << std::endl;
        //if (m_debugMode) std::cout << "Electron truthOrigin:		" << origin << std::endl;
        m_velectron_truthType         	.push_back( type                                                        );
        m_velectron_truthOrigin       	.push_back( origin                                                      );
      }
      else {
        m_velectron_truthType     	.push_back( -1.                                                         );
        m_velectron_truthOrigin   	.push_back( -1.                                                         );
      } 
    }
    // muons
    for(unsigned int m=0; m<sorted_muons.size(); m++) {
      xAOD::Muon	 *muon = sorted_muons[m];
      m_vmuon_E				.push_back( muon->e()							);
      m_vmuon_pt			.push_back( muon->pt()							);
      m_vmuon_eta			.push_back( muon->eta()							);
      m_vmuon_phi			.push_back( muon->phi()							);
      m_vmuon_q				.push_back( muon->charge()						);
      const xAOD::TrackParticle	*mutrk = muon->primaryTrackParticle();
      m_vmuon_d0			.push_back( mutrk->d0()										);
      m_vmuon_z0			.push_back( fabs(mutrk->z0() + mutrk->vz() - primary_vertex->z())				);
      m_vmuon_z0sinT			.push_back( fabs(mutrk->z0() + mutrk->vz() - primary_vertex->z()) * sin(mutrk->theta())		);
      m_vmuon_sigd0                 	.push_back( xAOD::TrackingHelpers::d0significance(mutrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY())                   );
      //m_vmuon_sigz0                 	.push_back( xAOD::TrackingHelpers::z0significance(mutrk, primary_vertex)                        );
      const xAOD::TruthParticle *matched_truth_muon = nullptr;
      if( !isData && muon->isAvailable<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink") ) {
        ElementLink<xAOD::TruthParticleContainer> link = muon->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
        if(link.isValid()) {	 	
          matched_truth_muon = *link;
          int type   = matched_truth_muon->auxdata<int>("truthType");
          int origin = matched_truth_muon->auxdata<int>("truthOrigin");
          //if (m_debugMode) std::cout << "Muon truthType:		" << type << std::endl;
          //if (m_debugMode) std::cout << "Muon truthOrigin:		" << origin << std::endl;
          m_vmuon_truthType           	.push_back( type 							);
          m_vmuon_truthOrigin         	.push_back( origin							);
        }
        else {
          delete matched_truth_muon;
          m_vmuon_truthType             .push_back( -1.                                                      	);
          m_vmuon_truthOrigin           .push_back( -1.                                                      	);
        }
      }
    }
  


    //
    // MET 
    //

    // Uncalibrated MET from x/DAOD
    const xAOD::MissingETContainer 	*metcont = nullptr;
    if(!event->retrieve(metcont, "MET_Reference_AntiKt4EMTopo").isSuccess()){
      Error("execute()", "Failed to retrieve the MET_RefFinal container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    const xAOD::MissingET		*mmet = nullptr;
    mmet = (*metcont)["FinalTrk"];// Trk or Clus
    m_MET 				= mmet->met();
    m_MET_phi				= mmet->phi();
    m_MET_x 				= mmet->mpx();
    m_MET_y 				= mmet->mpy();

    // Build MET for considered obects (i.e. muons, electrons and jets)
    if (m_debugMode) std::cout << "Now rebuilding MET..." << std::endl;
    // met core contains the soft terms we need to add to our met calculation
    if (m_debugMode) std::cout << "Retrieving MET core..." << std::endl;
    const xAOD::MissingETContainer *xaod_met_core = nullptr;
    std::string coreMetKey = "MET_Core_AntiKt4EMTopo";
    ANA_CHECK( event->retrieve( xaod_met_core , coreMetKey) );
    // This maps the met terms to objects
    if (m_debugMode) std::cout << "Retrieving MET map..." << std::endl;
    const xAOD::MissingETAssociationMap *xaod_met_map = nullptr;
    std::string metAssocKey = "METAssoc_AntiKt4EMTopo";
    ANA_CHECK( event->retrieve( xaod_met_map , metAssocKey ) );
    xAOD::MissingETContainer*    new_met_container    = new xAOD::MissingETContainer();
    xAOD::MissingETAuxContainer* new_met_Auxcontainer = new xAOD::MissingETAuxContainer();
    new_met_container->setStore(new_met_Auxcontainer);
    if (m_debugMode) std::cout << "METMaker::recalculateEventMET()" << std::endl;
    new_met_container = METMaker::recalculateEventMET(xaod_met_core, xaod_met_map, metMaker, raw_RecoElectrons, raw_allRecoMuons, jets, m_AsgElectronLikelihoodTool_looseID, m_MuonSelectionTool_looseID, metSystTool);
    m_MET_RefFinal_et 			= (*new_met_container)["FinalTrk"]->met();
    m_MET_RefFinal_phi 			= (*new_met_container)["FinalTrk"]->phi();
    m_MET_RefFinal_x 			= (*new_met_container)["FinalTrk"]->mpx();
    m_MET_RefFinal_y 			= (*new_met_container)["FinalTrk"]->mpy();

    if (m_debugMode) std::cout << "Rebuilt FinalTrk MET et:		" << m_MET_RefFinal_et 	<< "	uncalibrated MET et:	" << m_MET << std::endl;
    if (m_debugMode) std::cout << "Rebuilt FinalTrk MET phi:		" << m_MET_RefFinal_phi << "	uncalibrated MET phi:	" << m_MET_phi << std::endl;
    if (m_debugMode) std::cout << "Rebuilt FinalTrk MET x:		" << m_MET_RefFinal_x 	<< "	uncalibrated MET x:	" << m_MET_x << std::endl;
    if (m_debugMode) std::cout << "Rebuilt FinalTrk MET y:		" << m_MET_RefFinal_y 	<< "	uncalibrated MET y:	" << m_MET_y << std::endl;

    //
    // MET 
    //
    if (m_debugMode) std::cout << "Done with MET." << std::endl;
 
    //
    // Final filling of tree branches with final objects
    //

    //
    // Systematics ntuples: additional reduction of output size
    //

    if ( m_debugMode && ( m_doSystematics || !m_isnominalTTree ) ) {
       std::cout << "m_passEventCleaning:	" << m_passEventCleaning << std::endl;
       std::cout << "m_passTrigger:		" << m_passTrigger << std::endl;
       std::cout << "m_isTrigMatched:		" << m_isTrigMatched << std::endl;
       std::cout << "final_electrons.size():	" << final_electrons.size() << std::endl;
       std::cout << "final_muons.size():	" << final_muons.size() << std::endl;
       std::cout << "m_tot_charge:		" << m_tot_charge << std::endl;
       std::cout << "m_best_Z_Mll:		" << m_best_Z_Mll << std::endl;
    }

    if( m_doSystematics || !m_isnominalTTree ) {
      if( !m_passEventCleaning || !m_passTrigger || !m_isTrigMatched )						return EL::StatusCode::SUCCESS; // go to next event
      if( final_electrons.size()+final_muons.size() == 3 && (abs(m_tot_charge) != 1 || m_vjet_pt.size()==0 ) )	return EL::StatusCode::SUCCESS; // go to next event
      if( final_electrons.size()+final_muons.size() == 4 && m_tot_charge != 0 )					return EL::StatusCode::SUCCESS; // go to next event
      if( m_best_Z_Mll == 0 )											return EL::StatusCode::SUCCESS; // go to next event
      if( m_nJets_mv2c10_70 != 0 )										return EL::StatusCode::SUCCESS; // go to next event
      // Tight leptons
      bool isTight3L(false), isTight4L(false);
      isTight3L = (final_electrons.size()+final_muons.size() == 3) ?  ( ( (m_Mll12==m_best_Z_Mll && m_vlepton_isTightLH[0] && m_vlepton_ambiguityType[0]==0 && m_vlepton_isolationFixedCutLoose[0] && ((m_lepton_PromptLeptonVeto[0]<-0.7 && abs(m_vlepton_ID[0])==11) || (m_lepton_PromptLeptonVeto[0]<-0.5 && abs(m_vlepton_ID[0])==13))) || (m_Mll02==m_best_Z_Mll && m_vlepton_isTightLH[1] && m_vlepton_ambiguityType[1]==0 && m_vlepton_isolationFixedCutLoose[1] && ((m_lepton_PromptLeptonVeto[1]<-0.7 && abs(m_vlepton_ID[1])==11) || (m_lepton_PromptLeptonVeto[1]<-0.5 && abs(m_vlepton_ID[1])==13))) || (m_Mll01==m_best_Z_Mll && m_vlepton_isTightLH[2] && m_vlepton_ambiguityType[2]==0 && m_vlepton_isolationFixedCutLoose[2] && ((m_lepton_PromptLeptonVeto[2]<-0.7 && abs(m_vlepton_ID[2])==11) || (m_lepton_PromptLeptonVeto[2]<-0.5 && abs(m_vlepton_ID[2])==13))) ) ) : true;
      isTight4L = (final_electrons.size()+final_muons.size() == 4) ?  ( (m_vlepton_isTightLH[2] && m_vlepton_ambiguityType[2]==0 && m_vlepton_isolationFixedCutLoose[2] && ((m_lepton_PromptLeptonVeto[2]<-0.7 && abs(m_vlepton_ID[2])==11) || (m_lepton_PromptLeptonVeto[2]<-0.5 && abs(m_vlepton_ID[2])==13))) && (m_vlepton_isTightLH[3] && m_vlepton_ambiguityType[3]==0 && m_vlepton_isolationFixedCutLoose[3] && ((m_lepton_PromptLeptonVeto[3]<-0.7 && abs(m_vlepton_ID[3])==11) || (m_lepton_PromptLeptonVeto[3]<-0.5 && abs(m_vlepton_ID[3])==13))) ) : true;
      if( !isTight3L || !isTight4L )										return EL::StatusCode::SUCCESS; // go to next event
    }

    //
    // Systematics ntuples: additional reduction of output size
    //



    //
    // Truth-level objects
    //

    // Higgs truth decay mode
    const xAOD::TruthParticleContainer  *allTruthParticles = 0;
    if (!isData) {
      try { 
        if (m_debugMode) std::cout << "Retrieving TruthParticles container..." << std:: endl;
        event->retrieve(allTruthParticles		,"TruthParticles");
        if (m_debugMode) std::cout << "Retrieving Higgs-boson decay mode..." << std:: endl;
        m_higgsMode = WWZanalysis::TruthSelector::GetHiggsDecayMode(allTruthParticles);
      }
      catch(...)        { Warning("execute()","Issues with Higgs-boson decay mode!"); }  
    }
    // Higgs truth decay mode
    // Z-boson truth decay mode
    if (!isData) {
      try {
        if (m_debugMode) std::cout << "Retrieving Z-boson decay mode..." << std:: endl;
        if (m_debugMode) std::cout << " Number of truth Z bosons:\t" << (WWZanalysis::TruthSelector::GetZDecayMode(allTruthParticles)).size() << std::endl;
        m_ZdecayMode = WWZanalysis::TruthSelector::GetZDecayMode(allTruthParticles);
      }
      catch(...)        { Warning("execute()","Issues with Z-boson decay mode!"); }  
    }
    // Z-boson truth decay mode

    // STDM derivations have TruthMuons and TruthElectrons containers! 
    // deep copy
    auto truth_prompt_leptons         	= std::make_unique<xAOD::TruthParticleContainer>();
    auto truth_prompt_leptonsAux      	= std::make_unique<xAOD::AuxContainerBase>();
    truth_prompt_leptons->setStore (truth_prompt_leptonsAux.get()); //< Connect the two
    // N.B. truth electron and muon containers (added to STDM DAODs, absent in AODs)
    const xAOD::TruthParticleContainer  *allTruthMuons = 0;
    const xAOD::TruthParticleContainer  *allTruthElectrons = 0;
//    const xAOD::TruthParticleContainer  *allTruthTaus = 0;
    bool truth_el(true), truth_mu(true);//, truth_tau(true);
    if (!isData && m_doTruth) {
      if (m_debugMode) std::cout << "Before truth containers..." << std::endl;
      try 	{ event->retrieve(allTruthMuons         	,"STDMTruthMuons") ; }
      //try 	{ event->retrieve(allTruthMuons         	,"TruthMuons") ; }
      catch(...) 	{ if (m_debugMode) std::cout << "TruthMuons container is not available!" << std::endl;; }//Warning("execute()","TruthMuons container is not available!"); 
      try 	{ event->retrieve(allTruthElectrons         	,"STDMTruthElectrons") ; }
      //try 	{ event->retrieve(allTruthElectrons         	,"TruthElectrons") ; }
      catch(...) 	{ Warning("execute()","TruthElectrons container is not available!"); }
      //if (!isData && m_doTruth && !event->retrieve(allTruthMuons		,"TruthMuons")) 	{ Warning("execute()","TruthMuons container is not available!");	truth_mu=false; }
      //if (!isData && m_doTruth && !event->retrieve(allTruthElectrons	,"TruthElectrons")) 	{ Warning("execute()","TruthElectrons container is not available!");	truth_el=false; }
//      if (!isData && m_doTruth && !event->retrieve(allTruthTaus		,"STDMTruthTaus")) 	{ Warning("execute()","STDMTruthTaus container is not available!");	truth_tau=false; }
      if (allTruthMuons->size()==0 && allTruthElectrons->size()==0) 	std::cout << "ZERO TRUTH PROMPT LEPTONS in truth containers!!! Event n.	" << m_eventCounter << std::endl;
      uint prompt_origin[] = {1,2,4,10,12,13,14,16,43};
      if (m_debugMode) std::cout << "After truth containers..." << std::endl;
      if (truth_el && truth_mu) {// && truth_tau) 
        for (auto tr_muon: *allTruthMuons) {
          if (m_debugMode) std::cout << "Truth muon..." << std::endl;
          bool isPrompt(false);
          uint mu_orig = tr_muon	->auxdata<uint>("classifierParticleOrigin");
          if (m_debugMode) std::cout << "Truth muon origin:	" << mu_orig << std::endl;
          for (unsigned int i=0; i<sizeof(prompt_origin)/sizeof(prompt_origin[0]); i++) 	{ if (mu_orig==prompt_origin[i]) isPrompt=true; }
          if (!isPrompt)		continue;
          m_nTruthPromptLep	++;
          uint mu_type = tr_muon	->auxdata<uint>("classifierParticleType");
          if (m_debugMode) std::cout << std::endl; 
          if (m_debugMode) std::cout << "muon origin:	" << mu_orig << "	muon type:	" << mu_type << std::endl;
        }
        if (m_debugMode) std::cout << "Done with truth muons." << std::endl;
        for (auto tr_ele: *allTruthElectrons) {
          bool isPrompt(false);
          uint el_orig = tr_ele	->auxdata<uint>("classifierParticleOrigin");
          if (m_debugMode) std::cout << "Truth electron origin:	" << el_orig << std::endl;
          for (unsigned int i=0; i<sizeof(prompt_origin)/sizeof(prompt_origin[0]); i++) 	{ if (el_orig==prompt_origin[i]) isPrompt=true; }
          if (!isPrompt)		continue;
          m_nTruthPromptLep	++;
          uint el_type = tr_ele	->auxdata<uint>("classifierParticleType");
          if (m_debugMode) std::cout << std::endl; 
          if (m_debugMode) std::cout << "ele origin:	" << el_orig << "	ele type:	" << el_type << std::endl;
        }
        if (m_debugMode) std::cout << "Done with truth electrons." << std::endl;
//        for (auto tr_tau: *allTruthTaus) {
//          if (m_debugMode) std::cout << "truth tau..." << std::endl;
//          if (!tr_tau->child())	continue;
//          if (m_debugMode) std::cout << std::endl; 
//          if (m_debugMode) std::cout << "tau child pdg id:		" << tr_tau->child()->pdgId() << std::endl;
//        }
        if (m_nTruthPromptLep==0) 	std::cout << "I GOT ZERO TRUTH PROMPT LEPTONS!!! Event n.	" << m_eventCounter << std::endl;
      }
    }
    // STDM derivations have TruthMuons and TruthElectrons containers! 
    


/*
    // in AODs no truthMuons and truthElectrons containers...
    // not sure it works on AODs btw, check in case...

    // all truth particles
    const xAOD::TruthParticleContainer 	*allTruthParticles = 0;
    // n. of leptonic and hadronic W/Z bosons
    unsigned int W_lep(0.), W_had(0.), Z_lep(0.), Z_inv(0.), Z_had(0.);
    if (!isData && m_doTruth) {
      if (!event->retrieve(allTruthParticles	,"TruthParticles").isSuccess()) { Error("execute()", "Failed to retrieve the truth particle container. Exiting."); 	return EL::StatusCode::FAILURE; }
      try {
        for (auto particle: *allTruthParticles) {
          int id_abs = abs(particle->pdgId());
          // W and Z bosons
          //if ( id_abs!=23 && id_abs!=24 ) 	continue;
          if (m_debugMode) std::cout << "new particle" << std::endl;
          if (m_debugMode) std::cout << "pdg id:		" << particle->pdgId() << std::endl;
          if (m_debugMode) std::cout << "status:		" << particle->status() << std::endl;
          if (m_debugMode) std::cout << "barcode:	" << particle->barcode() << std::endl;
//          const xAOD::TruthParticle 	*mother = particle->parent();
//          do {
//            if (!mother->parent()) { Warning("execute()","Problems retrieving the truth mother..."); break; }
//            mother   = mother->parent();
//          } while( mother->pdgId()==particle->pdgId() );

          //if (particle->barcode() > 99999) continue;
          //if (m_debugMode) std::cout << "Push back truth particles" << std::endl;
          //xAOD::TruthParticle *truth_p = new xAOD::TruthParticle;
          //truth_prompt_leptons->push_back(truth_p);
          //truth_p = *particle;
          if (particle->status() != 3 && particle->status() != 22) continue;
          try {  
            const xAOD::TruthParticle   *child = particle->child();
            int child_id_abs(-999.); 
            try 	{ child_id_abs = abs(child->pdgId()); }
            catch (...)	{ Warning("execute()", "Failed to retrieve the child pdg id."); continue; }
            if (m_debugMode) std::cout << std::endl; 
            if (m_debugMode) std::cout << "Particle pdg id:		" << particle->pdgId() << std::endl;
            if (m_debugMode) std::cout << "Particle pt:			" << particle->pt() << "	Particle eta:		" << particle->eta() << std::endl;
            if (m_debugMode) std::cout << "status:			" << particle->status() << std::endl;
            if (m_debugMode) std::cout << "barcode:			" << particle->barcode() << std::endl;
            if (m_debugMode) std::cout << "child pdg id:			" << child->pdgId() << std::endl;
            // W
            if (id_abs==24) { 
                if (child_id_abs>=11 && child_id_abs<=14)				W_lep++;
                else									W_had++;
            }
            // Z
            if (id_abs==23) { 
                if (child_id_abs==11 || child_id_abs==13)				Z_lep++;
                else if (child_id_abs==12 || child_id_abs==14 || child_id_abs==16)	Z_inv++;
                else									Z_had++;
            }
          }
          catch (...) 	{ Warning("execute()", "Failed to retrieve the child of a particle."); }
        }
      } catch (...) { Warning("execute()","Problems with truth particles..."); }
    }
    m_W_lep                             = W_lep;
    m_W_had                             = W_had;
    m_Z_lep                             = Z_lep;
    m_Z_inv				= Z_inv;
    m_Z_had                             = Z_had;

    // in AODs no truthMuons and truthElectrons containers...
    // not sure it works on AODs btw, check in case...
*/



    //
    // Truth-level objects
    //







    if (m_debugMode) std::cout<< "End of execute()" <<std::endl; 

    // Record the objects into the output xAOD:
    ANA_CHECK(event->record( beforeOLR_jets.release()    		,"Jets" ));
    ANA_CHECK(event->record( beforeOLR_jetsAux.release() 		,"JetsAux." ));

    // Fill the tree
    m_tree->Fill();
  
    // Save the event:
    event->fill();

    return EL::StatusCode::SUCCESS;
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: postExecute ()
  {
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: finalize ()
  {
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.
  
    ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  
    xAOD::TEvent* event = wk()->xaodEvent();
  
    std::cout << "Removing initialised tools..." << std::endl;

    // in finalize, delete tool:
    if( m_PileupReweightingTool && !m_isAODfile ) {
      delete m_PileupReweightingTool;
      m_PileupReweightingTool = 0;
    }
    if( m_GoodRunsListSelectionTool ) {
      delete m_GoodRunsListSelectionTool;
      m_GoodRunsListSelectionTool = 0;
    }
    if( m_xAODConfigTool ) {
      delete m_xAODConfigTool;
      m_xAODConfigTool = 0;
    }
    if( m_TrigDecisionTool ) {
      delete m_TrigDecisionTool;
      m_TrigDecisionTool = 0;
    }
    if( m_OLR ) {
      delete m_OLR;
      m_OLR = 0;
    }
//    if( m_EventCleaningTool ) {
//      delete m_EventCleaningTool;
//      m_EventCleaningTool = 0;
//    }
    if( m_jetCleaning ) {
      delete m_jetCleaning;
      m_jetCleaning = 0;
    }
    if(m_JERTool){
      delete m_JERTool;
      m_JERTool = 0;
    }
    // in finalize, delete tool:
    if(m_JetCalibrationTool) {
      delete m_JetCalibrationTool;
      m_JetCalibrationTool = 0;
    }
    if(m_JetUncertaintiesTool) {
      delete m_JetUncertaintiesTool;
      m_JetUncertaintiesTool = 0;
    }
    if(m_JERSmearingTool) {
      delete m_JERSmearingTool;
      m_JERSmearingTool = 0;
    }
    if(m_JetVertexTaggerTool) {
      delete m_JetVertexTaggerTool;
      m_JetVertexTaggerTool = 0;
    }
    if(m_JetCaloEnergies) {
      delete m_JetCaloEnergies;
      m_JetCaloEnergies = 0;
    }
    if(m_JetVertexTaggerToolSF) {
      delete m_JetVertexTaggerToolSF;
      m_JetVertexTaggerToolSF = 0;
    }
    if(m_IsolationSelectionTool) {
      delete m_IsolationSelectionTool;
      m_IsolationSelectionTool = 0;
    }
    if(m_IsolationSelectionTool_FixedCutLoose) {
      delete m_IsolationSelectionTool_FixedCutLoose;
      m_IsolationSelectionTool_FixedCutLoose = 0;
    }
    if(m_MuonSelectionTool_looseID) {
      delete m_MuonSelectionTool_looseID;
      m_MuonSelectionTool_looseID = 0;
    }
    if(m_MuonSelectionTool) {
      delete m_MuonSelectionTool;
      m_MuonSelectionTool = 0;
    }
    if(m_MuonCalibrationAndSmearingTool) {
      delete m_MuonCalibrationAndSmearingTool;
      m_MuonCalibrationAndSmearingTool = 0;
    }
    if(m_MuonEfficiencyScaleFactors_ID) {
      delete m_MuonEfficiencyScaleFactors_ID;
      m_MuonEfficiencyScaleFactors_ID = 0;
    }
    if(m_MuonEfficiencyScaleFactors_isolation) {
      delete m_MuonEfficiencyScaleFactors_isolation;
      m_MuonEfficiencyScaleFactors_isolation = 0;
    }
    if(m_MuonEfficiencyScaleFactors_TTVA) {
      delete m_MuonEfficiencyScaleFactors_TTVA;
      m_MuonEfficiencyScaleFactors_TTVA = 0;
    }
    if(m_MuonEfficiencyScaleFactors_PLV) {
      delete m_MuonEfficiencyScaleFactors_PLV;
      m_MuonEfficiencyScaleFactors_PLV = 0;
    }
    if(m_MuonTriggerScaleFactors) {
      delete m_MuonTriggerScaleFactors;
      m_MuonTriggerScaleFactors = 0;
    }
    if(m_EgammaCalibrationAndSmearingTool) {
      delete m_EgammaCalibrationAndSmearingTool; 
      m_EgammaCalibrationAndSmearingTool = 0;
    }
    if(m_AsgElectronLikelihoodTool_looseID) {
      delete m_AsgElectronLikelihoodTool_looseID; 
      m_AsgElectronLikelihoodTool_looseID = 0;
    }
    if(m_AsgElectronLikelihoodTool) {
      delete m_AsgElectronLikelihoodTool; 
      m_AsgElectronLikelihoodTool = 0;
    }
    if(m_AsgElectronChargeIDSelectorTool) {
      delete m_AsgElectronChargeIDSelectorTool; 
      m_AsgElectronChargeIDSelectorTool = 0;
    }
    // Application of iso corrections on electrons is currently (r21.2.25) not needed, see https://its.cern.ch/jira/browse/ATLEGAMDPD-75
//    if(m_IsolationCorrectionTool) {
//      delete m_IsolationCorrectionTool; 
//      m_IsolationCorrectionTool = 0;
//    }
    if(m_AsgElectronEfficiencyCorrectionTool_ID) {
      delete m_AsgElectronEfficiencyCorrectionTool_ID;
      m_AsgElectronEfficiencyCorrectionTool_ID = 0;
    }
    if(m_AsgElectronEfficiencyCorrectionTool_ID_TightLH) {
      delete m_AsgElectronEfficiencyCorrectionTool_ID_TightLH;
      m_AsgElectronEfficiencyCorrectionTool_ID_TightLH = 0;
    }
    if(m_AsgElectronEfficiencyCorrectionTool_isolation) {
      delete m_AsgElectronEfficiencyCorrectionTool_isolation;
      m_AsgElectronEfficiencyCorrectionTool_isolation = 0;
    }
    if(m_AsgElectronEfficiencyCorrectionTool_reco) {
      delete m_AsgElectronEfficiencyCorrectionTool_reco;
      m_AsgElectronEfficiencyCorrectionTool_reco = 0;
    }
    if(m_AsgElectronEfficiencyCorrectionTool_trigSF) {
      delete m_AsgElectronEfficiencyCorrectionTool_trigSF;
      m_AsgElectronEfficiencyCorrectionTool_trigSF = 0;
    }
    if(m_AsgElectronEfficiencyCorrectionTool_PLV) {
      delete m_AsgElectronEfficiencyCorrectionTool_PLV;
      m_AsgElectronEfficiencyCorrectionTool_PLV = 0;
    }
    if(m_BTaggingSelectionTool) {
      delete m_BTaggingSelectionTool;
      m_BTaggingSelectionTool = 0;
    }
    if(m_BTaggingEfficiencyTool) {
      delete m_BTaggingEfficiencyTool;
      m_BTaggingEfficiencyTool = 0;
    }


    // finalize and close our output xAOD file:
    TFile *file = wk()->getOutputFile (m_outputName.c_str());
    //TFile *file = wk()->getOutputFile ("outputLabel");
    //if (m_isnominalTTree) ANA_CHECK(event->finishWritingTo( file ));
    ANA_CHECK(event->finishWritingTo( file ));
  
    return EL::StatusCode::SUCCESS;
  }
  
  
  
  EL::StatusCode MyxAODAnalysis :: histFinalize ()
  {
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.

    if (m_isnominalTTree) {
      h_total_events->SetBinContent(0,m_number_of_events_before_skimming);
      h_total_events->SetBinContent(1,m_sum_of_weights_before_skimming);
      for (unsigned int i=0; i<m_allSUM_of_weights_before_skimming.size(); i++) {
        h_total_events->SetBinContent(100+i,m_allSUM_of_weights_before_skimming[i]);
      }
    }

    return EL::StatusCode::SUCCESS;
  }

}
