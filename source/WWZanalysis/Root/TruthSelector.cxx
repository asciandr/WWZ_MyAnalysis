#include "WWZanalysis/TruthSelector.h"

namespace WWZanalysis {

  namespace TruthSelector {

    int GetHiggsDecayMode(const xAOD::TruthParticleContainer* truthCont) {
      // Categorise Higgs-boson decay mode
      int bb(0), WW(1), gg(2), tautau(3), cc(4), ZZ(5), gamgam(6), Zgamma(7), mumu(8), ss(9), taugamma(10);
      int unclassified(-99);

      for(const xAOD::TruthParticle *part: *truthCont) {
        if(!part) continue;
        //std::cout << "Truth particle PDG ID:\t" << part->pdgId() << std::endl;
        //std::cout << "Before number of children" << std::endl;
        //std::cout << part->nChildren() << std::endl;
        //std::cout << "After number of children" << std::endl;
        // is it a "good" Higgs boson?
        if ( std::abs(part->pdgId()) != 25 || (part->nChildren() < 2 ) ) continue;
        std::vector<int> higgs_cpdg;
        higgs_cpdg.clear();
        for(unsigned int mcp_c = 0; mcp_c < part->nChildren(); mcp_c++) {
          if(!part->child(mcp_c)) continue;
          int cpdg = part->child(mcp_c)->pdgId();
          //std::cout << "Truth child PDG ID:\t" << cpdg << std::endl;
          //15: status 2                 
          //5 : status 23                 
          //24: status 22                 
          //23: status 22?                 
          //22: status 1, 23?                 
          //13: status 1                 
          //21: status 23
          higgs_cpdg.push_back(cpdg);
        }
        int gammacount = 0;
        //if there is one (or more) extra gamma(s), remove it(them)
        if(higgs_cpdg.size() > 2 ) {
          //std::cout << "More than 2 children:\t" << higgs_cpdg.size() << std::endl;
          gammacount = std::count(higgs_cpdg.begin(), higgs_cpdg.end(), 22);
          if(higgs_cpdg.size() - gammacount >= 1) {
            std::vector<int>::iterator pos;
            //if only 1 other particle + photons, leave last photon
            if(higgs_cpdg.size() - gammacount == 1) gammacount--;
            for(int iter = 0; iter < gammacount; iter++) {                         
              pos = find(higgs_cpdg.begin(), higgs_cpdg.end(),22);                         
              higgs_cpdg.erase(pos);                     
            }
          }
        }
        if(higgs_cpdg.size()==2) {
          if(higgs_cpdg[0] + higgs_cpdg[1] == 0) {
            if(abs(higgs_cpdg[0]) == 5) return bb;                     
            if(abs(higgs_cpdg[0]) == 3) return ss;                     
            if(abs(higgs_cpdg[0]) == 24) return WW;                     
            if(abs(higgs_cpdg[0]) == 23) return ZZ;                     
            if(abs(higgs_cpdg[0]) == 4) return cc;                     
            if(abs(higgs_cpdg[0]) == 15) return tautau;                     
            if(abs(higgs_cpdg[0]) == 22) return gamgam;                     
            if(abs(higgs_cpdg[0]) == 13) return mumu;                     
            if(abs(higgs_cpdg[0]) == 21) return gg;
          } else if(higgs_cpdg[0] == higgs_cpdg[1]) {
            if(higgs_cpdg[0] == 23) return ZZ;                     
            if(higgs_cpdg[0] == 22) return gamgam;                     
            if(higgs_cpdg[0] == 21) return gg;
          }
          else if(higgs_cpdg[0] == 22 && higgs_cpdg[1] == 23) return Zgamma;                 
          else if(higgs_cpdg[0] == 23 && higgs_cpdg[1] == 22) return Zgamma;                 
          else if(abs(higgs_cpdg[0]) == 15 && higgs_cpdg[1] == 22) return taugamma;                 
          else if(higgs_cpdg[0] == 22 && abs(higgs_cpdg[1]) == 15) return taugamma;                 
          else std::cerr << "1 = " << higgs_cpdg[0] << ", 2 = " << higgs_cpdg[1] << std::endl;
        }
        std::cout<<"Warning: Higgs children size: "<<higgs_cpdg.size() <<std::endl;             
        return unclassified;
      }
      return unclassified;

    }



    std::vector<int> GetZDecayMode(const xAOD::TruthParticleContainer* truthCont) {
      std::vector<int> ZbosonDecays;
      // Categorise Z-boson decay mode
      int ee(0), mm(1), tautau(2), nunu(3), had(4);
      int unclassified(-99);

      for(const xAOD::TruthParticle *part: *truthCont) {
        if(!part) continue;
        //std::cout << "Truth particle PDG ID:\t" << part->pdgId() << std::endl;
        //std::cout << "Before number of children" << std::endl;
        //std::cout << part->nChildren() << std::endl;
        //std::cout << "After number of children" << std::endl;
        // is it a "good" Z boson?
        if ( std::abs(part->pdgId()) != 23 || (part->nChildren() < 2 ) ) continue;
        std::vector<int> higgs_cpdg;
        higgs_cpdg.clear();
        for(unsigned int mcp_c = 0; mcp_c < part->nChildren(); mcp_c++) {
          if(!part->child(mcp_c)) continue;
          int cpdg = part->child(mcp_c)->pdgId();
          //std::cout << "Truth child PDG ID:\t" << cpdg << std::endl;
          higgs_cpdg.push_back(cpdg);
        }
        int gammacount = 0;
        //if there is one (or more) extra gamma(s), remove it(them)
        if(higgs_cpdg.size() > 2 ) {
          std::cout << "Z boson with more than 2 children:\t" << higgs_cpdg.size() << std::endl;
          gammacount = std::count(higgs_cpdg.begin(), higgs_cpdg.end(), 22);
          if(higgs_cpdg.size() - gammacount >= 1) {
            std::vector<int>::iterator pos;
            //if only 1 other particle + photons, leave last photon
            if(higgs_cpdg.size() - gammacount == 1) gammacount--;
            for(int iter = 0; iter < gammacount; iter++) {                         
              pos = find(higgs_cpdg.begin(), higgs_cpdg.end(),22);                         
              higgs_cpdg.erase(pos);                     
            }
          }
        }
        if(higgs_cpdg.size()==2) {
          //std::cout << "Z-boson children are :\t" << higgs_cpdg[0] << " and " << higgs_cpdg[1] << std::endl;
          if(higgs_cpdg[0] + higgs_cpdg[1] == 0) {
            if(abs(higgs_cpdg[0]) == 11) ZbosonDecays.push_back(ee);                     
            if(abs(higgs_cpdg[0]) == 13) ZbosonDecays.push_back(mm);                     
            if(abs(higgs_cpdg[0]) == 15) ZbosonDecays.push_back(tautau);                     
            if(abs(higgs_cpdg[0]) == 12 || abs(higgs_cpdg[0]) == 14 || abs(higgs_cpdg[0]) == 16) ZbosonDecays.push_back(nunu);                     
            if(abs(higgs_cpdg[0]) <=  6) ZbosonDecays.push_back(had);                     
          } else if(higgs_cpdg[0] == higgs_cpdg[1]) {
            std::cout << "Check me -> Z-boson whose children have same PDG ID!" << std::endl;
            ZbosonDecays.push_back(unclassified);
          }
          else std::cerr << "1 = " << higgs_cpdg[0] << ", 2 = " << higgs_cpdg[1] << std::endl;
        }
        else {
          std::cout<<"Warning: Z children size: "<<higgs_cpdg.size() <<std::endl;             
          ZbosonDecays.push_back(unclassified);
        }
      }
      return ZbosonDecays;

    }


  }

}
