#include "WWZanalysis/SystematicVariations.h"

// Anonymous namespace ensures file scope
namespace {
  enum class VariationType {UP = 1, DOWN = -1};
}

namespace WWZanalysis {

  namespace SystematicVariations {



    CP::SystematicSet make_variation(std::string const & name, VariationType type) {
      auto var = CP::SystematicSet();
      var.insert(CP::SystematicVariation(name, static_cast<int>(type)));
      return var;
    }



    std::vector<CP::SystematicSet> make_systematic_variations_vector(std::vector<std::string> const & variation_names) {
      auto out = std::vector<CP::SystematicSet>();
      out.reserve(variation_names.size());

      /// Add nominal "variation"
      auto const nominal = CP::SystematicSet();
      out.push_back(nominal);

      /// Add the desired variations
      for (auto const & name : variation_names) {
        // Up variation
        auto const up = make_variation(name, VariationType::UP);
        out.push_back(up);
        // Down variation
        auto const down = make_variation(name, VariationType::DOWN);
        out.push_back(down);
      }

      return out;
    }



  }

}
