#include "WWZanalysis/SkimmingInfo.h"

#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"

#include <stdexcept>

namespace WWZanalysis {

  namespace SkimmingInfo {



    xAOD::CutBookkeeper const * get_all_events_cut_bookkeeper(xAOD::TEvent * const event) {
      xAOD::CutBookkeeperContainer const * cbk_container = 0;

      if (! event->retrieveMetaInput(cbk_container, "CutBookkeepers").isSuccess()) {
        delete cbk_container;
        throw std::runtime_error("SkimmingInfo::get_all_events_cut_bookkeeper(): failed to get container CutBookkeepers!");
      };

      // Now, find the right cutbookkeeper that contains all the needed info...
      int max_cycle = -1;
      xAOD::CutBookkeeper const * all = nullptr;
      for (auto const * cbk : * cbk_container) {
        if (cbk->inputStream() == "StreamAOD" && cbk->name() == "AllExecutedEvents" && cbk->cycle() > max_cycle) {
          max_cycle = cbk->cycle();
          all = cbk;
        }
      }
      if (max_cycle == -1 || ! all) {
        delete all;
        throw std::runtime_error("Failed to find needed cutbookkeeper!");
      }

      return all;
    }



    double sum_of_weights_before_skimming(xAOD::TEvent * const some_event_in_the_sample) {
      return get_all_events_cut_bookkeeper(some_event_in_the_sample)->sumOfEventWeights();
    }



    uint64_t number_of_events_before_skimming(xAOD::TEvent * const some_event_in_the_sample) {
      return get_all_events_cut_bookkeeper(some_event_in_the_sample)->nAcceptedEvents();
    }



    xAOD::CutBookkeeper const * get_events_cut_bookkeeper(xAOD::TEvent * const event, std::string cbkName) {
      xAOD::CutBookkeeperContainer const * cbk_container = 0;

      if (! event->retrieveMetaInput(cbk_container, "CutBookkeepers").isSuccess()) {
        delete cbk_container;
        throw std::runtime_error("SkimmingInfo::get_all_events_cut_bookkeeper(): failed to get container CutBookkeepers!");
      };

      // Now, find the right cutbookkeeper that contains all the needed info...
      int max_cycle = -1;
      xAOD::CutBookkeeper const * all = nullptr;
      for (auto const * cbk : * cbk_container) {
        //std::cout << "cbk name:\t" << cbk->name() << std::endl;
        if (cbk->inputStream() == "StreamAOD" && cbk->name() == cbkName) {
          //std::cout << "I AM IN" << std::endl;
          //std::cout << "and it's: " << cbk->name() << std::endl;
          //std::cout << "and my sumOfEventWeights is:" << std::endl;
          //std::cout << cbk->sumOfEventWeights() << std::endl;
          max_cycle = cbk->cycle();
          all = cbk;
          break;
        }
      }
      if (max_cycle == -1 || ! all) {
        delete all;
        throw std::runtime_error("Failed to find needed cutbookkeeper!");
      }

      //std::cout << "going to return all" << std::endl;

      return all;
    }



    double all_sum_of_weights_before_skimming(xAOD::TEvent * const some_event_in_the_sample, std::string cbkName) {
      return get_events_cut_bookkeeper(some_event_in_the_sample, cbkName)->sumOfEventWeights();
    }



  }
}
