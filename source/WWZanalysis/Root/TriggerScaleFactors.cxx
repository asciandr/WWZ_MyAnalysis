#include "WWZanalysis/TriggerScaleFactors.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"

//#include "CPAnalysisExamples/errorcheck.h"

#include <cstddef> // for std::size_t
#include <stdexcept>

#include <iostream>

namespace WWZanalysis {

  namespace TriggerScaleFactors {



    //double get_muon_trigger_weight(CP::MuonTriggerScaleFactors * tool, std::string const & trigger_name, std::vector<xAOD::Muon const *> const & muons) {
    double get_muon_trigger_weight(CP::MuonTriggerScaleFactors * tool, std::string const & trigger_name, const std::vector<xAOD::Muon *> & muons) {
      double weight = 1.0; // non-const!

      // Check if tool pointer is valid
      if (! tool) return weight;

      // Have to make a muon container for the tool :(
      xAOD::MuonContainer * muon_container = new xAOD::MuonContainer();
      xAOD::MuonAuxContainer * muon_container_aux = new xAOD::MuonAuxContainer();
      muon_container->setStore(muon_container_aux);
      for (auto const * ob : muons) {
        auto * copy = new xAOD::Muon;
        copy->makePrivateStore(* ob);
        * copy = * ob;
        muon_container->push_back(copy);
      }

      //char const * APP_NAME = "TriggerScaleFactors"; // for quick error checks

      tool->getTriggerScaleFactor(* muon_container, weight, trigger_name);
      //CHECK(tool->getTriggerScaleFactor(* muon_container, weight, trigger_name));

      if (muon_container) delete muon_container;
      if (muon_container_aux) delete muon_container_aux;

      return weight;
    }



    std::vector<double> get_scale_factors_or_efficiencies(AsgElectronEfficiencyCorrectionTool * tool, std::vector<xAOD::Electron const *> const & electrons) {
      std::vector<double> results = {};
      for (auto const * el : electrons) {
        double result = 1.0;
        tool->getEfficiencyScaleFactor(* el, result);
        results.push_back(result);
      }
      return results;
    }



    double product(std::vector<double> const & scale_factors, std::vector<double> const & mc_efficiencies) {
      if (scale_factors.size() != mc_efficiencies.size()) {
        throw std::logic_error("Vectors of per-electron trigger scale factors and MC efficiencies must have same size!");
      }
      double prod = 1.0;
      for (std::size_t i = 0; i != scale_factors.size(); ++i) {
        prod *= 1.0 - scale_factors.at(i) * mc_efficiencies.at(i);
      }
      return prod;
    }



    double calculate_trigger_weight(std::vector<double> const & scale_factors, std::vector<double> const & mc_efficiencies) {
      auto const ones = std::vector<double> (scale_factors.size(), 1.0);
      auto const prod_data = product(scale_factors, mc_efficiencies);
      auto const prod_mc = product(ones, mc_efficiencies);
      return ((1.0 - prod_mc) == 0.0) ? 1.0 : (1.0 - prod_data) / (1.0 - prod_mc);
    }



    double get_electron_trigger_weight(AsgElectronEfficiencyCorrectionTool * mceff_tool, AsgElectronEfficiencyCorrectionTool * sf_tool, std::vector<xAOD::Electron const *> const & electrons) {
      // Check if both tool pointers are valid, else return
      if (! mceff_tool || ! sf_tool) return 1.0;

      auto const scale_factors = get_scale_factors_or_efficiencies(sf_tool, electrons);
      auto const mc_efficiencies = get_scale_factors_or_efficiencies(mceff_tool, electrons);

      return calculate_trigger_weight(scale_factors, mc_efficiencies);
    }



  }

}
