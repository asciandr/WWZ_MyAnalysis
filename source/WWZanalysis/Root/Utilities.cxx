#include "WWZanalysis/Utilities.h"

namespace WWZanalysis {

  namespace UtilitiesFunctions {


    std::vector<float> vectorMultiplication(const std::vector<float>& v1, const std::vector<float>& v2) {
      std::vector<float> result;
      std::transform(v1.begin(), v1.end(), v2.begin(),
                     std::back_inserter(result), std::multiplies<float>());
      return result;
    }



    bool do_nominal(std::vector<CP::SystematicSet> const & systematics) {
      for (auto const & s : systematics) {
        if (s.name() == "") return true;
      }
      return false;
    }



    void fill_pt_eta_n_histograms(std::vector<xAOD::Electron const *> const & objects, TH1 *& pt_histo, TH1 *& eta_histo, TH1 *& n_histo) {

      for (auto const * ob : objects) {
        double weight = 1.0;
        if (ob->isAvailable<double>("scale_factor_weight")) {
          weight = ob->auxdataConst<double>("scale_factor_weight");
        }
        pt_histo->Fill(ob->pt() * 0.001, weight);
        eta_histo->Fill(ob->eta(), weight);
      }

      n_histo->Fill(objects.size());
    }



    void fill_pt_eta_n_histograms(std::vector<xAOD::Muon const *> const & objects, TH1 *& pt_histo, TH1 *& eta_histo, TH1 *& n_histo) {

      for (auto const * ob : objects) {
        double weight = 1.0;
        if (ob->isAvailable<double>("scale_factor_weight")) {
          weight = ob->auxdataConst<double>("scale_factor_weight");
        }
        pt_histo->Fill(ob->pt() * 0.001, weight);
        eta_histo->Fill(ob->eta(), weight);
      }

      n_histo->Fill(objects.size());
    }



  }



  void Utilities::newTH1(TH1*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title, int nbins, double xmin, double xmax) {
    std::string directory;
    if (type == HistoType::RECO) directory = "Reco/";
    else if (type == HistoType::TRUTH) directory = "Truth/";
    else if (type == HistoType::MATCHED) directory = "Matched/";
    else if (type == HistoType::CONTROL) directory = "Control/";
    else Error("Utilities::newTH1()", "Unknown histogram type (not RECO, TRUTH, MATCHED, or CONTROL). Exiting.");
    histo = new TH1D((directory + name).c_str(), title.c_str(), nbins, xmin, xmax);
    /// Initialise sum-of-squared-weights structure for the histogram
    histo->Sumw2();
    worker->addOutput(histo);
  }



  void Utilities::newTH2(TH2*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title, int nbins, double xymin, double xymax) {
    std::string directory;
    if (type == HistoType::RECO) directory = "Reco/";
    else if (type == HistoType::TRUTH) directory = "Truth/";
    else if (type == HistoType::MATCHED) directory = "Matched/";
    else if (type == HistoType::CONTROL) directory = "Control/";
    else Error("Utilities::newTH2()", "Unknown histogram type (not RECO, TRUTH, MATCHED, or CONTROL). Exiting.");
    histo = new TH2F((directory + name).c_str(), title.c_str(), nbins, xymin, xymax, nbins, xymin, xymax);
    /// Initialise sum-of-squared-weights structure for the histogram
    histo->Sumw2();
    worker->addOutput(histo);
  }

  void Utilities::newTH2(TH2*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title, int nxbins, double xmin, double xmax, int nybins, double ymin, double ymax) {
    std::string directory;
    if (type == HistoType::RECO) directory = "Reco/";
    else if (type == HistoType::TRUTH) directory = "Truth/";
    else if (type == HistoType::MATCHED) directory = "Matched/";
    else if (type == HistoType::CONTROL) directory = "Control/";
    else Error("Utilities::newTH2()", "Unknown histogram type (not RECO, TRUTH, MATCHED, or CONTROL). Exiting.");
    histo = new TH2F((directory + name).c_str(), title.c_str(), nxbins, xmin, xmax, nybins, ymin, ymax);
    /// Initialise sum-of-squared-weights structure for the histogram
    histo->Sumw2();
    worker->addOutput(histo);
  }

  //enable variable binning for TH2s
  void Utilities::newTH2(TH2*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title, int nxbins, const double* xedges, int nybins, const double* yedges) {
    std::string directory;
    if (type == HistoType::RECO) directory = "Reco/";
    else if (type == HistoType::TRUTH) directory = "Truth/";
    else if (type == HistoType::MATCHED) directory = "Matched/";
    else if (type == HistoType::CONTROL) directory = "Control/";
    else Error("Utilities::newTH2()", "Unknown histogram type (not RECO, TRUTH, MATCHED, or CONTROL). Exiting.");
    histo = new TH2F((directory + name).c_str(), title.c_str(), nxbins, xedges, nybins, yedges);
    /// Initialise sum-of-squared-weights structure for the histogram                                                                                                                              
    histo->Sumw2();
    worker->addOutput(histo);
  }



}
