#include "WWZanalysis/Dilepton.h"

#include <cmath>



namespace WWZanalysis {



  Dilepton::Dilepton(LeptonFlavour flavour_, std::pair<TLorentzVector, TLorentzVector> momenta_) :
      flavour(flavour_),
      momenta(momenta_)
  {
  }



  TLorentzVector Dilepton::momentum() const {
    return (momenta.first + momenta.second);
  }



  double Dilepton::mass() const {
    return this->momentum().M() * 0.001; // Converted to GeV!
  }



  double Dilepton::dr() const {
    return momenta.first.DeltaR(momenta.second);
  }



}
