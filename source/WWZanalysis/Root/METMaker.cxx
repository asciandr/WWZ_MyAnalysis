#include "WWZanalysis/METMaker.h"

#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"

namespace WWZanalysis {
  namespace METMaker {

     xAOD::MissingETContainer *recalculateEventMET( const xAOD::MissingETContainer *xaod_met_core, const xAOD::MissingETAssociationMap* xaod_met_map, asg::AnaToolHandle<IMETMaker> metMaker, const xAOD::ElectronContainer *xaod_el, const xAOD::MuonContainer *xaod_mu, const xAOD::JetContainer *xaod_jet, AsgElectronLikelihoodTool *m_AsgElectronLikelihoodTool_looseID, CP::MuonSelectionTool *m_MuonSelectionTool_looseID, asg::AnaToolHandle<IMETSystematicsTool> metSystTool) {
       // Make a new MET container that we are going to use     
       xAOD::MissingETContainer *new_met_container = new xAOD::MissingETContainer();     
       xAOD::MissingETAuxContainer *new_met_aux_container = new xAOD::MissingETAuxContainer();     
       new_met_container->setStore(new_met_aux_container);
       // Reset all the met map associations
       xaod_met_map->resetObjSelectionFlags();
       // 1. Electrons     
       ConstDataVector<xAOD::ElectronContainer> met_electrons(SG::VIEW_ELEMENTS);       
       for( const auto& el: *xaod_el ) {
         if ( !(el->pt()/1000. > 10.) ) 					continue;
         auto const electron_cluster_eta = std::abs(el->caloCluster()->etaBE(2));
         if ( (electron_cluster_eta>1.37 && electron_cluster_eta<1.52) )	continue;
         if ( electron_cluster_eta>=2.47 ) 					continue;
         // Electron LH is not supposed to be computed by analyses FWs anymore, but retrieved from DAODs, see https://groups.cern.ch/group/hn-atlas-EGammaWG/Lists/Archive/Flat.aspx?RootFolder=%2fgroup%2fhn%2datlas%2dEGammaWG%2fLists%2fArchive%2fMissing%20likelihood%20variables%20in%20default%20derivation%20lists&FolderCTID=0x01200200D17DAC8476E41D4F8451088E75A5CE34
         bool passes(false);
         if(el->isAvailable<char>(("DFCommonElectronsLHLooseBL"))) {
           passes								= static_cast<bool>(el->auxdata<char>("DFCommonElectronsLHLooseBL"));
           //std::cout << "METMaker -> DFCommonElectronsLHLooseBL is available! -> passes:              " << passes << std::endl;
         }
         else {
           //std::cout << "METMaker -> DFCommonElectronsLHLooseBL aux decoration is NOT available!" << std::endl;
           passes								= m_AsgElectronLikelihoodTool_looseID->accept(el);
         }

         if ( !passes )                                       			continue;
         met_electrons.push_back(el);
       }
       metMaker->rebuildMET("RefEle", xAOD::Type::Electron, new_met_container, met_electrons.asDataVector(), xaod_met_map);
       //metMaker->rebuildMET("RefEle", xAOD::Type::Electron, new_met_container, xaod_el, xaod_met_map);
       // 2. Muons
       ConstDataVector<xAOD::MuonContainer> met_muons(SG::VIEW_ELEMENTS);       
       for( const auto& mu: *xaod_mu ) {
         if ( !(mu->pt()/1000. > 10.) ) 					continue;
         if ( !m_MuonSelectionTool_looseID->accept(*mu) )			continue;
         met_muons.push_back(mu);
       }
       metMaker->rebuildMET("RefMuon", xAOD::Type::Muon, new_met_container, met_muons.asDataVector(), xaod_met_map);
       //metMaker->rebuildMET("RefMuon", xAOD::Type::Muon, new_met_container, xaod_mu, xaod_met_map);
       // 3. Jets
       ConstDataVector<xAOD::JetContainer> met_jets(SG::VIEW_ELEMENTS);
       for( const auto& jet : *xaod_jet ) {
         if( !(jet->pt()/1000.>25. && jet->eta()<2.5) ) 			continue;
         met_jets.push_back(jet);
       }
       metMaker->rebuildJetMET("RefJet", "SoftClus", "PVSoftTrk", new_met_container, met_jets.asDataVector(), xaod_met_core, xaod_met_map, true);

       // SYSTEMATICS
       // get the soft cluster term, and applyCorrection
       xAOD::MissingET * softClusMet = (*new_met_container)["SoftClus"];
       if (softClusMet != nullptr) { //check we retrieved the clust term
         metSystTool->applyCorrection(*softClusMet);
       }
       xAOD::MissingET * softTrkMet = (*new_met_container)["PVSoftTrk"];
       if (softTrkMet != nullptr) { //check we retrieved the soft trk           
         metSystTool->applyCorrection(*softTrkMet);                
       }
       // SYSTEMATICS

       // This will sum up all the contributions we've made so far e.g.     
       // Total MET = RefEle + RefPhoton + RefTau + RefMuon + RefJet + PVSoftTrk (Track Soft Term)
       metMaker->buildMETSum("FinalTrk" , new_met_container, MissingETBase::Source::Track);

       //std::cout << "MET muon size:		" << met_muons.size() << std::endl;
       //std::cout << "MET electron size:		" << met_electrons.size() << std::endl;
       //std::cout << "MET jet size:		" << met_jets.size() << std::endl;
       //std::cout<<std::endl; std::cout<<std::endl; std::cout<<std::endl;      

      return new_met_container;
    }

  }
}
