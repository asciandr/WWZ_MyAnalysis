#include "WWZanalysis/ScaleFactors.h"

#include <numeric>



namespace WWZanalysis {

  namespace ScaleFactors {



    double sum_of_weights(std::vector<double> const & weights) {
      return std::accumulate(weights.begin(), weights.end(), 0.0);
    }



  }

}
