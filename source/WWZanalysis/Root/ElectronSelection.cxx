#include "WWZanalysis/ElectronSelection.h"

#include "PATCore/TAccept.h" // to analyse and use the output of the electron likelihood ID tool, and use its "subcuts" for a looser electron pre-identification

#include <stdexcept>
#include <iostream>

namespace WWZanalysis {
  namespace ElectronSelection {



    std::vector<xAOD::Electron const *> make_std_vector(xAOD::ElectronContainer const * in) {
      std::vector<xAOD::Electron const *> out;
      out.reserve(in->size());

      for (auto const & ob : * in) {
        out.push_back(ob);
      }

      return out;
    }



    std::vector<xAOD::Electron const *> select_in_eta_acceptance(std::vector<xAOD::Electron const *> const & in, double cluster_abs_eta_max) {
      std::vector<xAOD::Electron const *> out;
      out.reserve(in.size());

      for (auto const * ob : in) {
        auto const electron_cluster_abs_eta = std::abs(ob->caloCluster()->etaBE(2));
        // Removing el within crack region (1.37<eta<1.52)
        if (electron_cluster_abs_eta < cluster_abs_eta_max && (electron_cluster_abs_eta<1.37 || electron_cluster_abs_eta>1.52)) {
        //if (electron_cluster_abs_eta < cluster_abs_eta_max) 
          out.push_back(ob);
        }
      }

      return out;
    }



    std::vector<xAOD::Electron const *> select_silicon_hit_id(std::vector<xAOD::Electron const *> const & in, AsgElectronLikelihoodTool * electron_id_tool) {
      if (! electron_id_tool) {
        return in;
      }

      /// This would be a nice check, but doesn't work: getOperatingPointName() returns an empty string...
      //if (electron_id_tool->getOperatingPointName() != "Loose") {
      //  throw std::logic_error("For very, very loose 'silicon hit ID' of electrons, need an electron ID tool configured at working point 'Loose'. If that's not true for the main tool, please provide a second one that is!");
      //}

      std::vector<xAOD::Electron const *> out;
      out.reserve(in.size());

      for (auto const * ob : in) {
        Root::TAccept electron_id_info = electron_id_tool->accept(ob);
        bool const passes = (electron_id_info.getCutResult("NPixel") && electron_id_info.getCutResult("NSilicon"));
        if (passes) {
          out.push_back(ob);
        }
      }

      return out;
    }



    std::vector<xAOD::Electron const *> select_isolated(std::vector<xAOD::Electron const *> const & in, CP::IsolationSelectionTool * electron_iso_tool) {
      if (! electron_iso_tool) {
        return in;
      }

      std::vector<xAOD::Electron const *> out;
      out.reserve(in.size());

      for (auto const * ob : in) {
        if (electron_iso_tool->accept(* ob)) {
          out.push_back(ob);
        }
      }

      return out;
    }




  }
}
