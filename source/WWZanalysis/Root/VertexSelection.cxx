#include "WWZanalysis/VertexSelection.h"

namespace WWZanalysis {

  namespace VertexSelection {



    xAOD::Vertex const * get_hard_scattering_vertex(xAOD::VertexContainer const * primary_vertices) {
      for (auto const * vertex : * primary_vertices) {
        if (vertex->vertexType() == xAOD::VxType::PriVtx) {
          return vertex;
        }
      }
      return nullptr;
    }



  }

}
