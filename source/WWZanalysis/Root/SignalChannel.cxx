#include "WWZanalysis/SignalChannel.h"



namespace WWZanalysis {
  namespace SignalChannel {

    Type get_signal_channel(std::size_t number_of_electrons, std::size_t number_of_muons) {
      if (number_of_electrons == 4 && number_of_muons == 0) return Type::Electrons;
      if (number_of_electrons == 0 && number_of_muons == 4) return Type::Muons;
      if (number_of_electrons == 2 && number_of_muons == 2) return Type::Mixed;
      return Type::None;
    }



    int get_higgs_signal_channel(const DileptonPair & q) {
      int type = -1;
      if (q.get_leading_dilepton().flavour == LeptonFlavour::Muon) {
        type = (q.get_subleading_dilepton().flavour == LeptonFlavour::Muon ? 0 : 2);
      }
      else {
        type = (q.get_subleading_dilepton().flavour == LeptonFlavour::Electron ? 1 : 3);
      }
      return type;
    }



  }
}
