#include "WWZanalysis/GenericObjectSelection.h"

namespace WWZanalysis {
  namespace GenericObjectSelection {



    double calculate_abs_z0sintheta(xAOD::TrackParticle const * track, xAOD::Vertex const * primary_vertex) {
      /// Check if there is a valid primary vertex
      if (! primary_vertex) return 9999999.0;

      // Position of primary vertex:
      auto const z0_of_primary_vertex = primary_vertex->z();
      // Track longitudinal displacement:
      auto const z0_of_particle_wrt_track_vertex = track->z0();
      // Position of track reference system origin:
      auto const track_coordinate_system_origin = track->vz();

      // Calculate z0 with respect to primary vertex location along the beam axis:
      auto const z0 = z0_of_particle_wrt_track_vertex + track_coordinate_system_origin - z0_of_primary_vertex;

      // Track scattering angle theta:
      auto const theta = track->theta();

      return std::abs(z0 * sin(theta));
    }



  }
}
