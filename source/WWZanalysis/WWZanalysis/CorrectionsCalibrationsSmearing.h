// -*- c++ -*-
/// Functions for applying correction, calibration, smearing, etc. tools.
/// That is, any tools that modify objects but do not perform selections on them.

#pragma once

#include <vector>

namespace WWZanalysis {

  namespace Corrections {



    /// Apply correction tool to elements in a std::vector and return a vector of the corrected objects.
    ///
    /// The way this function works may look a bit confusing, so we'll explain it here.
    ///
    /// It takes a vector of old objects and a tool as arguments, and returns a vector of updated objects. So far, so good.
    /// But since the new objects need to be stored somewhere the xAOD way, we also need to pass a non-const reference to a
    /// container for them as argument. This is where the actual data will live, while the returned vector only contains
    /// pointers to their "interface."
    template <typename ContainerType, typename ObjectType, typename ToolType>
    std::vector<ObjectType const *> apply_correction_tool(std::vector<ObjectType const *> const & in, ContainerType * container_for_new, ToolType * tool, double abs_eta_max = 999999.9) {
      /// Clear the container for corrected objects, in case it was previously filled with something
      container_for_new->clear();
      /// Produce error if the target container is not empty!
      if (! container_for_new->empty()) {
        throw std::logic_error("Error in Corrections::apply_correction_tool(): container for corrected objects not empty after clear()!");
      }

      std::vector<ObjectType const *> out;

      /// Go over the objects in the input vector, try to apply correction, and add them to the container and output vector
      for (auto const * ob : in) {
        ObjectType * ob_corrected = new ObjectType();
        ob_corrected->makePrivateStore(* ob);

        auto object_within_tool_range = (std::abs(ob->eta()) < abs_eta_max);

        if (tool && object_within_tool_range) {
          if (! tool->applyCorrection(* ob_corrected)) {
            throw std::logic_error("Error in Corrections::apply_correction_tool(): cannot apply correction!");
          }
        }
        else {
          * ob_corrected = * ob;
        }

        container_for_new->push_back(ob_corrected);
        out.push_back(ob_corrected);
      }

      return out;
    }



    template <typename ContainerType, typename ToolType>
    void apply_correction_tool(ContainerType * container, ToolType * tool) {
      /// Go over the objects in the input vector, try to apply correction, and add them to the output vector
      for (auto const & ob : * container) {
        if (tool) {
          if (! tool->applyCorrection(* ob)) {
            throw std::logic_error("Error in Corrections::apply_correction_tool(): cannot apply correction!");
          }
        }
      }
    }



  }
}
