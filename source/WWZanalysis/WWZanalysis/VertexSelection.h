// -*- c++ -*-

#pragma once

#include "xAODTracking/VertexContainer.h"


namespace WWZanalysis {

  namespace VertexSelection {

    /// Returns pointer to (const) hard scattering vertex of the event
    /// CAUTION: returns nullpointer if no primary vertex is found!
    xAOD::Vertex const * get_hard_scattering_vertex(xAOD::VertexContainer const * primary_vertices);

  }

}
