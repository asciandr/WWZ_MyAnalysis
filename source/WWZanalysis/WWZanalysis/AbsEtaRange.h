// -*- c++ -*-
/// Absolute pseudorapidity range as a neat data structure.

#pragma once

#include <utility>
#include <cmath>

namespace WWZanalysis {

  struct AbsEtaRange {

    /// Constructor for double-sided absolute pseudorapidity range
    /// The constructor automatically determines the larger of the two values
    AbsEtaRange(double abs_eta_1, double abs_eta_2);



    /// Constructor for one-sided absolute pseudorapidity range
    /// The minimum is automatically set to 0.0
    AbsEtaRange(double abs_eta_max);



    /// Check if a value lies in the absolute pseudorapidity range
    bool contains_value(double value) const;



    /// Check if an object lies in the absolute pseudorapidity range
    template <typename ObjectType>
    bool contains(ObjectType const * ob) const {
      double ob_abs_eta = std::abs(ob->eta());
      return (ob_abs_eta >= abs_eta_min && ob_abs_eta < abs_eta_max);
    }



    double abs_eta_min;
    double abs_eta_max;

  };

}
