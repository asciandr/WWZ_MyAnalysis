// -*- c++ -*-
/// Z boson pole mass

#pragma once

namespace WWZanalysis {

  /// Z boson pole mass
  /// Value taken from PDG 2015
  double const Z_mass = 91.1876;

}
