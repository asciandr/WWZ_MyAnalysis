// -*- c++ -*-
#pragma once


extern "C" {
#include <stdint.h> // for uint32_t (the type of the run number, as used in https://svnweb.cern.ch/trac/atlasoff/browser/Event/xAOD/xAODEventInfo/trunk/xAODEventInfo/versions/EventInfo_v1.h)
}

#include <string>
#include <unordered_map>
#include <utility>

#include "xAODPrimitives/IsolationType.h" // needed to access isolation enumerated types

namespace WWZanalysis {

  class AnalysisConfig {
    /// Read and store the configuration of the analysis (which object collections to use, cut values, tool settings, etc.)

  public:

    /// Default constructor
    AnalysisConfig();

    /// Read the configuration from a specified file
    /// The file path may still contain shell variables (probably $ROOTCOREBIN), which will be expanded internally
    /// Return true if successful
    bool read(std::string const & unexpanded_file_path);

    /// Print configuration settings to screen (useful for debugging)
    void print() const;

    /// Return the desired parameter of type double
    double get_float(std::string param) const;

    /// Return the desired parameter of type int
    int get_int(std::string param) const;

    /// Return the desired parameter of type std::string
    std::string get_string(std::string param) const;

    /// Return the desired parameter of type bool
    bool get_bool(std::string param) const;

    /// Check if the configuration makes sense
    /// Throw an exception of type std::logic_error if inconsistent settings are encountered
    void consistency_check() const;

  public:

    std::unordered_map<std::string, double> float_params;
    std::unordered_map<std::string, int> int_params;
    std::unordered_map<std::string, std::string> string_params;
    std::unordered_map<std::string, bool> bool_params;
    std::unordered_map<std::string, std::pair<uint32_t, uint32_t>> triggers;

    std::unordered_map<std::string, std::string> descriptions;

  };

}
