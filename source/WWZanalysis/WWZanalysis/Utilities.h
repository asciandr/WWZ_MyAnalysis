// -*- c++ -*-
#pragma once

#include <cstddef> // for std::size_t
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

#include "PATInterfaces/SystematicVariation.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODEgamma/Electron.h"
#include <TH1.h>
#include <TH2.h>
#include "TLorentzVector.h"
#include "EventLoop/Worker.h"
#include "WWZanalysis/WrappedHistogram.h"
#include "WWZanalysis/ScaleFactors.h"

namespace WWZanalysis {



  class Utilities {
  public:


    template <typename ObjectType>
    static bool greater_pT_pointers(ObjectType const * a, ObjectType const * b) {
      return a->pt() > b->pt();
    }



    template <typename ObjectType>
    static bool smaller_pT_pointers(ObjectType const * a, ObjectType const * b) {
      return a->pt() < b->pt();
    }



    template <typename ObjectType>
    static std::vector<ObjectType const *> sort_by_pT_pointers(std::vector<ObjectType const *> const & in) {
      auto out = in;
      std::sort(out.begin(), out.end(), greater_pT_pointers<ObjectType>);
      return out;
    }


    template <typename ObjectType>
    static std::vector<ObjectType *> sort_by_pT_pointers(std::vector<ObjectType *> & in) {
    //static std::vector<ObjectType const *> sort_by_pT_pointers(std::vector<ObjectType const *> const & in) 
      auto out = in;
      std::sort(out.begin(), out.end(), greater_pT_pointers<ObjectType>);
      return out;
    }



    template <typename ObjectType>
    static std::vector<ObjectType const *> sort_by_increasing_pT_pointers(std::vector<ObjectType const *> const & in) {
      auto out = in;
      std::sort(out.begin(), out.end(), smaller_pT_pointers<ObjectType>);
      return out;
    }



    // Return true if the first of two physics objects (jets, electrons, muons, ...) has the greater pT
    template <typename Object>
    static bool greater_pT(Object const & a, Object const & b) {
      return a.pt() > b.pt();
    }



    template <typename Object>
    static bool smaller_pT(Object const & a, Object const & b) {
      return a.pt() < b.pt();
    }



    static void newTH1(TH1*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title, int nbins, double xmin, double xmax);

    static void newTH2(TH2*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title, int nbins, double xymin, double xymax);
    static void newTH2(TH2*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title, int nxbins, double xmin, double xmax, int nybins, double ymin, double ymax);
    static void newTH2(TH2*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title, int nxbins, const double* xedges, int nybins, const double* ymin);

    static void newCounter(TH1*& histo, EL::Worker*& worker, HistoType type, std::string name, std::string title) {
      newTH1(histo, worker, type, "count_" + name, title, 1, -0.5, 0.5);
    }


    enum class HistogramType {Eta, Pt, AbsEta, N};

    template <typename ObjectType>
    static void fill_histo_type(std::vector<ObjectType const *> const & objects, TH1 * h, Utilities::HistogramType type, double weight) {
        switch ( type ) {

        default :
        case HistogramType::Eta :
          for (auto const * ob : objects) {
            h->Fill(ob->eta(), weight);
          }
          break;

        case HistogramType::Pt :
          for (auto const * ob : objects) {
            h->Fill(ob->pt() * 0.001, weight);
          }
          break;

        case HistogramType::AbsEta :
          for (auto const * ob : objects) {
            h->Fill(std::abs(ob->eta()), weight);
          }
          break;

        case HistogramType::N :
          h->Fill(objects.size(), weight);
          break;
        }
    }



  };






  namespace UtilitiesFunctions {


    std::vector<float> vectorMultiplication(const std::vector<float>& v1, const std::vector<float>& v2);
      

    /// The mathematical sign function
    /// Returns +1 if val is positive
    /// Returns -1 if val is negative
    /// Returns  0 if val is zero
    template <typename T>
    int sign(T val) {
        return (T(0) < val) - (val < T(0));
    }



    bool do_nominal(std::vector<CP::SystematicSet> const & systematics);



    /// Delete pointee object (if exists) and set pointer to null
    template <typename T>
    void delete_and_null(T * & pointer) {
      if (pointer) {
        delete pointer;
      }
      pointer = 0;
    }




    /// Multiply elements of standard vector by number
    template <typename ElementType, typename ScalarType>
    std::vector<ElementType> scale(std::vector<ElementType> const & vect, ScalarType scalar) {
      std::vector<ElementType> out;
      out.reserve(vect.size());

      for (auto const & elem : vect) {
        auto const scaled = elem * scalar;
        out.push_back(scaled);
      }

      return out;
    }


    // Get total scalar pT sum for a vector of objects
    template <typename ObjectType>
    float scalar_pT_sum(std::vector<ObjectType const *> const & obs) {
      float pT = 0.0;
      for (auto const * ob : obs) {
        pT += ob->pt();
      }
      return pT;
    }



    template <typename ObjectType>
    float scalar_pT_sum(const std::vector<ObjectType *> & obs) {
      float pT = 0.0;
      for (auto const * ob : obs) {
        pT += ob->pt();
      }
      return pT;
    }



    // Get total invariant mass out of vectors of IParticles
    template <typename ObjectType>
    float allLeptonMass(std::vector<ObjectType const *> const & obs) {
      TLorentzVector tv_tot(0.0, 0.0, 0.0, 0.0);
      for (auto const * ob : obs) {
        tv_tot += ob->p4();
      }
      return tv_tot.M();
    }


    // Get invariant mass of two leptons
    template <typename ObjectType>
    float twoObjectsMass(ObjectType const *ob1, ObjectType const *ob2) {
      TLorentzVector tv_tot(0.0, 0.0, 0.0, 0.0);
      tv_tot = ob1->p4() + ob2->p4(); 
      return tv_tot.M();
    }


    // Get invariant mass of three leptons
    template <typename ObjectType>
    float threeLeptonMass(ObjectType const *ob1, ObjectType const *ob2, ObjectType const *ob3) {
      TLorentzVector tv_tot(0.0, 0.0, 0.0, 0.0);
      tv_tot = ob1->p4() + ob2->p4() + ob3->p4(); 
      return tv_tot.M();
    }


    template <typename ObjectType>
    float bestZmass(std::vector<ObjectType const *> const & obs, std::vector<float> vlepton_ID) {
      float bestZmass(0.);
      const unsigned int v_size = obs.size();
      std::vector<TLorentzVector> tv_leps; tv_leps.reserve(v_size+1); 
      for (auto const * ob : obs) tv_leps.push_back(ob->p4());
      // select best Z
      for (unsigned int i=0; i<v_size; i++) {
        for (unsigned int j=0; j<v_size; j++) {
          if   (i>=j || (vlepton_ID[i]+vlepton_ID[j]!=0)) continue;
          else {
	    bestZmass = ( (abs(91.2-bestZmass/1000.)>abs(91.2-((tv_leps[i]+tv_leps[j]).M())/1000.)) || bestZmass==0 ) ? (tv_leps[i]+tv_leps[j]).M() : bestZmass;
          }
        }
      }
      //std::cout << "bestZmass:		" << bestZmass/1000. << std::endl;

      return bestZmass;
    }


    template <typename ObjectType>
    std::vector<unsigned int> get_bestZ_ind(std::vector<ObjectType const *> const & obs, std::vector<float> vlepton_ID) {
      float bestZmass(0.);
      std::vector<unsigned int> myBestZ_ind{10,10};
      const unsigned int v_size = obs.size();
      std::vector<TLorentzVector> tv_leps; tv_leps.reserve(v_size+1);
      for (auto const * ob : obs) tv_leps.push_back(ob->p4());
      // select best Z
      for (unsigned int i=0; i<v_size; i++) {
        for (unsigned int j=0; j<v_size; j++) {
          if   (i>=j || (vlepton_ID[i]+vlepton_ID[j]!=0)) continue;
          else {
            bool newZ = ( (abs(91.2-bestZmass/1000.)>abs(91.2-((tv_leps[i]+tv_leps[j]).M())/1000.)) || bestZmass==0 );
            if ( newZ ) { myBestZ_ind[0]=i; myBestZ_ind[1]=j; };
            bestZmass 	= ( newZ ) ? (tv_leps[i]+tv_leps[j]).M() : bestZmass;
          }
        }
      }
      //std::cout << "Z indices:	" << myBestZ_ind[0] << "	and 	" << myBestZ_ind[1] << std::endl;
 
      return myBestZ_ind;
    }


    template <typename ObjectType>
    float otherBestZmass(std::vector<ObjectType const *> const & obs, std::vector<float> vlepton_ID) {
      std::vector<unsigned int> bestZ_ind = get_bestZ_ind(obs,vlepton_ID); 
      if (bestZ_ind[0]==10 || bestZ_ind[1]==10) return 0;
      unsigned int bestZ_other_ind[]	= {10,10};
      const unsigned int v_size = obs.size();
      std::vector<TLorentzVector> tv_leps; tv_leps.reserve(v_size+1);
      for (auto const * ob : obs) tv_leps.push_back(ob->p4());
      for (unsigned int i=0; i<v_size; i++) { 
	if (i==bestZ_ind[0] || i==bestZ_ind[1]) continue;
        if (bestZ_other_ind[0]==10) 		bestZ_other_ind[0]=i;
        else					{ bestZ_other_ind[1]=i; break; }
      }
      float BestZmass_other = (tv_leps[bestZ_other_ind[0]]+tv_leps[bestZ_other_ind[1]]).M();
      //std::cout << "otherBestZmass:	" << BestZmass_other/1000. << std::endl;
      //std::cout << "bestZmass ind:	" << bestZ_ind[0] << "	and	" << bestZ_ind[1] << std::endl;
      //std::cout << "otherBestZmass ind:	" << bestZ_other_ind[0] << "	and	" << bestZ_other_ind[1] << std::endl;
      
      return BestZmass_other; 
    }



    /// Get vector sum of transverse momenta of objects in vector @obs
    template <typename ObjectType>
    TLorentzVector vector_momentum_sum(std::vector<ObjectType const *> const & obs) {
      TLorentzVector p(0.0, 0.0, 0.0, 0.0);
      for (auto const * ob : obs) {
        p += ob->p4();
      }
      return p;
    }



    /// Find index of object with largest transverse momentum of objects in vector @obs
    /// Return -1 if vector @obs is empty
    /// (If two objects have *exactly* the same pT [~impossible in practice...], the lower index is returned)
    template <typename ObjectType>
    std::size_t index_of_leading_pT_object(std::vector<ObjectType const *> const & obs) {
      std::size_t i = -1;
      double current_pT_max = -1.0;

      for (std::size_t k = 0; k < obs.size(); ++k) {
        if (obs.at(k)->pt() > current_pT_max) {
          i = k;
          current_pT_max = obs.at(k)->pt();
        }
      }

      return i;
    }


    //function to cast specific particles to generic IParticle type
    template <typename T>
    std::vector<xAOD::IParticle const *> to_iparticle(std::vector<T const *> const & tvect) {
      std::vector<xAOD::IParticle const *> out = {};

      for (auto const & t : tvect) {
        out.push_back(static_cast<xAOD::IParticle const *>(t));
      }

      return out;
    }
    //function to combine vectors of 2 objects, returning a vector of IParticles containing both
    template <typename A, typename B>
    std::vector<xAOD::IParticle const *> combine(std::vector<A *> & avect, std::vector<B *> & bvect) {
    //std::vector<xAOD::IParticle const *> combine(std::vector<A const *> const & avect, std::vector<B const *> const & bvect) 

      std::vector<xAOD::IParticle const *> out = {};
      for(auto & a: avect) out.push_back( a );
      for(auto & b: bvect) out.push_back( b );

      //for(auto & l: out) std::cout << "pushed lep pt	:" << l->pt() << "	eta:	" << l->eta() << std::endl;

      //auto const avect_cast = to_iparticle(avect);
      //auto const bvect_cast = to_iparticle(bvect);

      //out.insert(out.end(), avect_cast.begin(), avect_cast.end());
      //out.insert(out.end(), bvect_cast.begin(), bvect_cast.end());

      return out;
    }
  
  }


}
