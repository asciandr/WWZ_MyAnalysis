// -*- c++ -*-
/// Functions to select jets.
/// Most are implemented as templates, meaning that they work with different data types of jets.

#pragma once

#include "xAODJet/JetContainer.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "JetMomentTools/JetForwardJvtTool.h"

#include <vector>
#include <algorithm>

#include "WWZanalysis/AbsEtaRange.h"


namespace WWZanalysis {

  namespace JetSelection {

    /// From an xAOD::JetContainer, make a vector of pointers to the objects it contains
    std::vector<xAOD::Jet const *> make_std_vector(xAOD::JetContainer const * in);

    /// Vector subtraction based on std_difference (a - b = out)
    std::vector<xAOD::Jet const *> sub_std_vectors(std::vector<const xAOD::Jet *> a, std::vector<const xAOD::Jet *> b);

    /// Jet vertex tagging (JVT) selection
    /// Done to reject pileup jets
    std::vector<xAOD::Jet const *> select_vertex_tagged(std::vector<xAOD::Jet const *> const & in, double JVT_min, JetVertexTaggerTool const * tool);

    /// Decorate the jets with updated jet vertex tagging (JVT) value
    void decorate_with_updated_JVT(xAOD::JetContainer const * in,  JetVertexTaggerTool const * tool);

    /// Forward jet vertex tagging (fJVT) selection
    /// Done to reject pileup jets
    /// The algorithm requires that the relevant dataset contains the MET_Track container.
    /// The fJVT calculation assumes the jet container has already been calibrated and that the contained jets have calibrated JVT values.
    std::vector<xAOD::Jet const *> select_fwd_vertex_tagged(std::vector<const xAOD::Jet *> const &in, JetForwardJvtTool const * tool);

    /// Decorate the jets with forward jet vertex tagging (fJVT) value
    void decorate_with_fwd_JVT(xAOD::JetContainer *in, JetForwardJvtTool const * tool);

    enum class Type {HardScatter, Pileup, Jvt};

    /// TEST openc see: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ForwardJVT
    std::vector<xAOD::Jet const *> TEST_select_truth_fwd_vertex_tagged(std::vector<xAOD::Jet const *> const & in, Type type);
    void TEST_decorate_with_truth_fwd_JVT(xAOD::JetContainer * in, xAOD::JetContainer const * raw , JetForwardJvtTool const * tool);

    // Return jets falling into the given range of pseudorapidity
    // The jets' uncalibrated ("detector") eta is used, defined at the momentum scale used for the jet finding (e.g. EM scale)
    // See https://svnweb.cern.ch/trac/atlasoff/browser/Event/xAOD/xAODJet/trunk/xAODJet/JetTypes.h
    std::vector<xAOD::Jet const *> select_detector_eta(std::vector<xAOD::Jet const *> const & in, AbsEtaRange const & aer);

    /// gap and JVTs failed jet selection
    std::vector<xAOD::Jet const *> get_JVTs_failed_and_gap_jets(std::vector<const xAOD::Jet *> const & in, std::vector<xAOD::Jet const *> const & central, std::vector<xAOD::Jet const *> const & forward);

  }

}
