// -*- c++ -*-
/// Class for histogramming easily in the presence multiple signal channels as well as systematic variations.
/// A wrapped histogram here is actually a map whose values are histograms and whose keys are pairs of channel name
/// and systematic variation name.
///
/// The channels are hard-coded. They are: ElectronChannel, MuonChannel, MixedChannel, AllChannelsCombined.
///
/// The systematic uncertainty names must be provided by the user.

#pragma once

#include "WWZanalysis/SignalChannel.h"

#include "TH1.h"
#include "TH2.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicVariation.h"
#include "EventLoop/Worker.h"

#include <functional>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>



namespace WWZanalysis {

  typedef std::vector<TH1 *> histogram_per_weight;
  typedef std::vector<TH2 *> histogram2d_per_weight;

  enum class HistoType { RECO, TRUTH, MATCHED, CONTROL, UNFOLDING };



  std::unordered_map<int, std::string> const histo_type_directories = {
      {static_cast<int>(HistoType::RECO), "Reco/"},
      {static_cast<int>(HistoType::TRUTH), "Truth/"},
      {static_cast<int>(HistoType::MATCHED), "Matched/"},
      {static_cast<int>(HistoType::CONTROL), "Control/"},
      {static_cast<int>(HistoType::UNFOLDING), "Unfolding/"}
  };



  /// Equality operator for std::pair<int, std::string>
  /// Needed to be able to use such pairs as map keys
  bool operator== (std::pair<int, std::string> const & a, std::pair<int, std::string> const & b);



  struct hash_for_pair_keys {
    std::size_t operator() (std::pair<int, std::string> const & p) const;
  };



  namespace HistogramUtils {

    /// Construct uniformly spaced bin edges
    std::vector<double> uniform_binedges(int nbins, double xmin, double xmax);

  }



  class WrappedTH1 {

  public:

    /// Create one histogram for each systematic variation (including nominal, i.e. no variation) and put them in the map
    /// Histograms created this way have custom-spaced bins, with edges provided as a std::vector.
    /// Returns a vector of the paths of the created histograms (mainly for debugging)
    std::vector<std::string> create_histos(EL::Worker *& worker, std::vector<CP::SystematicSet> const & systematics_list, std::size_t number_of_PDF_weights, HistoType type, std::string name, std::string title, std::vector<double> const & binedges);

    /// Create one histogram for each systematic variation (including nominal, i.e. no variation) and put them in the map
    /// Histograms created this way have equally-spaced bins
    /// Returns a vector of the paths of the created histograms (mainly for debugging)
    std::vector<std::string> create_histos(EL::Worker *& worker, std::vector<CP::SystematicSet> const & systematics_list, std::size_t number_of_PDF_weights, HistoType type, std::string name, std::string title, int nbins, double xmin, double xmax);

    /// Create one counter (= histogram with one bin centered around 0) for each systematic variation (including nominal, i.e. no variation) and put them in the map
    /// This is a special case of the create_histos(), where the histograms have only one bin
    /// Returns a vector of the paths of the created histograms (mainly for debugging)
    std::vector<std::string> create_counters(EL::Worker *& worker, std::vector<CP::SystematicSet> const & systematics_list, std::size_t number_of_PDF_weights, HistoType type, std::string name, std::string title);

    /// Fill histogram for the desired signal channel and systematic variation
    /// The default channel is 'Any', which simply includes everything independently of channel selection
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void fill(std::string const & variation, double value, std::vector<double> const & weights, SignalChannel::Type channel = SignalChannel::Type::Any, bool ignore_missing=true);

    /// Fill histogram for the desired signal channel and systematic variation
    /// The default channel is 'Any', which simply includes everything independently of channel selection
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void fill(std::string const & variation, double value, double weight, SignalChannel::Type channel = SignalChannel::Type::Any, bool ignore_missing=true);

    /// Fill histogram for the nominal systematic "variation", ignoring any channel selection
    /// Useful shorthand, because many histogram sets only have a nominal histogram
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void fill_nominal(double value, std::vector<double> const & weights, SignalChannel::Type channel = SignalChannel::Type::Any);

    /// Fill histogram for the nominal systematic "variation", ignoring any channel selection
    /// Useful shorthand, because many histogram sets only have a nominal histogram
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void fill_nominal(double value, double weight, SignalChannel::Type channel = SignalChannel::Type::Any);

    /// A variation of fill() that's useful for counters. Fill the histogram at value = 0.0.
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void increment(std::string const & variation, std::vector<double> const & weights, SignalChannel::Type channel = SignalChannel::Type::Any, bool ignore_missing=true);

    /// A variation of fill() with a single weight that's useful for counters. Fill the histogram at value = 0.0.
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void increment(std::string const & variation, double weight, SignalChannel::Type channel = SignalChannel::Type::Any, bool ignore_missing=true);

    /// Increment counter for the nominal systematic "variation".
    /// Useful shorthand, because many counters sets only have a nominal histogram
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void increment_nominal(std::vector<double> const & weights, SignalChannel::Type channel = SignalChannel::Type::Any);

    /// Increment counter with a single weight for the nominal systematic "variation".
    /// Useful shorthand, because many counters sets only have a nominal histogram
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void increment_nominal(double weight, SignalChannel::Type channel = SignalChannel::Type::Any);

    /// Get vector of the bin edges of the wrapped histogram.
    /// All are lower bin edges, except the last entry, which is the upper edge of the last bin.
    /// So for a histogram with N bins, there are N + 1 edges.
    /// The underflow and overflow bins are ignored.
    std::vector<double> get_bin_edges() const;

    std::unordered_map<std::pair<int, std::string>, histogram_per_weight, hash_for_pair_keys> histograms_by_channel_and_systematic;

  private:

    void fill_for_channel(histogram_per_weight & histograms, double value, std::vector<double> const & weights);

    histogram_per_weight & get_histograms(SignalChannel::Type channel, std::string const & variation, bool ignore_missing, histogram_per_weight & dummy);

    histogram_per_weight const & get_histograms_const(SignalChannel::Type channel, std::string const & variation) const;

  };







  class WrappedTH2 {

  public:

    /// Create one histogram for each systematic variation (including nominal, i.e. no variation) and put them in the map
    /// Histograms created this way have custom-spaced bins, with edges provided as a std::vector.
    /// Returns a vector of the paths of the created histograms (mainly for debugging)
    std::vector<std::string> create_histos(EL::Worker *& worker, std::vector<CP::SystematicSet> const & systematics_list, std::size_t number_of_PDF_weights, HistoType type, std::string name, std::string title, std::vector<double> const & binedgesx, std::vector<double> const & binedgesy);

    /// Create one histogram for each systematic variation (including nominal, i.e. no variation) and put them in the map
    /// Histograms created this way have equally-spaced bins
    /// Returns a vector of the paths of the created histograms (mainly for debugging)
    std::vector<std::string> create_histos(EL::Worker *& worker, std::vector<CP::SystematicSet> const & systematics_list, std::size_t number_of_PDF_weights, HistoType type, std::string name, std::string title, int nbinsx, double xmin, double xmax, int nbinsy, double ymin, double ymax);

    /// Fill histogram for the desired signal channel and systematic variation
    /// The default channel is 'Any', which simply includes everything independently of channel selection
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void fill(std::string const & variation, double x, double y, std::vector<double> const & weights, SignalChannel::Type channel = SignalChannel::Type::Any, bool ignore_missing=true);

    /// Fill histogram for the desired signal channel and systematic variation
    /// The default channel is 'Any', which simply includes everything independently of channel selection
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void fill(std::string const & variation, double x, double y, double weight, SignalChannel::Type channel = SignalChannel::Type::Any, bool ignore_missing=true);

    /// Fill histogram for the nominal systematic "variation", ignoring any channel selection
    /// Useful shorthand, because many histogram sets only have a nominal histogram
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void fill_nominal(double x, double y, std::vector<double> const & weights, SignalChannel::Type channel = SignalChannel::Type::Any);

    /// Fill histogram for the nominal systematic "variation", ignoring any channel selection
    /// Useful shorthand, because many histogram sets only have a nominal histogram
    ///
    /// IMPORTANT: If another channel is chosen, the 'Any' histograms are still FILLED AS WELL!
    void fill_nominal(double x, double y, double weight, SignalChannel::Type channel = SignalChannel::Type::Any);

    std::unordered_map<std::pair<int, std::string>, histogram2d_per_weight, hash_for_pair_keys> histograms_by_channel_and_systematic;

  private:

    void fill_for_channel(histogram2d_per_weight & histograms, double xvalue, double yvalue, std::vector<double> const & weights);

    histogram2d_per_weight & get_histograms(SignalChannel::Type channel, std::string const & variation, bool ignore_missing, histogram2d_per_weight & dummy);

    histogram2d_per_weight const & get_histograms_const(SignalChannel::Type channel, std::string const & variation) const;

  };



}
