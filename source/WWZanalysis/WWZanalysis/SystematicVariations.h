// -*- c++ -*-
/// Provide functions to handle systematic variations, making it easy to to iterate over them and apply them.

#pragma once

#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"

#include "xAODMuon/Muon.h"
#include "xAODEgamma/Electron.h"

#include <iostream>
#include <string>
#include <vector>

namespace WWZanalysis {

  namespace SystematicVariations {


    // Take tool, CP::SystematicVariation string and UP+DOWN doubles to be set
    // xAOD::Electron
    template <typename ToolType>
    std::vector<double> return_lepton_SF_variations(ToolType *& tool, xAOD::Electron *lepton, std::string syst_name, std::vector<double> fillme) {
      bool success(false);
      // UP
      CP::SystematicSet tmpSet_UP; tmpSet_UP.insert(CP::SystematicVariation(syst_name,1));
      success =  ( tool->applySystematicVariation(tmpSet_UP) == CP::SystematicCode::Ok );
      tool->getEfficiencyScaleFactor(*lepton, fillme[0]);
      // DOWN
      CP::SystematicSet tmpSet_DOWN; tmpSet_DOWN.insert(CP::SystematicVariation(syst_name,-1));
      success *= ( tool->applySystematicVariation(tmpSet_DOWN) == CP::SystematicCode::Ok );
      tool->getEfficiencyScaleFactor(*lepton, fillme[1]);
      // RESET TOOL
      tool->applySystematicVariation(CP::SystematicSet());

      if (! success) {
        std::cout << "Warning: problem in applying systematic variation \"" << syst_name << "\" to tool!" << std::endl;
      }
 
      return fillme;
    }

    // Take tool, CP::SystematicVariation string and UP+DOWN doubles to be set
    // xAOD::Muon
    template <typename ToolType>
    std::vector<float> return_lepton_SF_variations(ToolType *& tool, xAOD::Muon *lepton, std::string syst_name, std::vector<float> fillme) {
      bool success(false);
      // UP
      CP::SystematicSet tmpSet_UP; tmpSet_UP.insert(CP::SystematicVariation(syst_name,1));
      success =  ( tool->applySystematicVariation(tmpSet_UP) == CP::SystematicCode::Ok );
      tool->getEfficiencyScaleFactor(*lepton, fillme[0]);
      // DOWN
      CP::SystematicSet tmpSet_DOWN; tmpSet_DOWN.insert(CP::SystematicVariation(syst_name,-1));
      success *= ( tool->applySystematicVariation(tmpSet_DOWN) == CP::SystematicCode::Ok );
      tool->getEfficiencyScaleFactor(*lepton, fillme[1]);
      // RESET TOOL
      tool->applySystematicVariation(CP::SystematicSet());

      if (! success) {
        std::cout << "Warning: problem in applying systematic variation \"" << syst_name << "\" to tool!" << std::endl;
      }
 
      return fillme;
    }



    // Take vector of variation names, return vector of systematic-set objects.
    // The nominal setting (no variation) is always included (as the first element).
    std::vector<CP::SystematicSet> make_systematic_variations_vector(std::vector<std::string> const & variation_names);



    // Apply systematic variation to a tool. Returns true if successful.
    // The tool needs to have an interface for applying variations that follows the Combined Performance guidelines.
    template <typename ToolType>
    bool apply(CP::SystematicSet const & variation, ToolType *& tool) {

      /// If no tool is given (pointer == null), do not apply any variation and treat as success
      if (! tool) return true;

      /// Try to apply the variation to the tool
      bool const success = (tool->applySystematicVariation(variation) == CP::SystematicCode::Ok);

      /// Warn about any problems
      if (! success) {
        std::cout << "Warning: problem in applying systematic variation \"" << variation.name() << "\" to tool!" << std::endl;
      }

      return success;
    }



  }

}
