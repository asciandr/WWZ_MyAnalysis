// -*- c++ -*-
/// Functions to select electrons.
/// Most are implemented as templates, meaning that they work with different data types of electrons.

#pragma once

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Egamma.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"

#include "WWZanalysis/Utilities.h"
#include "WWZanalysis/Config.h"
#include "WWZanalysis/BeamPosition.h"
#include "WWZanalysis/GenericObjectSelection.h"
#include "IsolationSelection/IsolationSelectionTool.h"

namespace WWZanalysis {
  namespace ElectronSelection {



    /// From an xAOD::ElectronContainer, make a vector of pointers to the objects it contains
    std::vector<xAOD::Electron const *> make_std_vector(xAOD::ElectronContainer const * in);



    /// Electron eta acceptance selection (cut on *electromagnetic calorimeter cluster* pseudorapidity!)
    std::vector<xAOD::Electron const *> select_in_eta_acceptance(std::vector<xAOD::Electron const *> const & in, double cluster_abs_eta_max);



    /// Electron very very loose, silicon-hit-based identification
    /// CAUTION: The electron ID tool must be configured at the Loose working point; it's the USER'S RESPONSIBILITY to make sure of this!
    /// (The reason the user has to do it is that there seems no easy & working way to obtain the config from the tool! :( )
    /// If tool pointer is nullptr, returns the input (i.e. selects all)
    std::vector<xAOD::Electron const *> select_silicon_hit_id(std::vector<xAOD::Electron const *> const & in, AsgElectronLikelihoodTool * electron_id_tool);

    /// Electron isolation 
    /// If tool pointer is nullptr, returns the input (i.e. selects all)
    std::vector<xAOD::Electron const *> select_isolated(std::vector<xAOD::Electron const *> const & in, CP::IsolationSelectionTool * electron_iso_tool);




    /// Electron object quality selection
    template <typename ObjectType>
    std::vector<ObjectType const *> select_good_object_quality(std::vector<ObjectType const *> const & in, uint32_t const qualifier) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        if (ob->isGoodOQ(qualifier)) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Transverse impact parameter (d0) selection
    template <typename ObjectType>
    std::vector<ObjectType const *> select_d0_smaller(std::vector<ObjectType const *> const & in, double const d0_max) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        auto const * track = ob->trackParticle();
        auto const d0 = track->d0();

        if (d0 < d0_max) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Longitudinal impact parameter (abs[z0 * sin(theta)]) selection
    /// Primary-vertex information is needed, because the distance z0 is measured with respect to the primary vertex
    template <typename ObjectType>
    std::vector<ObjectType const *> select_abs_z0sintheta_smaller(std::vector<ObjectType const *> const & in, xAOD::Vertex const * primary_vertex, double const abs_z0sintheta_max) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        auto const * track = ob->trackParticle();
        auto const abs_z0sintheta = GenericObjectSelection::calculate_abs_z0sintheta(track, primary_vertex);

        if (abs_z0sintheta < abs_z0sintheta_max) {
          out.push_back(ob);
        }
      }

      return out;
    }


    /// Transverse impact parameter significance (d0sig) selection
    template <typename ObjectType>
    std::vector<ObjectType const *> select_d0_significance_smaller(std::vector<ObjectType const *> const & in, BeamPosition const & beampos, double const d0sig_max) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        auto const * track = ob->trackParticle();

        /// Note: take absolute value!
        double const d0sig = std::abs(xAOD::TrackingHelpers::d0significance(track, beampos.sigma_x, beampos.sigma_y, beampos.sigma_xy));

        if (d0sig < d0sig_max) {
          out.push_back(ob);
        }
      }

      return out;
    }



  }
}
