// -*- c++ -*-
/// Helper functions to retrieve data from xAODs and create new containers.

#pragma once

#include "xAODRootAccess/TEvent.h" // for TEvent
#include <xAODRootAccess/TStore.h> // for TStore

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

namespace WWZanalysis {
  namespace EventData {


    //std::string retrieve_xsec(unsigned int mcChannelNumber) 
    float retrieve_xsec(TString xsecFile, unsigned int mcChannelNumber) { 
      std::stringstream ss_mcChannelNumber; ss_mcChannelNumber << mcChannelNumber;
      std::string str_mcChannelNumber = ss_mcChannelNumber.str();
      //std::cout << "mcChannelNumber:		" << str_mcChannelNumber << std::endl;
      TString xsec_file = xsecFile; 
      std::ifstream ifs(xsec_file.Data());
      std::string aLine;
      std::vector<std::string> tokens; // Create vector to hold piecies
      if (ifs.fail()) { std::cerr << "Failure when reading xsec file: " << xsec_file << std::endl; return 0; }
      else  {
        while (getline(ifs,aLine)) {
          if ( (aLine.find("#")==0 || aLine.find(str_mcChannelNumber)==std::string::npos) ) continue;
          else {
            std::string buf; // Have a buffer string
            std::stringstream ss(aLine); // Insert the string into a stream 
            while (ss >> buf) tokens.push_back(buf);
          }
        }
      }    
      if (tokens.size()!=6) { std::cerr << "Check xsec! tokens.size()!=6, it can be buggy!!!" << std::endl; return 0; }
      //std::cout << "tokens size:	" << tokens.size() << std::endl;
      //for(unsigned int i=0; i<tokens.size(); i++) std::cout << "Split vector:		" << tokens[i] << std::endl;
      // xsec=tokens[2]; k-factor=tokens[3]; filt. eff=tokens[4]; (rel.unc.=tokens[5])
      float nom_xsec(std::stof(tokens[2])), k_fact(std::stof(tokens[3])), f_eff(std::stof(tokens[4]));
      //std::cout << "nom_xsec:	" << nom_xsec <<"	k_fact:		" << k_fact << "	f_eff:		" << f_eff << std::endl;
      float tot_xsec=nom_xsec*k_fact*f_eff; 
      return tot_xsec;
    }


    /// Function to read a collection from an xAOD event
    /// @param target : pointer that should be made to point at the desired collection
    /// @param name   : name of the collection we want to read
    /// @param evt    : xAOD event we want to read the data from
    template <typename Container>
    bool retrieve(Container const *& target, std::string const & name, xAOD::TEvent * const & evt) {

      /// If this is successful, the pointer 'target' will point to the data identified by 'name':
      if (! evt->retrieve(target, name).isSuccess()) {
        auto const error_message = "Failed to retrieve collection \"" + name + "\"!";
        Error("EventData::retrieve()", error_message.c_str());
        return false;
      }

      return true;
    }



//    // Create new containers for the cleaned, calibrated, and corrected objects and fill them
//    // The new containers are (mostly?) deep copies and are all owned by TStore, so we don't need to delete them ourselves.
//    // Very useful info: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT#Creating_modified_container_s [sic!]
//    // What's in the store can be seen by doing store->print();
//    template <typename Container>
//    bool create(std::string const & name, Container *& target, Container *& aux, xAOD::TStore *& store) {
//
//      xAOD::ElectronContainer* centralRecoElectrons = new xAOD::ElectronContainer();
//      if (!store->record(centralRecoElectrons, "centralRecoElectrons")) {
//        return EL::StatusCode::FAILURE;
//      }
//      xAOD::ElectronAuxContainer* centralRecoElectronsAux = new xAOD::ElectronAuxContainer();
//      if (!store->record(centralRecoElectronsAux, "centralRecoElectronsAux.")) {
//        return EL::StatusCode::FAILURE;
//      } // < note full stop (.) in name
//      centralRecoElectrons->setStore(centralRecoElectronsAux);
//
//
//    }



    /// Create new object containers
    /// The new containers are (mostly?) deep copies and are all owned by TStore, so we don't need to delete them ourselves.
    /// Very useful info:
    /// https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInROOT#Creating_modified_container_s [sic!]
    /// What's in the store can be seen by doing store->print();
    template <typename Container, typename AuxContainer>
    Container * create(std::string const & name, xAOD::TStore *& store) {

      /// Try to make and record target container
      Container * target = new Container();
      if (! store->record(target, name)) {
        auto const error_message = "Failed to record new container called \"" + name + "\"to TStore! See file WWZanalysis/EventData.h";
        throw std::logic_error(error_message);
      }

      /// Try to make and record auxiliary container for the target container
      AuxContainer * aux = new AuxContainer();
      if (! store->record(aux, name + "Aux.")) { // < note full stop (.) in name
        auto const error_message = "Failed to record new container called \"" + name + "Aux.\" to TStore! See file WWZanalysis/EventData.h";
        throw std::logic_error(error_message);
      }

      /// Associate auxiliary container with target (as its store)
      target->setStore(aux);

      return target;
    }



    /// Make copies of objects in a vector and put them into a container
    /// Return pointer to that container (for convenience; not really needed, as a non-const reference to the container is
    /// passed as one of the arguments)
    /// (Needed for interfaces to tools that require xAOD containers)
    ///
    /// NOTE: the order of the input objects is the preserved in the container!
    template <typename ObjectType, typename Container>
    Container * to_container(std::vector<ObjectType const *> const & in, Container *& out) {

      /// Go over the objects in the input vector, make non-const copies of them, and add them to the container
      for (auto const * ob : in) {
        ObjectType * ob_nonconst = new ObjectType();
        ob_nonconst->makePrivateStore(* ob);
        * ob_nonconst = * ob;
        out->push_back(ob_nonconst);
      }

      return out;
    }



    /// Decorate a container with instances of type Decor, initialised to @init@
    /// If the decoration exists already (of the same type), I *think* it will get overwritten with @init@
    ///
    /// NOTE: for some reason, xAODs want bools to be stored as chars, so (static-) cast @init@ to a char if it's a bool...
    template <typename Container, typename Decor>
    Container * decorate_all(Container * out, std::string const & decorname, Decor init) {

      for (auto const & ob : * out) {
        if (! ob) throw std::logic_error("Error in EventData::decorate(): object pointer is null!");
        ob->template auxdecor<Decor>(decorname) = init;
      }

      return out;
    }




  }
}
