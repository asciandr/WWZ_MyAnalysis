// -*- c++ -*-
/// Get trigger scale factor weights in Monte Carlo

#pragma once

#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "xAODMuon/Muon.h"
#include "xAODEgamma/Electron.h"

#include <string>
#include <vector>

namespace WWZanalysis {
  namespace TriggerScaleFactors {



    /// If the tool pointer is null, return 1.0
    /// The *selected* muons should be passed to this function
    double get_muon_trigger_weight(CP::MuonTriggerScaleFactors * tool, std::string const & trigger_name, const std::vector<xAOD::Muon *> & muons);
    //double get_muon_trigger_weight(CP::MuonTriggerScaleFactors * tool, std::string const & trigger_name, std::vector<xAOD::Muon const *> const & muons);


    /// If any of the tool pointers is null, return 1.0
    /// The *selected* electrons should be passed to this function
    double get_electron_trigger_weight(AsgElectronEfficiencyCorrectionTool * mceff_tool, AsgElectronEfficiencyCorrectionTool * sf_tool, std::vector<xAOD::Electron const *> const & electrons);



  }
}
