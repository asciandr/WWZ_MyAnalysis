// -*- c++ -*-
/// Get sum of weights of events that were originally present in the sample, before skimming (derivation).
/// Needed to calculate skimming efficiency, which is in turn needed to normalise samples to cross section.
///
/// See e.g.: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#How_to_access_EventBookkeeper_in


#pragma once

#include "xAODRootAccess/TEvent.h"



namespace WWZanalysis {

  namespace SkimmingInfo {

    /// Get the sum of weights of all the events that were in the sample before skimming/derivation.
    /// Takes one event from the sample as parameter; the info is stored in each event.
    double sum_of_weights_before_skimming(xAOD::TEvent * const some_event_in_the_sample);

    /// Do it for alternative R and F scales and PDF
    double all_sum_of_weights_before_skimming(xAOD::TEvent * const some_event_in_the_sample, std::string cbkName);

    /// Get the number of all the events that were in the sample before skimming/derivation.
    /// Takes one event from the sample as parameter; the info is stored in each event.
    /// WARNING: ignores event weights!
    uint64_t number_of_events_before_skimming(xAOD::TEvent * const some_event_in_the_sample);


  }

}
