// -*- c++ -*-
/// Functions to select muons.
/// Most are implemented as templates, meaning that they work with different data types of muons.

#pragma once

#include "xAODRootAccess/TEvent.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "xAODTracking/Vertex.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "IsolationSelection/IsolationSelectionTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"

#include "WWZanalysis/ScaleFactors.h"
#include "WWZanalysis/Utilities.h"
#include "WWZanalysis/BeamPosition.h"
#include "WWZanalysis/GenericObjectSelection.h"

#include <cmath>
#include <vector>
#include <string>

namespace WWZanalysis {

  namespace MuonSelection {


    /// Get muons of a given muon type
    std::vector<xAOD::Muon const *> filter_by_type(xAOD::Muon::MuonType type, std::vector<xAOD::Muon const *> in);



    /// Convert muon selection working point from enumerated type to string
    /// Some tools need the string. This function automatically converts to prevent bugs stemming from conflicting configuration.
    xAOD::Muon::Quality to_enumerated_quality(std::string const & quality_str);



    /// From an xAOD::MuonContainer, make a vector of pointers to the objects it contains
    std::vector<xAOD::Muon const *> make_std_vector(xAOD::MuonContainer const * in);



    /// Transverse momentum selection that is only applied to muons of the specified type
    /// Muons of all other types automatically pass the selection
    template <typename ObjectType>
    std::vector<ObjectType const *> select_pt_greater_typeaware(std::vector<ObjectType const *> const & in, double const pt_min, xAOD::Muon::MuonType type) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        if (ob->muonType() == type && ob->pt() * 0.001 < pt_min) {
          continue;
        }
        out.push_back(ob);
      }

      return out;
    }



    /// Basic selection of muons using tool by Combined Performance group
    void apply_basic_selection(xAOD::MuonContainer * out, xAOD::MuonContainer const * in, CP::MuonSelectionTool const * selection_tool);



    /// Longitudinal impact parameter (abs[z0 * sin(theta)]) selection for muons, i.e. not applied for standalone muons
    /// Primary-vertex information is needed, because the distance z0 is measured with respect to the primary vertex
    template <typename ObjectType>
    std::vector<ObjectType const *> select_abs_z0sintheta_smaller(std::vector<ObjectType const *> const & in, xAOD::Vertex const * primary_vertex, double const abs_z0sintheta_max, bool disable_for_standalone_muons) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        auto const * track = ob->primaryTrackParticle();
        auto const abs_z0sintheta = GenericObjectSelection::calculate_abs_z0sintheta(track, primary_vertex);

        if (abs_z0sintheta < abs_z0sintheta_max || (disable_for_standalone_muons && ob->muonType() == xAOD::Muon::MuonType::MuonStandAlone)) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Cosmic muon rejection based on transverse impact parameter (d0)
    template <typename ObjectType>
    std::vector<ObjectType const *> select_not_cosmic_muons(std::vector<ObjectType const *> const & in, double const d0_max, bool disable_for_standalone_muons) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        auto const * track = ob->primaryTrackParticle();
        auto const abs_d0 = std::abs(track->d0());

        if (abs_d0 < d0_max || (disable_for_standalone_muons && ob->muonType() == xAOD::Muon::MuonType::MuonStandAlone)) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Transverse impact parameter significance (d0sig) selection
    template <typename ObjectType>
    std::vector<ObjectType const *> select_d0_significance_smaller(std::vector<ObjectType const *> const & in, BeamPosition const & beampos, double const d0sig_max, bool disable_for_standalone_muons) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        auto const * track = ob->primaryTrackParticle();

        /// Note: take absolute value!
        double const d0sig = std::abs(xAOD::TrackingHelpers::d0significance(track, beampos.sigma_x, beampos.sigma_y, beampos.sigma_xy));

        if (d0sig < d0sig_max || (disable_for_standalone_muons && ob->muonType() == xAOD::Muon::MuonType::MuonStandAlone)) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Apply an additional isolation selection for stand-alone muons only
    /// To get the isolation scale factors right, if a muon is found, its scale factor is divided by the "old" scale factor (if any) due to previous isolation requirement
    /// and multiplied by the "new" scale factor due to the stand-alone isolation requirement.
    /// NOTE: this function only makes sense if the additional standalone isolation is tighter than the used general muon isolation!
    ///       (Tighter in the sense that a standalone muon failing the general muon isolation should not pass the additional isolation.)
    std::vector<xAOD::Muon const *> select_accepted_by_standalone_isolation_and_adjust_scale_factor(
        std::vector<xAOD::Muon const *> const & in,
        CP::IsolationSelectionTool * iso_tool,
        CP::MuonEfficiencyScaleFactors * old_sf_tool,
        CP::MuonEfficiencyScaleFactors * new_sf_tool);



  }
}
