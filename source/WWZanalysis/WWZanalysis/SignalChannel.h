// -*- c++ -*-
/// Organising events by the signal channel they fall into.
/// The channels are: Electrons (4e), Muons (4mu), Mixed (2e2mu), None (any other number of leptons or flavour composition),
/// and Any (includes all of the previous)
///
/// IMPORTANT: what exactly it means for an event to be in a particular channel depends on the use case.
///            The classification here is based purely on counting the number of leptons of each flavour.

#pragma once

#include "DileptonPair.h"
#include "Dilepton.h"

#include <cstddef> // for std::size_t
#include <unordered_map>

namespace WWZanalysis {
  namespace SignalChannel {

    enum class Type {Electrons, Muons, Mixed, None, Any};



    /// Map the enumerated type (cast to integers) to names for the channels
    /// These names are used in the paths of output histogram directories for the different channels
    std::unordered_map<int, std::string> const channel_names = {
        {static_cast<int>(Type::Electrons), "ElectronChannel"},
        {static_cast<int>(Type::Muons), "MuonChannel"},
        {static_cast<int>(Type::Mixed), "MixedChannel"},
        {static_cast<int>(Type::None), "NotAnySignalChannel"},
        {static_cast<int>(Type::Any), "NoChannelSelection"}
    };



    /// Get channel of an event based on the numbers of leptons it contains
    /// Anything other than 4e, 4mu, or 2e2mu will be classified as channel 'None'
    /// E.g. an event with 5 muons and 0 electrons and an event with 1 muon and 3 electrons would both be classified as 'None'
    Type get_signal_channel(std::size_t number_of_electrons, std::size_t number_of_muons);



    /// Higgs event type getter (for the higher order correction EW/QCD tool in HZZUtils package)
    /// If returns -1 something is wrong
    /// Works only for two types {Electron, Muon} if another one is added this need to be reimplemented
    /// Depends on the higgs group conventions here:
    /// https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/HiggsPhys/Run2/HZZ/Minitrees/H4l/Primary/trunk/H4lCutFlow/H4lCutFlow/EnumDef4l.h
    int get_higgs_signal_channel(const DileptonPair & q);

  }
}
