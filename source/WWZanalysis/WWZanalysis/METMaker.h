// -*- c++ -*-

#pragma once

#include "AsgTools/MessageCheck.h"
#include "AsgTools/AnaToolHandle.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"

#include "METInterface/IMETMaker.h"
#include "METInterface/IMETSystematicsTool.h"
#include "METUtilities/METHelpers.h"

#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"

namespace WWZanalysis {
  namespace METMaker {

    xAOD::MissingETContainer* recalculateEventMET( const xAOD::MissingETContainer* xaod_met_core, const xAOD::MissingETAssociationMap* xaod_met_map, asg::AnaToolHandle<IMETMaker> tool, const xAOD::ElectronContainer *xaod_el, const xAOD::MuonContainer *xaod_mu, const xAOD::JetContainer *xaod_jet, AsgElectronLikelihoodTool *m_AsgElectronLikelihoodTool_looseID, CP::MuonSelectionTool *m_MuonSelectionTool_looseID, asg::AnaToolHandle<IMETSystematicsTool> metSystTool);

  }
}
