// -*- c++ -*-
/// Functions for adding scale factor weights to objects (typically muons and electrons).
/// Usually they are to be applied/used after the full object selection.

#pragma once

#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"
//#include "xAODEgamma/ElectronAuxContainer.h"

#include <iostream>

#include <vector>
#include <typeinfo>

namespace WWZanalysis {

  namespace ScaleFactors {



    /// Return the object weight stored in decoration "scale_factor_weight" if available, else 1.0
    /// IMPLEMENTATION NOTE: in the function body, need to specify that the called method is templated.
    template <typename ObjectType>
    double get_weight(ObjectType const * ob) {
      if (ob->template isAvailable<double>("scale_factor_weight")) {
        return ob->template auxdataConst<double>("scale_factor_weight");
      }
      return 1.0;
    }



    /// Return weight of one single object, given by tool passed as argument. A tool pointer that is null has no effect.
    /// The object is left unchanged (it is passed as a reference-to-const)
    template <typename ObjectType>
    double get_scale_factor(ObjectType const & ob, std::vector<CP::MuonEfficiencyScaleFactors *> const & tools) {

      float scale_factor = 1.0;

      /// pT check (in the past, negative pT sometimes led to segfaults)
      if (ob.pt() < 0.0) return scale_factor;

      /// For quick error checks using a macro (eeeewww... :D)
      char const * APP_NAME = "ScaleFactors";

      for (auto * const & tool : tools) {
        float tool_sf = 1.0;
        if (tool) {
          CHECK( tool->getEfficiencyScaleFactor(ob, tool_sf) );
        }
        scale_factor *= tool_sf;
      }

      return static_cast<double>(scale_factor);

    }



    /// Return weight of one single object, given by tool passed as argument. A tool pointer that is null has no effect.
    /// The object is left unchanged (it is passed as a reference-to-const)
    template <typename ObjectType>
    double get_scale_factor(ObjectType const & ob, std::vector<AsgElectronEfficiencyCorrectionTool *> const & tools) {

      double scale_factor = 1.0;

      /// pT check (in the past, negative pT sometimes led to segfaults)
      if (ob.pt() < 0.0) return scale_factor;

      /// For quick error checks using a macro (eeeewww... :D)
      char const * APP_NAME = "ScaleFactors";

      for (auto * const & tool : tools) {
        double tool_sf = 1.0;
        if (tool) {
          CHECK( tool->getEfficiencyScaleFactor(ob, tool_sf) );
        }
        scale_factor *= tool_sf;
      }

      return scale_factor;
    }





    /// Get a vector containing the weight of each object passed to the function.
    /// Each object's weight is the product of the weights it receives by various tools: selection efficiency, trigger, etc.
    ///
    /// If any of the tools doesn't exist (argument vector contains null pointers), that tool is ignored.
    /// The other tools are considered normally.
    template <typename ObjectType, typename SFToolType>
    std::vector<double> get_weights(std::vector<ObjectType const *> const & obs, std::vector<SFToolType *> const & tools) {
      // All object weights initialised to unity:
      auto weights = std::vector<double>();

      for (auto const * ob : obs) {
        if (! ob) throw std::logic_error("Error in ScaleFactors::get_weights(): physics object pointer is null!");
        auto const weight = get_scale_factor(* ob, tools);
        weights.push_back(weight);
      }

      // Check if there are as many weights as there are objects
      if (weights.size() != obs.size()) {
        throw std::logic_error("Error in ScaleFactors::get_weights(): number of object weights not equal to number of objects!");
      }

      return weights;
    }



    /// Dress the input objects with weights given by the tools passed as argument.
    /// The decoration can later be accessed by doing 'object->auxdecor<double>("scale_factor_weight")'
    /// Each object's weight is the product of the weights it receives by various tools: selection efficiency, trigger, etc.
    ///
    /// If any of the tools doesn't exist (argument vector contains null pointers), that tool is ignored.
    /// The other tools are considered normally.
    ///
    /// USAGE NOTE: you must specify the first template argument (ObjectType) explicitly when calling, e.g.
    /// ... = get_decorated_with_weights<xAOD::Electron>(...)
    /// as it cannot be inferred from the input arguments.
    /// (It *could* be inferred from ContainerType, but I'm too lazy to code it.)
    template <typename ObjectType, typename ContainerType, typename SFToolType>
    std::vector<ObjectType const *> get_decorated_with_weights(ContainerType * collection, std::vector<SFToolType *> const & tools) {
      auto decorated = std::vector<ObjectType const *>();

      for (auto const & ob : * collection) {
        if (! ob) throw std::logic_error("Error in ScaleFactors::get_decorated_with_weights(): electron pointer is null!");
        double const weight = get_scale_factor(* ob, tools);
        /// Need to specify "template" explicitly, because it's a templated method called on a templated type:
        ob->template auxdecor<double>("scale_factor_weight") = weight;
        decorated.push_back(ob);
      }

      return decorated;
    }



    /// Sum up the weights in the vector
    double sum_of_weights(std::vector<double> const & weights);



    /// Sum up the weights of the objects
    /// Make sure an overload of get_weight() exists for ObjectType!
    template <typename ObjectType>
    double sum_of_weights(std::vector<ObjectType const *> const & obs) {
      double sum_of_weights = 0.0;

      for (auto const * ob : obs) {
        sum_of_weights += get_weight(ob);
      }

      return sum_of_weights;
    }



  }

}
