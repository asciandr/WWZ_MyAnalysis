// -*- c++ -*-
/// Functions to select jets.
/// Most are implemented as templates, meaning that they work with different data types of jets.

#pragma once

#include "xAODJet/JetContainer.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

#include <vector>
#include <algorithm>



namespace WWZanalysis {

  namespace BTaggingScaleFactors {

    void decorate_with_bTagSFs(const xAOD::JetContainer * in, const BTaggingEfficiencyTool * tool, const BTaggingSelectionTool * sel_tool);

    std::vector<float> retrieve_Eigen(const xAOD::Jet *jet, BTaggingEfficiencyTool * tool, const BTaggingSelectionTool * sel_tool, std::vector<CP::SystematicSet> ftag_systematics_list, std::string bstring, int up_down);

  }

}
