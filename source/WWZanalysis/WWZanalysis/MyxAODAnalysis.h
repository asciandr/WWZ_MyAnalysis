#ifndef WWZanalysis_MyxAODAnalysis_H
#define WWZanalysis_MyxAODAnalysis_H

#include <EventLoop/Algorithm.h>

//ToolHandle
#include "AsgTools/MessageCheck.h" 
#include "AsgTools/AnaToolHandle.h"

// Event
#include "PileupReweighting/PileupReweightingTool.h"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"
// Overlap removal tools:
#include "AssociationUtils/ToolBox.h"
#include "AssociationUtils/OverlapRemovalInit.h"

// Jets
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetMomentTools/JetCaloEnergies.h"
#include "JetSelectorTools/EventCleaningTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"
#include "JetResolution/JERTool.h"
#include "JetResolution/JERSmearingTool.h"
#include "JetJvtEfficiency/JetJvtEfficiency.h"

// Muons
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
// isolation is for both mu and el
#include "IsolationSelection/IsolationSelectionTool.h"

// Electrons
#include "IsolationCorrections/IsolationCorrectionTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"

// MET
#include "METInterface/IMETMaker.h"
#include "METInterface/IMETSystematicsTool.h"
#include "METUtilities/METHelpers.h"

// b-tagging
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"

#include <TTree.h>
#include <TH1.h>

namespace WWZanalysis {

  class MyxAODAnalysis : public EL::Algorithm
  {
    // put your configuration variables here as public variables.
    // that way they can be set directly from CINT and python.
  public:
    // float cutValue;
  
  
  
    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
  public:
    // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree
    std::string m_outputName;
    std::string m_outputtree;
    TTree *m_tree; //!
    // event
    unsigned long long 		m_EventNumber; //!
    unsigned int 		m_runNumber; //!
    unsigned int 		m_RunYear; //!
    unsigned int 		m_mcChannelNumber; //!
    float 			m_mu; //!
    float 			m_mcEventWeight; //!
    std::vector<std::string> 	m_MCWeightNames; //!
    std::vector<float> 		m_allMC_weights; //!
    float 			m_xsec; //!
    unsigned long long 		m_number_of_events_before_skimming; //!
    float 			m_sum_of_weights_before_skimming; //!
    std::vector<float>		m_allSUM_of_weights_before_skimming; //!
    bool			m_passTrigger; //!
    bool			m_passEventCleaning; //!
    bool 			m_HLT_e24_lhmedium_L1EM20VH; //!
    bool 			m_HLT_e60_lhmedium; //!
    bool 			m_HLT_e120_lhloose; //!
    bool 			m_HLT_mu20_iloose_L1MU15; //!
    bool 			m_HLT_mu50; //!
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // bool 			m_HLT_2e12_lhloose_L12EM10VH; //!
    // bool 			m_HLT_mu18_mu8noL1; //!
    // bool 			m_HLT_e17_lhloose_mu14; //!
    bool 			m_HLT_mu26_ivarmedium; //!
    bool 			m_HLT_e26_lhtight_nod0_ivarloose; //!
    bool 			m_HLT_e60_lhmedium_nod0; //!
    bool 			m_HLT_e140_lhloose_nod0; //!
    // 14.08.18: drop DLTs, as -0.1% on signal acceptance
    // bool			m_HLT_mu22_mu8noL1; //!
    // bool			m_HLT_2e17_lhvloose_nod0; //!
    // bool			m_HLT_2e24_lhvloose_nod0; //!
    // bool			m_HLT_e17_lhloose_nod0_mu14; //!
    // unused triggers
    //bool			m_HLT_e26_lhmedium_nod0_mu8noL1; //!
    //bool			m_HLT_e7_lhmedium_nod0_mu24; //!
    //bool			m_HLT_mu20_2mu4noL1; //!
    //bool			m_HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH; //!
    //bool                     	m_HLT_2mu14; //!
    // unused triggers
    bool 			m_isTrigMatched; //!
    bool 			m_has_PV; //!
    int				m_tot_charge; //!
    // truth event info
    // Higgs decay mode
    int				m_higgsMode; //!
    // Z decay mode
    std::vector<int>		m_ZdecayMode; //!
    unsigned int		m_nTruthPromptLep; //!
    unsigned int		m_W_lep; //!
    unsigned int		m_W_had; //!
    unsigned int		m_Z_lep; //!
    unsigned int		m_Z_inv; //!
    unsigned int		m_Z_had; //!
    // inv masses
    float			m_Mllll0123; //!
    float			m_Mlll012; //!
    float			m_Mlll013; //!
    float			m_Mlll023; //!
    float			m_Mlll123; //!
    float			m_Mll01; //!
    float			m_Mll02; //!
    float			m_Mll03; //!
    float			m_Mll12; //!
    float			m_Mll13; //!
    float			m_Mll23; //!
    float			m_best_Z_Mll; //!
    float			m_best_Z_other_Mll; //!
    float			m_Mjj01; //!
    float			m_Mjj02; //!
    float			m_Mjj03; //!
    float			m_Mjj04; //!
    float			m_Mjj05; //!
    float			m_Mjj12; //!
    float			m_Mjj13; //!
    float			m_Mjj14; //!
    float			m_Mjj15; //!
    float			m_Mjj23; //!
    float			m_Mjj24; //!
    float			m_Mjj25; //!
    float			m_Mjj34; //!
    float			m_Mjj35; //!
    float			m_Mjj45; //!
    // event kinematics
    float			m_HT_lep; //!
    float			m_HT_had; //!
    // MET
    float 			m_MET; //!
    float 			m_MET_phi; //!
    float 			m_MET_x; //!
    float 			m_MET_y; //!
    float                 	m_MET_RefFinal_et; //!
    float			m_MET_RefFinal_phi; //!
    float			m_MET_RefFinal_x; //!
    float			m_MET_RefFinal_y; //!
    // jets
    std::vector<float> 		m_vjet_E; //! 
    std::vector<float> 		m_vjet_pt; //! 
    std::vector<float> 		m_vjet_eta; //! 
    std::vector<float> 		m_vjet_phi; //!
    std::vector<float> 		m_vjet_jvt; //! 
    std::vector<int> 		m_vjet_coneLabel; //! 
    std::vector<double>		m_vjet_mv2c10; //! 
    std::vector<double>		m_vjet_mv2c10mu; //! 
    int  		   	m_nJets_mv2c10_60; //! 
    int  		   	m_nJets_mv2c10_70; //! 
    int  		   	m_nJets_mv2c10_77; //! 
    int  		   	m_nJets_mv2c10_85; //! 
    // leptons
    std::vector<float>          m_vlepton_ID; //! 
    std::vector<float>          m_vlepton_E; //! 
    std::vector<float>          m_vlepton_pt; //! 
    std::vector<float>          m_vlepton_eta; //! 
    std::vector<float>          m_vlepton_phi; //! 
    std::vector<float>          m_vlepton_q; //! 
    std::vector<float>          m_vlepton_d0; //! 
    std::vector<float>          m_vlepton_z0; //! 
    std::vector<float>          m_vlepton_z0sinT; //! 
    std::vector<int>            m_vlepton_truthType; //! 
    std::vector<int>            m_vlepton_truthOrigin; //! 
    std::vector<double>         m_vlepton_sigd0; //!
    std::vector<double>         m_vlepton_sigz0; //!
    std::vector<double>         m_lepton_PromptLeptonVeto; //!
    std::vector<double>         m_lepton_PromptLeptonIso_TagWeight; //!
    std::vector<bool>          	m_vlepton_isTightLH; //! 
    std::vector<bool>          	m_vlepton_isTightID; //! 
    std::vector<float>         	m_vlepton_chargeIDBDTTight; //! 
    std::vector<unsigned char>	m_vlepton_ambiguityType; //! 
    std::vector<short>		m_vlepton_isolationFixedCutLoose; //! 
    // lepton scale factors and syst variations
//    float			m_lepSFTrigLoose; //!
//    float 			m_weight_leptonSF_EL_SF_Trigger_UP; //!
//    float 			m_weight_leptonSF_EL_SF_Trigger_DOWN; //!
//    float 			m_weight_leptonSF_MU_SF_Trigger_STAT_UP;
//    float 			m_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
//    float 			m_weight_leptonSF_MU_SF_Trigger_SYST_UP;
//    float 			m_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
    std::vector<float> 		m_vlepton_SFIDLoose; //!
    std::vector<float> 		m_vlepton_SFIDTightLH; //!
    std::vector<float> 		m_vlepton_SFIsoLoose; //!
    std::vector<float> 		m_vlepton_SFIsoPLV; //!
    std::vector<float>		m_vlepton_SFReco; //!
    std::vector<float>		m_vlepton_SFTTVA; //!
    std::vector<float>		m_vlepton_SFObjLoose; //!
    std::vector<float> 		m_vlepton_SFObjLoose_EL_SF_ID_UP; //!
    std::vector<float> 		m_vlepton_SFObjLoose_EL_SF_ID_DOWN; //!
    std::vector<float>		m_vlepton_SFObjLoose_EL_SF_Reco_UP; //!
    std::vector<float> 		m_vlepton_SFObjLoose_EL_SF_Reco_DOWN; //!
    std::vector<float>		m_vlepton_SFObjLoose_EL_SF_Iso_UP; //!
    std::vector<float> 		m_vlepton_SFObjLoose_EL_SF_Iso_DOWN; //!
    std::vector<float> 		m_vlepton_SFObjLoose_MU_SF_RECO_STAT_UP; //!
    std::vector<float> 		m_vlepton_SFObjLoose_MU_SF_RECO_STAT_DOWN; //!
    std::vector<float> 		m_vlepton_SFObjLoose_MU_SF_RECO_SYST_UP; //!
    std::vector<float> 		m_vlepton_SFObjLoose_MU_SF_RECO_SYST_DOWN; //!
    std::vector<float>		m_vlepton_SFObjLoose_MU_SF_Iso_STAT_UP; //!
    std::vector<float>		m_vlepton_SFObjLoose_MU_SF_Iso_STAT_DOWN; //!
    std::vector<float>		m_vlepton_SFObjLoose_MU_SF_Iso_SYST_UP; //!
    std::vector<float>		m_vlepton_SFObjLoose_MU_SF_Iso_SYST_DOWN; //!
    std::vector<float>		m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_UP; //!
    std::vector<float>		m_vlepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN; //!
    std::vector<float>		m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_UP; //!
    std::vector<float>		m_vlepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN; //!
    std::vector<float>		m_vlepton_SFObjTightLH; //!
    std::vector<float> 		m_vlepton_SFObjTightLH_EL_SF_ID_UP; //!
    std::vector<float> 		m_vlepton_SFObjTightLH_EL_SF_ID_DOWN; //!
    std::vector<float>		m_vlepton_SFObjTightLH_EL_SF_Reco_UP; //!
    std::vector<float> 		m_vlepton_SFObjTightLH_EL_SF_Reco_DOWN; //!
    std::vector<float>		m_vlepton_SFObjTightLH_EL_SF_Iso_UP; //!
    std::vector<float> 		m_vlepton_SFObjTightLH_EL_SF_Iso_DOWN; //!
    std::vector<float> 		m_vlepton_SFObjTight_MU_SF_RECO_STAT_UP; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_RECO_STAT_DOWN; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_RECO_SYST_UP; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_RECO_SYST_DOWN; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_Iso_STAT_UP; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_Iso_STAT_DOWN; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_Iso_SYST_UP; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_Iso_SYST_DOWN; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_TTVA_STAT_UP; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_TTVA_STAT_DOWN; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_TTVA_SYST_UP; //!
    std::vector<float>		m_vlepton_SFObjTight_MU_SF_TTVA_SYST_DOWN; //!

    // muons
    std::vector<float> 		m_vmuon_E; //! 
    std::vector<float> 		m_vmuon_pt; //! 
    std::vector<float> 		m_vmuon_eta; //! 
    std::vector<float> 		m_vmuon_phi; //! 
    std::vector<float> 		m_vmuon_q; //! 
    std::vector<float> 		m_vmuon_d0; //! 
    std::vector<float> 		m_vmuon_z0; //! 
    std::vector<float> 		m_vmuon_z0sinT; //! 
    std::vector<int> 		m_vmuon_truthType; //! 
    std::vector<int> 		m_vmuon_truthOrigin; //! 
    std::vector<double>		m_vmuon_sigd0; //!
    std::vector<double>		m_vmuon_sigz0; //!
    // electrons
    std::vector<float> 		m_velectron_E; //! 
    std::vector<float> 		m_velectron_pt; //! 
    std::vector<float> 		m_velectron_eta; //! 
    std::vector<float> 		m_velectron_phi; //! 
    std::vector<float> 		m_velectron_q; //! 
    std::vector<float> 		m_velectron_d0; //! 
    std::vector<float> 		m_velectron_z0; //! 
    std::vector<float> 		m_velectron_z0sinT; //! 
    std::vector<int>            m_velectron_truthType; //! 
    std::vector<int>            m_velectron_truthOrigin; //!
    std::vector<double>		m_velectron_sigd0; //!
    std::vector<double>		m_velectron_sigz0; //!
    // jets
    std::vector<float> 		m_beforeOLR_vjet_E; //! 
    std::vector<float> 		m_beforeOLR_vjet_pt; //! 
    std::vector<float> 		m_beforeOLR_vjet_eta; //! 
    std::vector<float> 		m_beforeOLR_vjet_phi; //!
    std::vector<float> 		m_beforeOLR_vjet_jvt; //! 
    std::vector<int> 		m_beforeOLR_vjet_coneLabel; //! 
    // forward jets
    std::vector<float> 		m_forward_vjet_E; //! 
    std::vector<float> 		m_forward_vjet_pt; //! 
    std::vector<float> 		m_forward_vjet_eta; //! 
    std::vector<float> 		m_forward_vjet_phi; //!
    std::vector<float> 		m_forward_vjet_jvt; //! 
    std::vector<int> 		m_forward_vjet_coneLabel; //! 
    // muons
    std::vector<float> 		m_beforeOLR_vmuon_E; //! 
    std::vector<float> 		m_beforeOLR_vmuon_pt; //! 
    std::vector<float> 		m_beforeOLR_vmuon_eta; //! 
    std::vector<float> 		m_beforeOLR_vmuon_phi; //! 
    std::vector<float> 		m_beforeOLR_vmuon_q; //! 
    std::vector<float> 		m_beforeOLR_vmuon_d0; //! 
    std::vector<float> 		m_beforeOLR_vmuon_z0; //! 
    std::vector<float> 		m_beforeOLR_vmuon_z0sinT; //! 
    // electrons
    std::vector<float> 		m_beforeOLR_velectron_E; //! 
    std::vector<float> 		m_beforeOLR_velectron_pt; //! 
    std::vector<float> 		m_beforeOLR_velectron_eta; //! 
    std::vector<float> 		m_beforeOLR_velectron_phi; //! 
    std::vector<float> 		m_beforeOLR_velectron_q; //! 
    std::vector<float> 		m_beforeOLR_velectron_d0; //! 
    std::vector<float> 		m_beforeOLR_velectron_z0; //! 
    std::vector<float> 		m_beforeOLR_velectron_z0sinT; //! 
    // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree
  

    //---------------------------------------
    // Event SFs and related SYSTEMATICS
    //---------------------------------------

    // Vector of systematic uncertainties to run over:
    std::vector<CP::SystematicSet> systematics_list; //!
    // Vector of FTAG systematic variations
    std::vector<CP::SystematicSet> ftag_systematics_list; //!

    // PRW and systematic variations
    float   			m_pileup_weight; //!
    float 			m_pileup_weight_UP; //!
    float 			m_pileup_weight_DOWN; //!
    // JVT SF and systematic variations
    float			m_JVT_EventWeight; //!
    float			m_JVT_EventWeight_UP; //!
    float			m_JVT_EventWeight_DOWN; //!
    // b-tagging SFs and systematic variations
    float			m_MV2c10_FixedCutBEff_70_EventWeight; //!
    std::vector<float>		m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_up; //!
    std::vector<float>		m_bTagSF_weight_MV2c10_FixedCutBEff_70_B_down; //!
    std::vector<float>		m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_up; //!
    std::vector<float>		m_bTagSF_weight_MV2c10_FixedCutBEff_70_C_down; //!
    std::vector<float>		m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up; //!
    std::vector<float>		m_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down; //!


    //---------------------------------------
    // Event SFs and related SYSTEMATICS
    //---------------------------------------


    TH1 *h_total_events; //!
    int m_eventCounter; //!
    int m_nalljets; //!
    bool m_theres_cutbookkeeper; //!

    //CP TOOLS
    //----------------------------
    // EVENT
    //---------------------------

    CP::PileupReweightingTool 			*m_PileupReweightingTool; //!
    GoodRunsListSelectionTool 			*m_GoodRunsListSelectionTool; //!
    TrigConf::xAODConfigTool 			*m_xAODConfigTool; //!
    Trig::TrigDecisionTool 			*m_TrigDecisionTool; //!
    // from concrete tool to interface, see discussion here https://groups.cern.ch/group/hn-atlas-PATHelp/Lists/Archive/DispForm.aspx?ID=24791
    //Trig::MatchingTool 			*m_MatchingTool;//!
    asg::AnaToolHandle<Trig::IMatchingTool> 	m_MatchingTool;//!
//    ECUtils::EventCleaningTool 		*m_EventCleaningTool; //!
    ORUtils::ToolBox 				*m_OLR; //!
    // MET
    asg::AnaToolHandle<IMETMaker>          	metMaker; //!
    asg::AnaToolHandle<IMETSystematicsTool>	metSystTool; //!

    //----------------------------
    // EVENT
    //---------------------------



    //----------------------------
    // JETS
    //---------------------------

    JetCalibrationTool 	 		*m_JetCalibrationTool; //!
    JetCleaningTool 	 		*m_jetCleaning; //! 
    JetVertexTaggerTool	 		*m_JetVertexTaggerTool; //!
    JetCaloEnergies 	 		*m_JetCaloEnergies; //!  
    JetUncertaintiesTool 		*m_JetUncertaintiesTool; //!
    JERTool 		 		*m_JERTool; //!  
    JERSmearingTool 			*m_JERSmearingTool; //!
    CP::JetJvtEfficiency		*m_JetVertexTaggerToolSF; //!

    //----------------------------
    // JETS
    //---------------------------



    // isolation tool is the same for both mu and el
    // iso reqs on loose leptons
    CP::IsolationSelectionTool			*m_IsolationSelectionTool; //!
    // iso reqs stored into dedicated branches
    CP::IsolationSelectionTool			*m_IsolationSelectionTool_FixedCutLoose; //!

    //----------------------------
    // MUONS
    //---------------------------

    CP::MuonSelectionTool 			*m_MuonSelectionTool; //!
    CP::MuonSelectionTool 			*m_MuonSelectionTool_looseID; //!
    CP::MuonCalibrationAndSmearingTool 		*m_MuonCalibrationAndSmearingTool; //!
    CP::MuonEfficiencyScaleFactors 		*m_MuonEfficiencyScaleFactors_ID; //!
    CP::MuonEfficiencyScaleFactors 		*m_MuonEfficiencyScaleFactors_isolation; //!
    // Muon track-to-vertex-association (TTVA) efficiency scale factor tool
    // See: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesMC16#Muon_track_to_vertex_association
    CP::MuonEfficiencyScaleFactors              *m_MuonEfficiencyScaleFactors_TTVA; //!
    // Custom MuonEfficiencyScaleFactors devoted to calibration of PLV
    CP::MuonEfficiencyScaleFactors            	*m_MuonEfficiencyScaleFactors_PLV; //!

    CP::MuonTriggerScaleFactors 		*m_MuonTriggerScaleFactors; //!

    //----------------------------
    // MUONS
    //---------------------------



    //----------------------------
    // ELECTRONS
    //---------------------------

    // Application of iso corrections on electrons is currently (r21.2.25) not needed, see https://its.cern.ch/jira/browse/ATLEGAMDPD-75
//    CP::IsolationCorrectionTool			*m_IsolationCorrectionTool; //!

    AsgElectronLikelihoodTool 			*m_AsgElectronLikelihoodTool; //!
    AsgElectronLikelihoodTool 			*m_AsgElectronLikelihoodTool_looseID; //!
    AsgElectronEfficiencyCorrectionTool 	*m_AsgElectronEfficiencyCorrectionTool_ID; //!
    AsgElectronEfficiencyCorrectionTool 	*m_AsgElectronEfficiencyCorrectionTool_ID_TightLH; //!
    AsgElectronEfficiencyCorrectionTool 	*m_AsgElectronEfficiencyCorrectionTool_isolation; //!
    AsgElectronEfficiencyCorrectionTool 	*m_AsgElectronEfficiencyCorrectionTool_reco; //!
    AsgElectronEfficiencyCorrectionTool 	*m_AsgElectronEfficiencyCorrectionTool_trigSF; //!
    // Custom AsgElectronEfficiencyCorrectionTool devoted to calibration of PLV
    AsgElectronEfficiencyCorrectionTool		*m_AsgElectronEfficiencyCorrectionTool_PLV; //!
//    AsgElectronEfficiencyCorrectionTool 	*m_AsgElectronEfficiencyCorrectionTool_trigEff; //!
    CP::EgammaCalibrationAndSmearingTool 	*m_EgammaCalibrationAndSmearingTool; //!
    AsgElectronChargeIDSelectorTool		*m_AsgElectronChargeIDSelectorTool; //!

    //----------------------------
    // ELECTRONS
    //---------------------------


    //----------------------------
    // B-TAGGING 
    //---------------------------

    BTaggingSelectionTool			*m_BTaggingSelectionTool; //!
    BTaggingEfficiencyTool 			*m_BTaggingEfficiencyTool; //!
 
    //----------------------------
    // B-TAGGING 
    //---------------------------
    //CP TOOLS



    // CP TOOLS OPTIONS
    // CP::PileupReweightingTool
    std::vector<std::string> 				m_opt_prw_confFiles;
    std::vector<std::string> 				m_opt_prw_lcalcFiles;
    // GoodRunsListSelectionTool 
    std::vector<std::string> 				m_opt_grl_GoodRunsList;
    // JetCleaningTool 
    std::string	 					m_opt_jclean_CutLevel;
    bool	 					m_opt_jclean_DoUgly;
    // JetCalibrationTool
    std::string						m_opt_jcal_JetCollection;
    std::string						m_opt_jcal_ConfigFile;
    std::vector<std::string>				m_opt_jcal_CalibSequence;
    // JetUncertaintiesTool
    std::string                                         m_opt_junc_MCType;
    std::string                                         m_opt_junc_ConfigFile;
    // JERTool
    std::string                                         m_opt_jer_PlotFileName;
    // JERSmearingTool
    bool	          				m_opt_jersmear_ApplyNominalSmearing;
    std::string           				m_opt_jersmear_SystematicMode;
    // CP::JetJvtEfficiency
    std::string						m_opt_jvtSF_WP;
    std::string						m_opt_jvtSF_SFFile;
    // CP::IsolationSelectionTool 
    bool						m_opt_isosel_doLepIso;
    std::string                                         m_opt_isosel_MuonWP;
    std::string                                         m_opt_isosel_ElectronWP;
    // CP::MuonSelectionTool
    double	                                        m_opt_musel_MaxEta;
    int	              					m_opt_musel_MuQuality;
    // CP::MuonEfficiencyScaleFactors 
    std::string						m_opt_muidSF_WP; 
    std::string						m_opt_muisoSF_WP;      
    std::string						m_opt_muttvaSF_WP;     
    bool						m_opt_mutrigSF_AllowZeroSF;
    // CP::MuonTriggerScaleFactors
    std::string                                         m_opt_mutrigSF_MuonQuality;
    std::string                                         m_opt_mutrigSF_Isolation;
    // CP::EgammaCalibrationAndSmearingTool
    std::string                                         m_opt_elcal_ESModel;
    std::string                                         m_opt_elcal_decorrelationModel;
    // AsgElectronLikelihoodTool
    std::string                                         m_opt_elLH_WorkingPoint; 
    std::string                                         m_opt_elLH_ConfigFile; 
    // AsgElectronEfficiencyCorrectionTool 
    std::string						m_opt_elidSF_input;
    std::string						m_opt_elisoSF_input;
    std::string						m_opt_elrecoSF_input;
    std::string						m_opt_eltrigSF_input;
    // CP TOOLS OPTIONS



    
    // Basic selection
    // event
    float						m_Nleptons_cut;
    float						m_Njets_cut;
    // jets
    float						m_jet_pt_cut;
    float						m_jet_eta_cut;
    float						m_jet_jvt_cut;
    // muons
    float						m_muon_pt_cut; 
    float						m_muon_eta_cut; 
    float						m_muon_z0sinT_cut; 
    float						m_muon_d0_cut; 
    float						m_muon_sigd0_cut; 
    // electrons 
    float                                              	m_electron_pt_cut;
    float                                              	m_electron_eta_cut;
    float                                              	m_electron_z0sinT_cut;
    //float                                              m_electron_d0_cut;
    float                                              	m_electron_sigd0_cut;
    // Basic selection

     
    // Other options
    bool						m_doTruth;
    bool						m_debugMode;
    TString						m_xsecFile;
    TString						m_PLV_calfolder;
    bool						m_isAODfile;
    bool						m_doSystematics;
    bool						m_isnominalTTree;
    // Other options


 
    // Histograms
    TH1 *h_jetPt; //!
    TH1 *h_jeteta; //!
    TH1 *h_jetphi; //!
  
    // this is a standard constructor
    MyxAODAnalysis ();
  
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();
  
    // this is needed to distribute the algorithm to the workers
    ClassDef(WWZanalysis::MyxAODAnalysis, 1);

  };

}
#endif
