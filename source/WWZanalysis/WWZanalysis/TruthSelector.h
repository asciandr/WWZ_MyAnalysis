// -*- c++ -*-
/// Helper functions to retrieve truth information.

#pragma once

#include "xAODRootAccess/TEvent.h" // for TEvent
#include <xAODRootAccess/TStore.h> // for TStore

// xAOD 
#include "xAODTruth/TruthEventContainer.h"

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

namespace WWZanalysis {
  namespace TruthSelector {

    int GetHiggsDecayMode(const xAOD::TruthParticleContainer* truthCont);

    std::vector<int> GetZDecayMode(const xAOD::TruthParticleContainer* truthCont);

  }
}
