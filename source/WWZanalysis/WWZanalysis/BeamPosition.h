// -*- c++ -*-
/// Store information about the beam position for the current event

#pragma once

#include "xAODEventInfo/EventInfo.h"

namespace WWZanalysis {



  struct BeamPosition {

    /// Constructor. All members are initialised in it.
    /// (A bit dangerous to take a pointer argument, initialization might fail if the pointer is not valid...)
    BeamPosition(xAOD::EventInfo const * ei);

    double sigma_x;
    double sigma_y;
    double sigma_xy;

  };



}

