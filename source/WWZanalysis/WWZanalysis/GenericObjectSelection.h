// -*- c++ -*-
/// Functions for selecting objects that are applicable to various different kinds of objects, e.g.
/// electrons, muons, and jets. NOTE: it might be that not all functions here work for all objects.

#pragma once

#include "WWZanalysis/AbsEtaRange.h"
#include "WWZanalysis/BeamPosition.h"
#include "WWZanalysis/Utilities.h"

#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include <math.h>



namespace WWZanalysis {
  namespace GenericObjectSelection {



    /// Selection of items accepted by a selection tool (usually provided by Combined Performance group)
    /// Relies on the tool providing method accept() to check if an object passes the selection or not
    template <typename ObjectType, typename ToolType>
    std::vector<ObjectType const *> select_accepted_by_tool(std::vector<ObjectType const *> const & in, ToolType const * tool) {

      /// Do nothing if the tool does not exist
      if (! tool) {
        return in;
      }

      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        if (tool->accept(* ob)) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Decoration of items accepted by a selection tool (usually provided by Combined Performance group)
    /// Relies on the tool providing method accept() to check if an object passes the selection or not
    template <typename Container, typename ToolType>
    void decorate_accepted_by_tool(Container const * in, ToolType const * tool, std::string const & label) {

      /// Do nothing if the tool does not exist
      if (! tool)
        return;

      for (auto const & ob : * in) {
        if (tool->accept(ob)) {
          ob->template auxdecor<char>(label) = static_cast<char>(true);
        }
        else {
          ob->template auxdecor<char>(label) = static_cast<char>(false);
        }
      }

      return;
    }



    /// Absolute pseudorapidity selection
    template <typename ObjectType>
    std::vector<ObjectType const *> select_in_abs_eta_range(std::vector<ObjectType const *> const & in, AbsEtaRange const & aer) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        if (aer.contains(ob)) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Transverse momentum selection for objects in a given absolute-pseudorapidity range
    template <typename ObjectType>
    std::vector<ObjectType const *> select_pt_greater(std::vector<ObjectType const *> const & in, double const pt_min, AbsEtaRange const & aer) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        if (aer.contains(ob) && ob->pt() * 0.001 > pt_min) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Transverse momentum selection for objects of any absolute pseudorapidity
    template <typename ObjectType>
    std::vector<ObjectType const *> select_pt_greater(std::vector<ObjectType const *> const & in, double const pt_min) {
      auto const aer = AbsEtaRange(1000.0); // allow abs(eta) up to a ridiculously high value
      return select_pt_greater(in, pt_min, aer);
    }



    /// Transverse impact parameter (d0) selection
    template <typename ObjectType>
    std::vector<ObjectType const *> select_d0_smaller(std::vector<ObjectType const *> const & in, double const d0_max) {
      std::vector<ObjectType const *> out;

      for (auto const * ob : in) {
        auto const * track = ob->primaryTrackParticle();
        auto const d0 = track->d0();

        if (d0 < d0_max) {
          out.push_back(ob);
        }
      }

      return out;
    }



    /// Return true if the selected object has *NONE* of the overlappers within dR_min
    template <typename ObjectType, typename OverlapperType>
    bool no_overlap(ObjectType const * ob, std::vector<OverlapperType const *> const & overlappers, double const dR_min) {
      auto const momentum = ob->p4();
      for (auto const * overlapper : overlappers) {
        if (momentum.DeltaR(overlapper->p4()) < dR_min) return false;
      }
      return true;
    }



    /// Select objects that don't have other objects within a given delta R
    /// ObjectType: type of the objects of interest
    /// OverlapperType: type of the objects that veto the objects of interest if they are nearby
    ///
    /// LIMITATION: cannot currently skip checks of objects "overlapping with themselves", i.e. only works for different collections of objets
    template <typename ObjectType, typename OverlapperType>
    std::vector<ObjectType const *> select_without_nearby(std::vector<ObjectType const *> const & in, std::vector<OverlapperType const *> const & overlappers, double const dR_min) {
      std::vector<ObjectType const *> out;
      for (auto const * ob : in) {
        if (no_overlap(ob, overlappers, dR_min)) {
          out.push_back(ob);
        }
      }
      return out;
    }



    /// Calculate longitudinal impact parameter (abs[z0 * sin(theta)]) selection
    /// Primary-vertex information is needed, because the distance z0 is measured with respect to the primary vertex
    ///
    /// CAUTION: returns 9999999.0 if no primary vertex is present (argument receives nullpointer)
    double calculate_abs_z0sintheta(xAOD::TrackParticle const * track, xAOD::Vertex const * primary_vertex);



//    /// Longitudinal impact parameter (abs[z0 * sin(theta)]) selection
//    /// Primary-vertex information is needed, because the distance z0 is measured with respect to the primary vertex
//    template <typename ObjectType>
//    std::vector<ObjectType const *> select_abs_z0sintheta_smaller(std::vector<ObjectType const *> const & in, xAOD::Vertex const * primary_vertex, double const abs_z0sintheta_max) {
//      std::vector<ObjectType const *> out;
//
//      for (auto const * ob : in) {
//        auto const abs_z0sintheta = calculate_abs_z0sintheta(ob, primary_vertex);
//
//        if (abs_z0sintheta < abs_z0sintheta_max) {
//          out.push_back(ob);
//        }
//      }
//
//      return out;
//    }



  }
}
