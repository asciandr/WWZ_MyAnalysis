// -*- c++ -*-
/// Structure for holding two lepton pairs and returning useful information about them

#pragma once

#include <utility>
#include "TLorentzVector.h"
#include "WWZanalysis/Dilepton.h"


namespace WWZanalysis {

  /// Combination of dilepton flavours in a dilepton pair. Four electrons, four muons, or two each
  enum class DileptonPairType {Electrons, Muons, Mixed};



  /// A handy function that returns true if the first argument is closer to the Z pole mass
  /// (comparing absolute values of differences)
  bool first_argument_is_closer_to_Z_mass(double m1, double m2);



  struct DileptonPair {

    /// Constructor, designed to make sure that incomplete instantiation is prevented.
    DileptonPair(std::pair<Dilepton, Dilepton> dileptons_);

    std::pair<Dilepton, Dilepton> dileptons;

    /// Return the flavour channel of the dilepton pair: 4 electrons, 4 muons, or 2 each
    DileptonPairType channel() const;

    /// Return the lepton pair whose mass is closer to the Z boson pole mass (ca. 91 GeV)
    Dilepton get_dilepton_with_mass_closer_to_Z() const;

    /// Return the lepton pair whose mass is further from the Z boson pole mass (ca. 91 GeV)
    Dilepton get_dilepton_with_mass_further_from_Z() const;

    /// Return the lepton pair with greater transverse momentum
    Dilepton get_leading_dilepton() const;

    /// Return the lepton pair with smaller transverse momentum
    Dilepton get_subleading_dilepton() const;

    /// Mass in GeV (CAUTION: In GeV, NOT MeV!)
    double mass() const;

    /// Total four-momentum of dilepton pair
    TLorentzVector momentum() const;

  };

}
