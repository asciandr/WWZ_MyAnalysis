// -*- c++ -*-
/// Structure for holding a lepton pair and returning useful information about it

#pragma once

#include <utility>
#include "TLorentzVector.h"



namespace WWZanalysis {

  /// Flavour of the leptons in a dilepton
  enum class LeptonFlavour {Electron, Muon};



  struct Dilepton {

    /// Constructor, designed to make sure that incomplete instantiation is prevented.
    Dilepton(LeptonFlavour flavour_, std::pair<TLorentzVector, TLorentzVector> momenta_);

    /// Flavour of the leptons in the pair
    LeptonFlavour flavour;

    /// Four-momenta of the leptons in the pair
    std::pair<TLorentzVector, TLorentzVector> momenta;

    /// Total four-momentum of the dilepton
    TLorentzVector momentum() const;

    /// Invariant mass of the pair of momenta in GeV (Caution!!! In GeV, NOT in MeV)
    double mass() const;

    /// Angular distance between the two constituent leptons
    double dr() const;

  };

}
