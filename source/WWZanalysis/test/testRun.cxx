#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/DiskListXRD.h"
#include "SampleHandler/DiskListEOS.h"
#include <SampleHandler/ToolsDiscovery.h>
#include <TSystem.h>
#include <TH1.h>
#include <random>
#include "PathResolver/PathResolver.h"

#include "WWZanalysis/MyxAODAnalysis.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* dataFileName = "root://eosatlas.cern.ch//eos/atlas/user/m/mdobre/forRTTdata16/Data16_210.AOD.pool.root";
  //SH::DiskListXRD list ("root://eosatlas.cern.ch", "/eos/atlas/user/m/mdobre/forRTTdata16/");
  //SH::DiskListEOS list ("/eos/atlas/user/m/mdobre/forRTTdata16/", "root://eosatlas.cern.ch//eos/atlas/user/m/mdobre/forRTTdata16/");
  //const char* inputFilePath = gSystem->ExpandPathName ("/tmp/data16_13TeV/");
  //SH::ScanDir().filePattern("Data16_210.AOD.pool.root").scan(sh,list);
  //  SH::ScanDir().filePattern("AOD.11078889._000001.pool.root.1").scan(sh,inputFilePath); 
  std::string confFile = PathResolverFindCalibFile("WWZanalysis/input.txt");
  SH::readFileList (sh, "sample", confFile);

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, 500);

  //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);
  job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);  
  //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);  
  job.options()->setDouble (EL::Job::optXAODSummaryReport, 0);
 
  // Add our analysis to the job:
  WWZanalysis::MyxAODAnalysis* alg = new WWZanalysis::MyxAODAnalysis();
  job.algsAdd( alg );

  // Update the submitDir
  //  std::string ran = std::to_string(((unsigned long long)rand() << 32) + rand());
  //EL::Driver::updateLocation ("submitDir_"+ran);

  // Run the job using the local/direct driver:
  EL::DirectDriver driver;
  driver.submit( job, submitDir );

  // Fetch and plot our histogram
  //  SH::SampleHandler sh_hist;
  //sh_hist.load (submitDir + "/hist");
  //TH1 *hist = (TH1*) sh_hist.get ("AOD.11078889._000001.pool.root.1")->readHist ("h_jetPt");
  // hist->Draw ();

  return 0;
}
