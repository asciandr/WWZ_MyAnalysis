#!/usr/bin/env python

import math
import ROOT
import os
import sys
#from ROOT.POOL import TEvent

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
  '''
  Python 2 implementation of Python 3.5 math.isclose()
  https://hg.python.org/cpython/file/tip/Modules/mathmodule.c#l1993
  '''
  # sanity check on the inputs
  if rel_tol < 0 or abs_tol < 0:
      raise ValueError("tolerances must be non-negative")

  # short circuit exact equality -- needed to catch two infinities of
  # the same sign. And perhaps speeds things up a bit sometimes.
  if a == b:
      return True

  # This catches the case of two infinities of opposite sign, or
  # one infinity and one finite number. Two infinities of opposite
  # sign would otherwise have an infinite relative tolerance.
  # Two infinities of the same sign are caught by the equality check
  # above.
  if math.isinf(a) or math.isinf(b):
      return False

  # now do the regular computation
  # this is essentially the "weak" test from the Boost library
  diff = math.fabs(b - a)
  result = (((diff <= math.fabs(rel_tol * b)) or
             (diff <= math.fabs(rel_tol * a))) or
            (diff <= abs_tol))
  return result

def main():

  # Input file name
  filename = '/tmp/data17_13TeV/data17_13TeV.00328099.physics_Main.merge.AOD.f836_m1824._lb0241._0002.1'

  # Check if athena is available
  athena = False
  cmd_exists = lambda x: any(os.access(os.path.join(path, x), os.X_OK) for path in os.environ["PATH"].split(os.pathsep))
  if cmd_exists('athena'):
    athena = True
    print "Athena based release"
  else:
    print "No Athena base release"
    sys.exit(1)

  from ROOT.POOL import TEvent
  # Access mode of TEvent
  #evt = TEvent(TEvent.kAthenaAccess)
  evt = TEvent(TEvent.kClassAccess)

  # File tracer - C++ code - python equivalent ?
  # xAOD::TFileAccessTracer::enableDataSubmission(false)
  # Control/xAODRootAccess/xAODRootAccess/tools/TFileAccessTracer.h

  #Setup for AnalysisBase environment
  #evt = ROOT.xAOD.TEvent()
  #f = ROOT.TFile("path/to/test/file.root")
  #evt.readFrom(f)
  #evt.getEntry(0) #load first event

  # Open input files
  rootfile = ROOT.TFile.Open(filename)
  evt.readFrom(rootfile)

  # Number of entries ?
  nevents = evt.getEntries()
  print 'File setup done, input contains {0} events'.format(nevents)

  # loop over the events and find pfos
  for entry in xrange(nevents):
    if( (entry % 100) == 0 ):
      print "Event number = %i" %entry

    evt.getEntry(entry)
    evtinfo = evt.retrieve("xAOD::EventInfo","EventInfo")
    eventnumber = evtinfo.eventNumber()
    runnumber = evtinfo.runNumber()

    #ppfo = evt.retrieve("xAOD::PFOContainer","JetETMissChargedParticleFlowObjects" )
    ppfo = evt.retrieve("xAOD::PFOContainer","JetETMissNeutralParticleFlowObjects" )

    for ipfo, pfo in enumerate(ppfo):
      charge = pfo.charge()
      if (math.fabs(charge) > 0.0):
        print "pfo charge = ", charge, ", runNumber = ", runnumber, ", eventNumber = ", eventnumber
    clusters = evt.retrieve("xAOD::CaloClusterContainer","CaloCalTopoClusters" )

    #for icluster, cluster in enumerate(clusters):
    #  clpt = cluster.pt()
    #  clphi = cluster.phi()
    #  cleta = cluster.eta()
    #  if isclose(math.fabs(clpt), 0.0):
    #    print "cluster pt = ", clpt, ", runNumber = ", runnumber, ", eventNumber = ", eventnumber
    #  if isclose(math.fabs(clphi), 0.0):
    #    print "cluster phi = ", clpt, ", runNumber = ", runnumber, ", eventNumber = ", eventnumber
    #  if isclose(math.fabs(cleta), 0.0):
    #    print "cluster eta = ", clpt, ", runNumber = ", runnumber, ", eventNumber = ", eventnumber

  return

if __name__ == "__main__":
    main()
      
