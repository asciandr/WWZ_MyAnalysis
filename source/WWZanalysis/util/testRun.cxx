#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include <TH1.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

#include <iostream> 
#include <sstream>

#include "WWZanalysis/MyxAODAnalysis.h"

std::string 			return_string (TString, std::string);
std::vector<std::string> 	return_vstring(TString, std::string, bool, std::string);
bool 				return_bool   (TString, std::string);
float 				return_float  (TString, std::string);

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 0 ) submitDir = argv[ 1 ];

  // Feed a txt file as job configuration
  TString configFile = "configFile";
  if( argc > 1 ) configFile = argv[ 2 ];
  // Folder for local inputs 
  std::string data_folder = "$TestArea/WWZanalysis/data/";
  // Fetch all info from config file
  // list of systematic TTrees
  std::vector<std::string>	v_systematicTTrees	= return_vstring(configFile	,"syst_ttree",	false,  "");
  // event
  std::string 			filePath	= return_string (configFile	,"LocalInputFilePath");
  std::string 			fileName	= return_string (configFile	,"LocalInputFileName");
  bool				isAODfile	= return_bool 	(configFile	,"isAODfile");
  bool				doSystematics	= return_bool 	(configFile	,"doSystematics");
  std::string 			outName 	= return_string (configFile	,"OutputName");
  std::vector<std::string>	vprwConfFile	= return_vstring(configFile	,"PRW_ConfigFile",	true,  data_folder);
  std::vector<std::string>	vprwLumiCalc	= return_vstring(configFile	,"PRW_LumiCalcFile",	false, data_folder);
  std::vector<std::string>	vgrlXML		= return_vstring(configFile	,"GoodRunList", 	true,  data_folder);
  // jets
  std::string 			jClean_cut	= return_string (configFile	,"JetClean_CutLevel");
  bool 				jClean_doUgly	= return_bool   (configFile	,"JetClean_doUgly");
  std::string   		jetCollection	= return_string (configFile	,"JetCollection");
  std::string 			jetCalConfig    = return_string (configFile	,"JetCalConfigFile");
  std::string			jetCalSequence	= return_string (configFile	,"JetCalCalibSequence");
  std::string   		jetUncMCtype	= return_string (configFile	,"JetUncert_MCtype");
  std::string   		jetUncConfig	= return_string (configFile	,"JetUncert_ConfigFile");
  std::string   		jerPlotName	= return_string (configFile	,"JER_plotFileName");
  bool   			jerSmearNominal	= return_bool   (configFile	,"JERsmear_ApplyNominalSmearing");
  std::string   		jerSmearSys	= return_string (configFile	,"JERsmear_SystematicMode");
  // jet scale factors
  std::string   		jvtSF_WP	= return_string (configFile	,"JetJvtSF_WP");
  std::string   		jvtSF_SFFile	= return_string (configFile	,"JetJvtSF_SFFile");
  // leptons
  bool   			doLepIso        = return_bool   (configFile	,"doLeptonIsolation");
  std::string 			isoSel_muWP	= return_string (configFile	,"IsoSel_MuonWP");
  std::string 			isoSel_elWP	= return_string (configFile	,"IsoSel_ElectronWP");
  float				muSel_MaxEta	= return_float  (configFile	,"MuSel_MaxEta");
  float				muSel_MaxQual	= return_float  (configFile	,"MuSel_MuQuality");
  // lepton scale factors 
  std::string   		muIDSF_WP	= return_string (configFile	,"MuEffSF_ID_WP");
  std::string   		muISOSF_WP	= return_string (configFile	,"MuEffSF_ISO_WP");
  std::string   		muTTVASF_WP	= return_string (configFile	,"MuEffSF_TTVA_WP");
  bool   			muTrigSF_zeroSF	= return_bool   (configFile	,"MuTrigSF_AllowZeroSF");
  std::string   		muTrigSF_muQual = return_string (configFile	,"MuTrigSF_MuQuality");
  std::string   		muTrigSF_muIso 	= return_string (configFile	,"MuTrigSF_MuIsolation");
  std::string   		elCal_ESmodel 	= return_string (configFile	,"ElCal_ESModel");
  std::string   		elCal_DecorMod 	= return_string (configFile	,"ElCal_DecorrelationModel");
  std::string   		elLH_WP		= return_string (configFile	,"ElLH_WorkingPoint");
  std::string   		elLH_config	= return_string (configFile	,"ElLH_ConfigFile");
  // electron scale factors
  std::string   		elIDSF_input	= return_string (configFile	,"ElEffSF_ID_input");
  std::string   		elISOSF_input	= return_string (configFile	,"ElEffSF_ISO_input");
  std::string   		elRECOSF_input	= return_string (configFile	,"ElEffSF_RECO_input");
  std::string   		elTrigSF_input	= return_string (configFile	,"ElTrigSF_input");
  // event selection
  float         		minNumOfLeptons = return_float  (configFile	,"NumberOfLeptons");
  float         		minNumOfJets 	= return_float  (configFile	,"NumberOfJets");
  float         		jetPtCut	= return_float  (configFile	,"JetPtCut");
  float         		jetEtaCut	= return_float  (configFile	,"JetEtaCut");
  float         		jetJVTCut	= return_float  (configFile	,"JetJVTcut");
  float         		muonPtCut	= return_float  (configFile	,"MuonPtCut");
  float         		muonEtaCut	= return_float  (configFile	,"MuonEtaCut");
  float         		muonz0sinTCut	= return_float  (configFile	,"Muonz0sinTcut");
  float         		muond0Cut	= return_float  (configFile	,"Muond0Cut");
  float         		muonsigd0Cut	= return_float  (configFile	,"Muonsigd0Cut");
  float         		elePtCut	= return_float  (configFile	,"ElectronPtCut");
  float         		eleEtaCut	= return_float  (configFile	,"ElectronEtaCut");
  float         		elez0sinTCut	= return_float  (configFile	,"Electronz0sinTcut");
  float         		electronsigd0Cut= return_float  (configFile	,"Electronsigd0Cut");
  // truth options
  bool				doTruth		= return_bool   (configFile	,"doTruth");
  bool				debugMode	= return_bool   (configFile	,"debugMode");
  std::cout << "doTruth:	" << doTruth << std::endl;
  std::cout << "debugMode:	" << debugMode << std::endl;
  if (isAODfile) 	std::cout << "WARNING:		You're running on AOD inputs... at your own risk ;)" << std::endl;
  if (doSystematics) 	std::cout << "INFO:		I'm gonna run systematics... :O" << std::endl;
  // Fetch all info from config file

  // xsec file
  TString xsecFile = "xsecFile";
  if( argc > 2 ) xsecFile = argv[ 3 ];

  // input folder for PLV calibration
  TString PLV_calfolder = "$TestArea/WWZanalysis/data/PLV_calibration";

  // Set up the job for xAOD access:
  //xAOD::TReturnCode::enableFailure();
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  TString ts_filePath = filePath.data();
  TString ts_fileName = fileName.data();
  const char* inputFilePath = gSystem->ExpandPathName (filePath.c_str());
  SH::ScanDir().filePattern(fileName).scan(sh,inputFilePath);
  
  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();


  // if systematic mode is false run only nominal version!
  if (!doSystematics) 	v_systematicTTrees={"nominal"};
  for (unsigned int i=0; i<v_systematicTTrees.size(); i++) {
    // Is it the nominal TTree?
    // Or any systematic tree which does not have up and down variations (e.g. MET and JER ones)?
    bool no_up_and_down = ( v_systematicTTrees[i].compare("nominal")==0 || v_systematicTTrees[i].find("MET_")!=std::string::npos || v_systematicTTrees[i].compare("JET_JER_SINGLE_NP__1up")==0 );
    unsigned int UP_DOWN = no_up_and_down ? 1 : 2;
    for (unsigned int ud=0; ud<UP_DOWN; ud++) {

      // Create an EventLoop job:
      EL::Job job;
      job.sampleHandler( sh );
      // Set max number of processed events
      //job.options()->setDouble (EL::Job::optMaxEvents, 10);
      job.options()->setDouble (EL::Job::optMaxEvents, 1000);
      //job.options()->setDouble (EL::Job::optMaxEvents, 3000);

      //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);
      job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);  
      //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);  
      job.options()->setDouble (EL::Job::optXAODSummaryReport, 0);
 
      std::string suffix("");
      if (ud==0)                suffix="__1down";
      else if (ud==1)           suffix="__1up";
      std::string myoutName = (UP_DOWN==1) ? v_systematicTTrees[i] : v_systematicTTrees[i]+suffix;
      // define an output and an ntuple associated to that output
      EL::OutputStream output  (myoutName);
      job.outputAdd (output);
      EL::NTupleSvc *ntuple = new EL::NTupleSvc (myoutName);
      job.algsAdd (ntuple);

      // let's initialize the algorithm to use the xAODRootAccess package
      job.useXAOD ();

      // Add our analysis to the job:
      WWZanalysis::MyxAODAnalysis* alg = new WWZanalysis::MyxAODAnalysis();
      job.algsAdd( alg );

      // Feed all options to main EL algorithm
      alg->m_outputName 					= myoutName; // give the name of the output TFile to our algorithm
      // TTree name
      alg->m_outputtree                    			= (UP_DOWN==1) ? v_systematicTTrees[i] : v_systematicTTrees[i]+suffix;
      // CP TOOLS OPTIONS
      // FIXME random files !
      // Event
      alg->m_opt_prw_confFiles 				= vprwConfFile; 
      alg->m_opt_prw_lcalcFiles 				= vprwLumiCalc; 
      // FIXME random xml 
      alg->m_opt_grl_GoodRunsList 				= vgrlXML; 
      // Jets
      alg->m_opt_jclean_CutLevel				= jClean_cut;
      alg->m_opt_jclean_DoUgly				= jClean_doUgly;//false;
      alg->m_opt_jcal_JetCollection				= jetCollection;
      alg->m_opt_jcal_ConfigFile				= jetCalConfig;
      // MC
      alg->m_opt_jcal_CalibSequence				.push_back( jetCalSequence		);
      // data 
      // FIXME: JetArea_Residual_Origin_EtaJES_GSC_Insitu (which is for data) not found
      alg->m_opt_jcal_CalibSequence				.push_back( jetCalSequence		);
      alg->m_opt_junc_MCType				= jetUncMCtype;
      alg->m_opt_junc_ConfigFile				= jetUncConfig;
      alg->m_opt_jer_PlotFileName				= jerPlotName;
      alg->m_opt_jersmear_ApplyNominalSmearing		= jerSmearNominal;
      alg->m_opt_jersmear_SystematicMode			= jerSmearSys;
      // jet scale factors
      alg->m_opt_jvtSF_WP					= jvtSF_WP;
      alg->m_opt_jvtSF_SFFile				= jvtSF_SFFile;
      // Leptons
      alg->m_opt_isosel_doLepIso                            = doLepIso;
      alg->m_opt_isosel_MuonWP				= isoSel_muWP;
      alg->m_opt_isosel_ElectronWP				= isoSel_elWP;
      alg->m_opt_musel_MaxEta				= muSel_MaxEta;
      alg->m_opt_musel_MuQuality				= muSel_MaxQual; //enum {Tight, Medium, Loose, VeryLoose}, corresponding to 0, 1, 2 and 3
      // muon scale factors
      alg->m_opt_muidSF_WP  				= muIDSF_WP; 
      alg->m_opt_muisoSF_WP					= muISOSF_WP;      
      alg->m_opt_muttvaSF_WP				= muTTVASF_WP;     
      alg->m_opt_mutrigSF_AllowZeroSF 			= muTrigSF_zeroSF; 
      alg->m_opt_mutrigSF_MuonQuality			= muTrigSF_muQual;
      alg->m_opt_mutrigSF_Isolation				= muTrigSF_muIso;
      // electrons
      alg->m_opt_elcal_ESModel 				= elCal_ESmodel;
      alg->m_opt_elcal_decorrelationModel 			= elCal_DecorMod;
      alg->m_opt_elLH_WorkingPoint				= elLH_WP;
      alg->m_opt_elLH_ConfigFile				= elLH_config;
      // electron scale factors
      alg->m_opt_elidSF_input				= elIDSF_input;
      alg->m_opt_elisoSF_input				= elISOSF_input;
      alg->m_opt_elrecoSF_input				= elRECOSF_input;
      alg->m_opt_eltrigSF_input				= elTrigSF_input;
      // CP TOOLS OPTIONS

      // EVENT SELECTION 
      // event
      alg->m_Nleptons_cut					= minNumOfLeptons;
      alg->m_Njets_cut					= minNumOfJets;
      // jets
      alg->m_jet_pt_cut 					= jetPtCut;
      alg->m_jet_eta_cut					= jetEtaCut;
      alg->m_jet_jvt_cut					= jetJVTCut; //applied for jets whose pT<60 GeV only!
      // muons
      alg->m_muon_pt_cut    				= muonPtCut; 
      alg->m_muon_eta_cut   				= muonEtaCut; 
      alg->m_muon_z0sinT_cut				= muonz0sinTCut; 
      alg->m_muon_d0_cut    				= muond0Cut; 
      alg->m_muon_sigd0_cut    				= muonsigd0Cut; 
      // electrons 
      alg->m_electron_pt_cut				= elePtCut;
      alg->m_electron_eta_cut				= eleEtaCut;
      alg->m_electron_z0sinT_cut				= elez0sinTCut;
      //m_electron_d0_cut;
      alg->m_electron_sigd0_cut    				= electronsigd0Cut; 
      // EVENT SELECTION 

      // OTHER OPTIONS
      alg->m_doTruth					= doTruth;
      alg->m_debugMode					= debugMode;
      alg->m_isAODfile					= isAODfile;
      alg->m_doSystematics					= doSystematics;
      // OTHER OPTIONS
      // Feed all options to main EL algorithm

      // xsec file
      alg->m_xsecFile                                       = xsecFile;

      // input folder for PLV calibration
      alg->m_PLV_calfolder					= PLV_calfolder;

      // Run the job using the local/direct driver:
      EL::DirectDriver driver;
      driver.submit( job, submitDir+myoutName );

    }
  }


  // Fetch and plot our histogram
  //  SH::SampleHandler sh_hist;
  //sh_hist.load (submitDir + "/hist");
  //TH1 *hist = (TH1*) sh_hist.get ("AOD.11078889._000001.pool.root.1")->readHist ("h_jetPt");
  // hist->Draw ();

  return 0;
}


std::string return_string(TString configFile, std::string keyword) {
  int count(0.);
  // Fetch all info from config file
  std::string outString;
  std::ifstream ifs(configFile.Data());
  std::string aLine;
  if (ifs.fail()) { std::cerr<<"Failure when reading a file: "<< configFile <<std::endl; return 0; }
  else  {
    while (getline(ifs,aLine)) {
      //std::cout << aLine << std::endl;
      if ( (aLine.find("#")==0 || aLine.find(keyword)==std::string::npos) ) continue;
      else {
        if (count>0) throw std::logic_error(std::string("Error in testRun.cxx -> I found more than one options with the same substring! The keyword is ") + keyword);
        size_t pos = aLine.find_first_of(":");
        outString=aLine.substr(pos+1,aLine.size());
        count ++;
      }
    }
  }
  //std::cout << "return_string: keyword	is	" << keyword << std::endl;
  //std::cout << "return_string: outString is	" << outString << std::endl;
  if (outString.empty()) std::cout << "WARNING: empty string for '" << keyword << "'" << std::endl;
  return outString;
}

std::vector<std::string> return_vstring(TString configFile, std::string keyword, bool isLocal, std::string data_folder) {
  std::vector<std::string> vstring;
  std::string outString;
  std::ifstream ifs(configFile.Data());
  std::string aLine;
  if (ifs.fail()) { throw std::logic_error(std::string("Failure when reading a file: ") + configFile); }
  else  {
    while (getline(ifs,aLine)) {
      if ( (aLine.find("#")==0 || aLine.find(keyword)==std::string::npos) ) continue;
      else {
        size_t pos = aLine.find_first_of(":");
        outString = isLocal ? data_folder+aLine.substr(pos+1,aLine.size()) : aLine.substr(pos+1,aLine.size());
        vstring.push_back(outString);
      }
    }
  }
  if (!vstring.size()) std::cout << "WARNING: empty string vector for '" << keyword << "'" << std::endl; 
  return vstring;
}

bool return_bool(TString configFile, std::string keyword) {
  bool outBool;
  std::string first_string = return_string(configFile,keyword);
  std::istringstream(first_string) >> outBool;
  //std::cout << "BOOLEAN:	" << outBool << std::endl;
  return outBool;
}

float return_float(TString configFile, std::string keyword) {
  float outFloat;
  std::string first_string = return_string(configFile,keyword);
  outFloat = std::stof(first_string);
  //std::cout << "FLOAT:	" << outFloat << std::endl;
  return outFloat;
}
