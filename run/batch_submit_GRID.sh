#!/bin/bash

xsecfile=usr/WorkDir/21.2.34/InstallArea/x86_64-slc6-gcc62-opt/src/WWZanalysis/data/susy_crosssections_13TeV.txt
#xsecfile=usr/WorkDir/21.2.27/InstallArea/x86_64-slc6-gcc62-opt/src/WWZanalysis/data/susy_crosssections_13TeV.txt
#xsecfile=usr/WorkDir/21.2.26/InstallArea/x86_64-slc6-gcc62-opt/src/WWZanalysis/data/susy_crosssections_13TeV.txt
#xsecfile=usr/WorkDir/21.2.20/InstallArea/x86_64-slc6-gcc62-opt/src/WWZanalysis/data/susy_crosssections_13TeV.txt
#xsecfile=usr/WorkDir/21.2.10/InstallArea/x86_64-slc6-gcc62-opt/src/WWZanalysis/data/susy_crosssections_13TeV.txt
#xsecfile=usr/WorkDir/21.2.5/InstallArea/x86_64-slc6-gcc62-opt/src/WWZanalysis/data/susy_crosssections_13TeV.txt
config=config
blacklist=blacklist.txt
listtxt=$*
COUNTER=0
while read sample
do
    echo "Input:"
    echo ${sample}
    substring=$(echo ${sample} | cut -d '_' -f 2 | cut -d '.' -f 2)
#    outputName="mc16a_13TeV."${substring}".ntuple_Sys7" 
#    outputName="mc16d_13TeV."${substring}".ntuple_Sys7"
    outputName="mc16a_13TeV."${substring}".ntuple_Miss_v7" 
#    outputName="mc16c_13TeV."${substring}".ntuple_Nomv7" 
#    outputName="mc16d_13TeV."${substring}".ntuple_Miss_v7" 
#    outputName="data15_13TeV."${substring}".ntuple_Nomv7" 
#    outputName="data16_13TeV."${substring}".ntuple_Nomv7.1" 
#    outputName="data17_13TeV."${substring}".ntuple_Nomv7"
    echo "Output:"
    echo ${outputName} 
    cat BATCH_job.sh | sed "s/XFOLDERX/folder_${COUNTER}/g" | sed "s|XSAMPLEX|${sample}|g" | sed "s|XOUTPUTNAMEX|${outputName}|g" | sed "s/XCONFIGX/${config}/g" | sed "s/XBLACKLISTX/${blacklist}/g" | sed "s|XCROSSSECFILEX|${xsecfile}|g" | sed "s|XLOGX|log_${COUNTER}|g" > BATCH_job_${COUNTER}.sh
    chmod 755 BATCH_job_${COUNTER}.sh
    queue="8nh"
    bsub -q $queue BATCH_job_${COUNTER}.sh
    COUNTER=$[${COUNTER}+1]
done < $*
