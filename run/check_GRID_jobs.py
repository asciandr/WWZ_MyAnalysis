#============================================================================
class link:     
    """     
    object that creates a link and clean up after itself     
    """        
    def __init__(self,target,linkname):         
        self.target = target         
        self.linkname = linkname         
        cmd = ['ln','-s']         
        cmd += [target]         
        cmd += [linkname]         
        cmd = " ".join(cmd)         
        print cmd         
        os.system(cmd)     
    def __del__(self):         
        os.remove(self.linkname)         
        os.remove(self.linkname+"c")

#============================================================================
class Sample:
    def __init__(self,job=None):
        ''' takes a pandatools.LocalJobsetSpec'''
        if job:
            outDS = job.outDS.split(',')[0]
            dsid  = outDS.split('.')[2]
            self.dsid     = str(dsid)
            self.outDS    = str(outDS)
            self.inDS     = str(job.inDS)
            self.AFII     = self.outDS.count('_a766') > 0

    def __eq__(self, other):
        return (self.dsid == other.dsid) and (self.AFII == other.AFII )

    def __str__(self):
        return "DSID: %s"  % self.dsid

    def __hash__(self):
        return hash(self.dsid)

#============================================================================
def getDoneSamplesOnGRID(productionName,retry_aborted,retry_failed,retry_finished,killAndRetry_pending,killAndRetry_scouting):
    # No need to link pbook to pbook.py and import it, see https://groups.cern.ch/group/hn-atlas-dist-analysis-help/Lists/Archive/Flat.aspx?RootFolder=%2Fgroup%2Fhn%2Datlas%2Ddist%2Danalysis%2Dhelp%2FLists%2FArchive%2FQuery%20panda%20servers%20without%20using%20pbook&FolderCTID=0x01200200FB8F64B0E343CB4680DEAC546A9DF813
    #pbookCore = pbook.PBookCore(False,False,False,False)
    pbookCore.sync()
    pbookCore.clean()
    pbookCore.updateTaskJobsetMap()

    unknown  = 0
    exhausted= 0
    done     = 0
    aborted  = 0
    ready    = 0
    broken   = 0
    finished = 0
    failed   = 0
    running  = 0
    scouting = 0
    pending  = 0
    total    = 0

    # output file with relecant jobID
    txtpath	= './'
    f_unknown	= open(txtpath+'unknown.txt', 'w')
    f_exhausted	= open(txtpath+'exhausted.txt', 'w')
    f_done     	= open(txtpath+'done.txt', 'w')
    f_aborted  	= open(txtpath+'aborted.txt', 'w')  
    f_ready    	= open(txtpath+'ready.txt', 'w')  
    f_broken   	= open(txtpath+'broken.txt', 'w')  
    f_finished 	= open(txtpath+'finished.txt', 'w')  
    f_failed   	= open(txtpath+'failed.txt', 'w')  
    f_running  	= open(txtpath+'running.txt', 'w')  
    f_scouting 	= open(txtpath+'scouting.txt', 'w')  
    f_pending  	= open(txtpath+'pending.txt', 'w')  
    f_total    	= open(txtpath+'total.txt', 'w')  

    samples = []

    jobList = pbookCore.getLocalJobList()
    for job in jobList:
        if job.outDS.count(productionName):
            total += 1
            if killAllOfThem:
                try:
                    pbookCore.kill(job.JobID)
                except:
                    pbookCore.kill(job.JobsetID)
            try:
                f_total.write("%s\n" %job.JobID)
            except:
                f_total.write("%s\n" %job.JobsetID)
            if job.taskStatus == 'finished' or job.taskStatus == 'failed' or job.taskStatus == 'aborted' or job.taskStatus == 'pending':
                # these are the jobs that we sometimes retry and for which the status might not be up to date
                job = pbookCore.statusJobJobset(job.JobsetID,forceUpdate=True)
            if job.taskStatus == 'done':
                done += 1
                samples += [Sample(job)]
                f_done.write("%s\n" %job.JobsetID)
            elif job.taskStatus == 'aborted':
                aborted += 1
                f_aborted.write("%s\n" %job.JobID)
                if retry_aborted:
                    print 'Retry aborted job with DS', job.inDS
                    pbookCore.retry(job.JobID)
            elif job.taskStatus == 'ready':
                ready += 1
                f_ready.write("%s\n" %job.JobsetID)
	        if kill_ready:
                    print 'Kill ready job'
                    pbookCore.kill(job.JobsetID)
            elif job.taskStatus == 'broken':
                broken += 1
                f_broken.write("%s\n" %job.JobsetID)
            elif job.taskStatus == 'scouting':
                scouting += 1
                f_scouting.write("%s\n" %job.JobsetID)
                if kill_scouting:
                    print 'Kill pending job'
                    pbookCore.kill(job.JobsetID)
                if killAndRetry_scouting:
                    print 'Kill and retry scouting job' 
                    pbookCore.killAndRetry(job.JobsetID)
            elif job.taskStatus == 'pending':
                pending += 1
                f_pending.write("%s\n" %job.JobID)
                if kill_pending:
                    print 'Kill pending job'
                    pbookCore.kill(job.JobID)
                if killAndRetry_pending:
                    print 'Kill and retry pending job' 
                    pbookCore.killAndRetry(job.JobID)
            elif job.taskStatus == 'finished':
                f_finished.write("%s\n" %job.JobID)
                finished += 1
                print job
                if retry_finished:
                    print 'Retrying finished job with DS', job.inDS
                    pbookCore.retry(job.JobID)
                #status_map = {}
                # jobStatus look like:
                #>>> job.jobStatus
                #'finished*109,failed*1'
                #for status in job.jobStatus.split(','):
                #    status_name  = status.split('*')[0]
                #    status_count = status.split('*')[1]
                #    status_map[status_name] = int(status_count)
                #call anything with more than 95% a success for mc
                #finish_fraction = float(status_map['finished'])/float(sum(status_map.values()))
                #if finish_fraction > 0.95 and not job.inDS.count('data') and not job.outDS.count('Sys'):
                #    print 'Uploading job with %s finish fraction and DS %s' %( finish_fraction, job.inDS)
                #    samples += [Sample(job)]
                #else:
                #    print 'Retrying finished job with DS', job.inDS
                #    pbookCore.retry(job.JobID)
                #    pass
            elif job.taskStatus == 'failed':
                failed += 1
                f_failed.write("%s\n" %job.JobID)
                if retry_failed:
                    print 'Retry failed job with DS', job.inDS
                    pbookCore.retry(job.JobID)
            elif job.taskStatus == 'running':
                running += 1
                f_running.write("%s\n" %job.JobsetID)
            elif job.taskStatus == 'exhausted':
                exhausted += 1
                f_exhausted.write("%s\n" %job.JobsetID)
                if retry_exhausted:
                    print 'Retry exhausted job with DS', job.JobsetID
                    pbookCore.retry(job.JobsetID)
            else: 
                unknown += 1
                f_unknown.write("%s\n" %job.JobsetID)
                print 'You did not forsee me... I am job ID ' + job.JobsetID + ' in status: '+job.taskStatus

    print '%s jobs in 	%s' % (total, productionName)
    print 'unknown:	%s' % unknown
    print 'exhausted:	%s' % exhausted
    print 'scouting:  	%s' % scouting 
    print 'pending:  	%s' % pending
    print 'ready:    	%s' % ready 
    print 'broken:   	%s' % broken
    print 'aborted:  	%s' % aborted 
    print 'failed:   	%s' % failed
    print 'finished: 	%s' % finished
    print 'running:  	%s' % running
    print 'done:     	%s' % done

    return samples

#============================================================================ 
#============================================================================ 
#============================================================================
if __name__ == '__main__':     
    import os, sys     
    import subprocess

    # name of the ntuple production you want to check
#    productionName = 'ntuple_v0.9'
#    productionName = 'ntuple_v0.10'
#    productionName = 'ntuple_v0.11'
#    productionName = 'ntuple_v0.12'
#    productionName = 'ntuple_v0.13'
#    productionName = 'ntuple_v0.14'
#    productionName = 'ntuple_v0.16'
#    productionName = 'ntuple_v0.17'
#    productionName = 'ntuple_v0.18'
#    productionName = 'ntuple_v0.20'
#    productionName = 'ntuple_check_ZZ_norm'
#    productionName = 'ntuple_2LEP'
#    productionName = 'ntuple_wMass2LEP'
#    productionName = 'ntuple_v1.0'
#    productionName = 'ntuple_v2.0'
#    productionName = 'ntuple_v3.0'
#    productionName = 'ntuple_v3.1'
#    productionName = 'ntuple_v4.0'
#    productionName = 'ntuple_v4.1'
#    productionName = 'ntuple_newLepDef'
#    productionName = 'ntuple_v5.0'
#    productionName = 'ntuple_v6'
#    productionName = 'QandD'
#    productionName = 'allSys'
#    productionName = 'Sysv7'
#    productionName = 'Nomv7'
#    productionName = 'Miss_v7'
    productionName = 'VVjjnominal'

    # list of options 
    retry_exhausted 		= False#True
    retry_aborted 		= False#True
    retry_failed 		= False#True
    retry_finished		= False#True
    kill_pending		= False#True
    killAndRetry_pending	= False#True
    kill_scouting		= False#True
    kill_ready			= False#True
    killAndRetry_scouting	= False#True
    # kill all pjobs matching the productionName
    killAllOfThem		= False#True


    try:         
        os.environ['X509_USER_PROXY']     
    except KeyError:         
        raise RuntimeError('Please setup your GRID certificate.') 

    # No need to link pbook to pbook.py and import it, see https://groups.cern.ch/group/hn-atlas-dist-analysis-help/Lists/Archive/Flat.aspx?RootFolder=%2Fgroup%2Fhn%2Datlas%2Ddist%2Danalysis%2Dhelp%2FLists%2FArchive%2FQuery%20panda%20servers%20without%20using%20pbook&FolderCTID=0x01200200FB8F64B0E343CB4680DEAC546A9DF813
    ##link pbook to pbook.py in working directory so it can be imported
    #pbookPath = os.environ['ATLAS_LOCAL_PANDACLIENT_PATH']+'/bin/pbook'     
    #pbookLinkname = os.getcwd()+"/pbook.py"     
    #pbookLink = link(pbookPath,pbookLinkname)     
    #import pbook
 
    from pandatools.PBookCore import PBookCore
    pbookCore = PBookCore()
    doneSamplesOnGRID = getDoneSamplesOnGRID(productionName,retry_aborted,retry_failed,retry_finished,killAndRetry_pending,killAndRetry_scouting)
