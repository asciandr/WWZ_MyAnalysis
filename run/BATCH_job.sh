#!/bin/bash

WORK_DIR=/afs/cern.ch/user/a/asciandr/work/WWZ/systematic_ttrees/
PROJ_DIR=systematic_ttrees/

pwd
cp -r $WORK_DIR .
ls -haltr $PROJ_DIR
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
cd $PROJ_DIR 
rm -rf grid_proxy 
source setup.sh && cd ../build 
rm -rf * && cmake ../source && make -j4 && source x86_64-slc6-gcc62-opt/setup.sh
cd ../run
### IF ON THE GRID 
ls -haltr $WORK_DIR/grid_proxy
cp $WORK_DIR/grid_proxy /tmp/x509up_u76083
chmod 600 /tmp/x509up_u76083
voms-proxy-init -voms atlas -n
lsetup panda

testRun_on_the_GRID XFOLDERX XSAMPLEX XOUTPUTNAMEX XBLACKLISTX XCONFIGX XCROSSSECFILEX

echo 'DONE!'
