#! /bin/bash

export RUCIO_ACCOUNT=${USER} 

cd source/

setupATLAS 
#asetup AnalysisBase,21.2.4,here # ---> HUGE MEMORY LEAK IN PILEUPREWEIGHTING TOOL
#asetup AnalysisBase,21.2.5,here 
#asetup AnalysisBase,21.2.6,here 
#asetup AnalysisBase,21.2.10,here 
#asetup AnalysisBase,21.2.20,here 
#asetup AnalysisBase,21.2.26,here 
#asetup AnalysisBase,21.2.27,here 
asetup AnalysisBase,21.2.34,here

voms-proxy-init -voms atlas 

lsetup pyami 
lsetup emi 
lsetup rucio 
lsetup panda 

cd ../run

source ../build/x86_64-slc6-gcc62-opt/setup.sh
