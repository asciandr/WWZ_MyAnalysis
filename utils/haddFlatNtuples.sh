#!/bin/bash


#workdir=`pwd`

#setupATLAS
lsetup root 

#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/newVVjjSamples/MC16a/
HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/newVVjjSamples/MC16d/

#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/Nomv7/MC16a/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/Nomv7/MC16d/

#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/MC16a/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/MC16d/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/data15/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/data16/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/data17/

#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/MC16d/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/MC16c/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/MC16a/

#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/newLepDef/MC16a/

#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.1/MC16a/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.1/MC16c/

#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.0/MC16a/

#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v2.0/MC16c/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v2.0/MC16a/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v1.0/MC16c/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v1.0/MC16a/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v1.0/data2016/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/rebuiltMET/MC16a/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/2LEP/MC16a/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.20/MC16a/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.20/MC16c/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.18/MC16a_v0.18/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.18/MC16c_v0.18/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.18/2016data_v0.18/
#HADD_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.18/2017data_v0.18/
echo "Going to HADD flat ntuples in:	"${HADD_FOLDER}

# ls -haltr

listtxt=$*
COUNTER=0
while read sample
do
    substring=$(echo ${sample} | cut -d '_' -f 2 | cut -d '.' -f 2)
    echo "Hadding:"
    echo ${substring}    
    folder=$(echo ${sample} | cut -d ':' -f 2)
    echo "Folder:"
    echo ${folder}    
    cd ${HADD_FOLDER}
    rm -rf ${substring}.root
    hadd -k ${substring}.root ${folder}/*.root*
    COUNTER=$[${COUNTER}+1]
    cd -
done < $*
