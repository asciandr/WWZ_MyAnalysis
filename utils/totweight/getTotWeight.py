#!/usr/bin/python

import os, math, sys, string, ROOT
import hashlib
from operator import itemgetter
ROOT.gROOT.SetBatch(True)

def main(argv):

  print "mc_channel_number\t\tevents\t\tweight"

  sampleListFile = ""
  if len(argv) == 0:
    print "Scanning current directory for root files and trying to extract yields..."
  elif len(argv) == 1:
    sampleListFile = argv[0]
#    print "Trying to extract yields from files listed in '" + sampleListFile + "'..."
  else:
    print "Usage: python getTotWeight.py [sampleListFile]"
    return
  # check for valid file list file
  if sampleListFile is not "" and not os.path.isfile(sampleListFile):
    print "Error: File '" + sampleListFile + "' does not exist!"
    return
  # check for valid output directory
  yieldDir = "./output/"
  if sampleListFile is not "":
    if not os.path.isdir(yieldDir):
      print "Warning: Directory '" + yieldDir + "' does not exist, can't copy output files!"

  # compile file list
  fileList = []
  files = open(sampleListFile)
  for file in files :
    fileList.append(file.rstrip())
#    print file.rstrip()

  # loop over root files and count
  for my_file in fileList:

    # root file?
    if not ".root" in my_file :
      print "Skipping non-root file", my_file
      continue

    # mc_channel_number
    start 	= string.rfind(my_file, "/")+1
    end 	= string.rfind(my_file, ".") 
    mc_channel_number = my_file[start : end]
#    print "mc_channel_number ", mc_channel_number 

    # read totweight 
    f = ROOT.TFile.Open(my_file,"read")
    h = f.Get("h_total_events")
    if not h:
      print "warning: h_total_events not found, skipping file", my_file
      continue

    ntot	= h.GetBinContent(0)
    totweight	= h.GetBinContent(1)
#    print "total number of processed events 	",ntot
#    print "total weight 			",totweight
    print str(mc_channel_number)+"\t\t"+str(ntot)+"\t\t"+str(totweight)
      
    f.Close()


if __name__ == "__main__":
  main(sys.argv[1:])
    
