#!/bin/bash

WORK_DIR=/afs/cern.ch/user/a/asciandr/work/WWZ/systematic_ttrees/
#EOS_SM_DIR=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16a/
EOS_SM_DIR=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16d/
#EOS_DIR=/eos/atlas/user/a/asciandr/WWZ/QandD/MC16d/

pwd
#ls -haltr $EOS_DIR
ls -haltr $EOS_SM_DIR
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
### IF ON THE GRID 
ls -haltr $WORK_DIR/grid_proxy
cp $WORK_DIR/grid_proxy /tmp/x509up_u76083
chmod 600 /tmp/x509up_u76083/
voms-proxy-init -voms atlas -n
lsetup rucio

#mc16a
#rucio download user.asciandr.mc16a_13TeV.XSAMPLEX.ntuple_Sysv7*.root
#mc16d
rucio download user.asciandr.mc16d_13TeV.XSAMPLEX.ntuple_Sysv7*.root
NRUCIO=$(ls -1 | grep user.asciandr | wc -l)
echo 'Number of rucio folders:	'${NRUCIO}

#if [NRUCIO!=74]; then
#   echo "I don't have all of the 74 rucio folders handy!"
#else
hadd -f2 XSAMPLEX.root user.asciandr.*.root/*
ls -haltr XSAMPLEX.root
#cp XSAMPLEX.root $EOS_DIR
#cp XSAMPLEX.root $EOS_SM_DIR
xrdcp -f -np XSAMPLEX.root root://eosatlas.cern.ch/$EOS_SM_DIR/XSAMPLEX.root

echo 'DONE!'
