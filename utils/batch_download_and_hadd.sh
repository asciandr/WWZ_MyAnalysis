#!/bin/bash

listtxt=$*
#COUNTER=20
#COUNTER=10
COUNTER=0
while read sample
do
    echo "SAMPLE:"
    echo ${sample}
    cat BATCH_job.sh | sed "s|XSAMPLEX|${sample}|g" > BATCH_job_${COUNTER}.sh
    chmod 755 BATCH_job_${COUNTER}.sh
    queue="1nd"
    #queue="8nh"
    bsub -q $queue BATCH_job_${COUNTER}.sh
    COUNTER=$[${COUNTER}+1]
done < $*
