#!/bin/bash


#workdir=`pwd`

#setupATLAS
#voms-proxy-init -voms atlas
#lsetup rucio

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/newVVjjSamples/MC16a/
GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/newVVjjSamples/MC16d/

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/Nomv7/data/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/Nomv7/MC16a/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/Nomv7/MC16d/

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/MC16a/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/MC16d/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/data15/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/data16/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v6/data17/

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/data/data15/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/data/data16/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/data/data17/

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/MC16d/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/MC16c/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v5.0/MC16a/

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/newLepDef/MC16a/

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.1/MC16c/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.1/MC16a/

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.0/data2015/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.0/data2016/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.0/data2017/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v3.0/MC16a/

#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v2.0/MC16c/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v2.0/data2017/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v2.0/MC16a/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v2.0/data2016/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v1.0/MC16c/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v1.0/data2017/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v1.0/data2016/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v1.0/MC16a/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/rebuiltMET/MC16a/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/2LEP/data2016/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/2LEP/MC16a/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/check_ZZ_norm/data2016/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.20/MC16a/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.20/MC16c/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.18/MC16a_v0.18/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.18/MC16c_v0.18/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.18/2016data_v0.18/
#GET_FOLDER=/eos/atlas/user/a/asciandr/WWZ/v0.18/2017data_v0.18/
echo "Going to download flat ntuples in:	"${GET_FOLDER}

#ls -haltr

listtxt=$*
COUNTER=0
while read sample
do
    echo "Downloading:"
    echo ${sample}
    cd ${GET_FOLDER}
    substring=$(echo ${sample} | cut -d '_' -f 2 | cut -d '.' -f 2)
    echo ${substring}
    rucio download ${sample}
    COUNTER=$[${COUNTER}+1]
    cd -
done < $*
